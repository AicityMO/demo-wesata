<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'demo-wesata');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '-DX69yIK~~3@Y@N<=pQ+=t2o yEkbO7[wn/4VR(olun;)5wiJA;0V1oTC<l`xv}p');
define('SECURE_AUTH_KEY',  '><0o))*cL)/r4D;tL4laQ}uPB>iApzPkSDX?X8eR$r,9nOA~}?a[xN;6B?deJ{!8');
define('LOGGED_IN_KEY',    'DVb|>b9[U;`4yOAkCMh%x%B9hH[0c{v{5Y:7(4qhqq{!n5l^@[-6j`J_~<JGO=7!');
define('NONCE_KEY',        '-6FJ*]#U3jL]2N<]hi]5lP#r~hkxUQ5TdZcnP3IG(bTE,-C*@IHF_[zw5cXC&y[L');
define('AUTH_SALT',        'cB^O0}b4b+p/<a@ZshW(.CQP_s~q/q-&2mdJOP#atZ-`|Ya$ ]u8wl.L(@!*: eF');
define('SECURE_AUTH_SALT', 'e8GPN8~aKvtyKYlTHz?=/fa(NzsHxzL|iEaaK.M<~1!H27&FySxWzZ-I5%-Fjx@L');
define('LOGGED_IN_SALT',   'cH m@/9`=u#],/0]_V98*m%4q/niPo.%sZ&dyKKH!ARA*yreFIx ./3bzUs2{}#:');
define('NONCE_SALT',       '{].Gl}RiJC&dbh:}c^StoWZt54$s(e1T2srxP*@>TNA{QU{ZkSDXPj^Ot>/_wE#G');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
