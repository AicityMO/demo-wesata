<?php
/**
 * Paket tema weesata
 * Author : yayun
 * Facebook : http://facebook.com/ciussgw
 * Whatsapp : 0838-1525-1385
 */
?>

<?php get_header(); ?>



<section class="wecontent clear">
    <div class="pactour">
        <?php if (have_posts()): ?>
	        <?php while (have_posts()): the_post(); ?>
		
            <?php global $post;
		    $diskon = get_post_meta($post->ID, '_diskon', true);
			$expired = get_post_meta($post->ID, '_expidi', true);
	    	$sekarang = date_i18n("j F Y");
			$wadiskon = date_i18n("j F Y", strtotime($expired));
			$repeatable_fields = get_post_meta($post->ID, 'repeatable_fields', true);
			$date = get_post_meta($post->ID, 'hasp_expire_date', true); 
			$images = get_post_meta($post->ID, 'vdw_gallery_id', true);
			?>
			
			<!-- detail tour top -->
			<div class="dettour clear">
			    
				<!-- MAIN TOP -->
                <div class="toptour">
		        	<div <?php post_class('single clear'); ?> id="post_<?php the_ID(); ?>">
			         	<div class="post-con">
						    <div class="imagen">
							    
								<!-- slide gallery paket -->
						    	<div class="slideshow">
							    	<div id="slideshow">
								    	<?php foreach ($images as $image) { ?>
									    	<div class="slide clear">
										    	<div class="post">
											    	<?php echo wp_get_attachment_link($image, 'full'); ?>
												</div>
											</div>
										<?php } ?>
									</div>
								</div>
								<!-- slide gallery paket -->
							
					        	<!-- slide title / promo -->
								<div class="package">
						            <div class="clear">
						                <div class="tttl">
										    <div class="pacti"><?php the_title(); ?></div>
										</div>
										<?php if(($wadiskon-$sekarang) > 0) { ?>
										<div id="clockdiv">
							                <div class="unday">		    
						    	    	    	<div class="days"></div>
									        	<span class="tebo">HARI</span>
							            	</div>
							              	<div class="unhour">
                            		            <div class="hours"></div>
								        		<span class="tebo">JAM</span>
                                            </div>
						          	    	<div class="unminute">
                            	        	    <div class="minutes"></div>
									        	<span class="tebo">MENIT</span>
                                            </div>
							             	<div class="unsecond">
                            		            <div class="seconds"></div>
									        	<span class="tebo">DETIK</span>
                                            </div>
						            	</div>
										<?php } else { ?>
										<div id="clockdiv">
										<div class="unday">
									    	<span class="tebo">harga paket mulai</span><br/>
									    	<span class="nodis">IDR <?php echo get_post_meta($post->ID, '_single', true); ?></span>
										</div>
										</div>
										<?php } ?>
						    		</div>
								</div>
								<!-- slide title / promo -->
								
								<!-- show price -->
								<div class="showprice">
							    	<?php if(($wadiskon-$sekarang) > 0) { ?>
									    <div class="butn">PROMO</div>
										<div class="priceft">
									    	HARGA MULAI<br/>
									    	<span class="diskon">IDR <?php echo get_post_meta($post->ID, '_dsingle', true); ?></span><br/>
								    		<del><em>IDR <?php echo get_post_meta($post->ID, '_single', true); ?></em></del>
										</div>
									<?php } ?>
									<span class="transparan"><?php setPostViews(get_the_ID()); ?><?php echo getPostViews(get_the_ID()); ?></span>
								</div>
								<!-- show price -->
								
							    <div class="bcovlay">
						    	</div>
								
							</div>
						</div>
					</div>		
				</div>
				<!-- MAIN TOP -->
				
				<!-- MAIN RIGHT -->
				<div class="drprice">
				    <div class="drin">
				    	<?php if(($wadiskon-$sekarang) > 0) { ?>
			    		<h2>PAKET DEWASA</h2>
						<div class="drp">
							<div class="drtt">Daftar Harga (Diskon) : </div>
							<table class="price">
							    <tr>
								    <td class="gan">SINGLE</td>
									<td><strong>IDR <?php echo get_post_meta($post->ID, '_dsingle', true); ?></strong><br/>
									<em><del>IDR <?php echo get_post_meta($post->ID, '_single', true); ?></del></em>
									</td>
								</tr>
								<tr>
								    <td>COUPLE</td>
									<td class="gan"><strong>IDR <?php echo get_post_meta($post->ID, '_dcouple', true); ?></strong><br/>
									<em><del>IDR <?php echo get_post_meta($post->ID, '_couple', true); ?></del></em>
									</td>
								</tr>
								<tr>
								    <td class="gan">TRIPLE</td>
									<td><strong>IDR <?php echo get_post_meta($post->ID, '_dtriple', true); ?></strong><br/>
									<em><del>IDR <?php echo get_post_meta($post->ID, '_triple', true); ?></del></em>
									</td>
								</tr>
								<tr>
								    <td>QUAD</td>
									<td class="gan"><strong>IDR <?php echo get_post_meta($post->ID, '_dquad', true); ?></strong><br/>
									<em><del>IDR <?php echo get_post_meta($post->ID, '_quad', true); ?></del></em>
									</td>
								</tr>
							</table>
							</div>
						<?php } else { ?>
						<h2>PAKET DEWASA</h2>
						<div class="drp">
							<div class="drtt">Daftar Harga (Normal) : </div>
							<table class="price">
							    <tr>
								    <td class="gan">SINGLE</td>
									<td><strong>IDR <?php echo get_post_meta($post->ID, '_single', true); ?></strong>
									</td>
								</tr>
								<tr>
								    <td>COUPLE</td>
									<td class="gan"><strong>IDR <?php echo get_post_meta($post->ID, '_couple', true); ?></strong>
									</td>
								</tr>
								<tr>
								    <td class="gan">TRIPLE</td>
									<td><strong>IDR <?php echo get_post_meta($post->ID, '_triple', true); ?></strong>
									</td>
								</tr>
								<tr>
								    <td>QUAD</td>
									<td class="gan"><strong>IDR <?php echo get_post_meta($post->ID, '_quad', true); ?></strong>
									</td>
								</tr>
							</table>
						</div>
						<?php } ?>
					</div>
				</div>
				<!-- MAIN RIGHT -->
				
				<!-- MAIN LEFT -->
				<div class="dlprice">
				    <div class="dlin">
				    	<?php if(($wadiskon-$sekarang) > 0) { ?>
			    		<h2>PAKET ANAK</h2>
						<div class="dlp">
							<div class="dltt">Daftar Harga (Diskon) : </div>
							<table class="price">
							    <tr>
								    <td class="gan">SINGLE</td>
									<td><strong>IDR <?php echo get_post_meta($post->ID, '_dsingle1', true); ?></strong><br/>
									<em><del>IDR <?php echo get_post_meta($post->ID, '_single1', true); ?></del></em>
									</td>
								</tr>
								<tr>
								    <td>COUPLE</td>
									<td class="gan"><strong>IDR <?php echo get_post_meta($post->ID, '_dcouple1', true); ?></strong><br/>
									<em><del>IDR <?php echo get_post_meta($post->ID, '_couple1', true); ?></del></em>
									</td>
								</tr>
								<tr>
								    <td class="gan">TRIPLE</td>
									<td><strong>IDR <?php echo get_post_meta($post->ID, '_dtriple1', true); ?></strong><br/>
									<em><del>IDR <?php echo get_post_meta($post->ID, '_triple1', true); ?></del></em>
									</td>
								</tr>
								<tr>
								    <td>QUAD</td>
									<td class="gan"><strong>IDR <?php echo get_post_meta($post->ID, '_dquad1', true); ?></strong><br/>
									<em><del>IDR <?php echo get_post_meta($post->ID, '_quad1', true); ?></del></em>
									</td>
								</tr>
							</table>
							</div>
						<?php } else { ?>
						<h2>PAKET ANAK</h2>
						<div class="dlp">
							<div class="dltt">Daftar Harga (Normal) : </div>
							<table class="price">
							    <tr>
								    <td class="gan">SINGLE</td>
									<td><strong>IDR <?php echo get_post_meta($post->ID, '_single1', true); ?></strong>
									</td>
								</tr>
								<tr>
								    <td>COUPLE</td>
									<td class="gan"><strong>IDR <?php echo get_post_meta($post->ID, '_couple1', true); ?></strong>
									</td>
								</tr>
								<tr>
								    <td class="gan">TRIPLE</td>
									<td><strong>IDR <?php echo get_post_meta($post->ID, '_triple1', true); ?></strong>
									</td>
								</tr>
								<tr>
								    <td>QUAD</td>
									<td class="gan"><strong>IDR <?php echo get_post_meta($post->ID, '_quad1', true); ?></strong>
									</td>
								</tr>
							</table>
							</div>
						
						<?php } ?>
					</div>
				</div>
				<!-- MAIN LEFT -->
				
			</div>
			<!-- detail tour top -->
			
			<div class="clear tourall">
			
				<!-- complete detail -->
                <div class="tourcmplt">
				
				    <!-- tab menu -->
					<div class="tmenu clear">
					    <div class="iti"><i class="fa fa-navicon"></i><span>ITINERARY</span></div>
					    <div class="ket"><i class="fa fa-file"></i><span>KETERANGAN</span></div>
						<div class="vid"><i class="fa fa-film"></i><span>VIDEO</span></div>
						<div class="pri"><i class="fa fa-heart-o"></i><span>PRIVATE TOUR</span></div>
					    <div class="kom"><i class="fa fa-comments-o"></i><span>KOMENTAR</span></div>
						<div class="bag"><i class="fa fa-share-alt"></i><span>BAGIKAN</span></div>
					</div>
					<!-- tab menu -->
					
		<div class="weeshare">
		    <div class="in">
			<a href="http://facebook.com/share.php?u=<?php the_permalink() ?>" target="_blank" title="<?php _e('Share ke Facebook', 'weesata'); ?>"><i class="fa fa-facebook"></i></a> 
			<a href="http://twitter.com/home?status=<?php the_title(); ?> <?php the_permalink() ?>" target="_blank" title="<?php _e('Share ke Twitter', 'weesata'); ?>"><i class="fa fa-twitter"></i></a> 
			<a href="whatsapp://send?text=<?php the_permalink() ?> <?php the_title(); ?>" title="<?php _e('Share ke Whatsapp', 'weesata'); ?>"><i class="fa fa-whatsapp"></i></a>
			<a href="http://plus.google.com/share?url=<?php the_permalink() ?>" target="_blank" title="<?php _e('Share ke Google+', 'weesata'); ?>"><i class="fa fa-google-plus"></i></a>  
			</div>
		</div>
					
					<!-- content -->
					<div class="itic">
				        <?php $count = 0;
						    foreach ( $repeatable_fields as $field ) {
							$count++;							?>
							<div class="outline">
							<div class="nday">#<?php echo $count; ?> <?php echo esc_attr( $field['hari'] );  ?></div>
							<div class="maptime">
						    	<span class="mtl"><i class="fa fa-map-marker"></i> <?php echo esc_attr( $field['dest'] ); ?></span> <span class="mtr"><i class="fa fa-clock-o"></i> <?php echo esc_attr( $field['jam'] ); ?></span>
							</div>
							<?php echo esc_attr( $field['det'] ); ?><br/>
							</div>
						<?php } ?>
					</div>
					
					<div class="ketc">
				    	<div class="nday">KETERANGAN</div>
			    	    	<?php the_content(); ?>
			    		
					</div>
					
					<div class="vidc">
				    	<div class="nday">VIDEO</div>
			    	    <iframe src="https://www.youtube.com/embed/<?php echo get_post_meta($post->ID, '_embed', true); ?>" frameborder="0" allowfullscreen></iframe>
					</div>
					
					<div class="pric">
				    	<div class="nday">PRIVATE TOUR</div>
						<?php if (get_option('private')) { ?>
				            <?php echo get_option('private'); ?>
						<?php } else { ?>
							Private Tour adalah jenis tour khusus, Anda dapat memilih jenis Private Tour pada semua jenis paket yang ada dan menentukan sendiri kapan tanggal keberangkatan yang diinginkan. Paket ini adalah paket fleksible bagi Anda yang menginginkan kenyamanan.
							<br/><br/>
							Berlaku ketentuan yang berbeda untuk memesan Private Tour:<br/>
							- Jumlah peserta Tour minimal 5 orang<br/>
							- Berlaku harga Single untuk semua peserta, dan tidak berlaku harga kolektif Couple, Triple, Quad, dan lainnya
						<?php } ?>
					</div>
					
					<div class="komc">
			    		<div class="nday">KOMENTAR</div>
				    	<?php comments_template(); ?>
					</div>
					<!-- content -->
					
				</div>
				<!-- complete detail -->
				
				<!-- order form -->
				<div class="odetr">
		     		<div class="odtt"><span class="ot">OPEN TOUR</span><span class="pt">PRIVATE TOUR</span></div>
					<div class="isform otop">
					<?php if (function_exists('wpcf7')) {
					    echo do_shortcode('[contact-form-7 id="336" title="Open Tour"]'); 
					} else {
					    echo 'Harap instal dan aktifkan plugin <strong>Contact Form 7</strong> dan plugin <strong>Contact Form 7 Cost Calculator</strong> untuk menjalankan Form Order<br/><br/>
						Tanpa kedua plugin tersebut maka Form Order + Perhitungan Biaya tidak dapat muncul';
					} ?>
					</div>
					<div class="isform obot">
					<?php if (function_exists('wpcf7')) {
					    echo do_shortcode('[contact-form-7 id="334" title="Private Tour"]');  
					} else {
					    echo 'Harap instal dan aktifkan plugin <strong>Contact Form 7</strong> dan plugin <strong>Contact Form 7 Cost Calculator</strong> untuk menjalankan Form Order<br/><br/>
						Tanpa kedua plugin tersebut maka Form Order + Perhitungan Biaya tidak dapat muncul';
					} ?>
					</div>
				</div>
				<!-- order form -->
			
			    <!-- additional -->
				<div class="deton">
				    <div class="ontt"><?php echo (get_option('online')) ? get_option('online').'' : 'ONLINE HOURS' ?></div>

					<!-- onhour table -->
					<div class="onlist">
					<table class="jad">
		    			<tr>
					    	<td class="jlef">
							    <div class="clock">
								    <i class="fa fa-clock-o"></i>	
								</div>
							</td>
							<td><div class="jday">SENIN</div></td>
							<td class="jright"><div class="jday"><?php echo (get_option('senin')) ? get_option('senin').'' : '08.00 - 21.00' ?></div></td>
		    			</tr>
						<tr>
					    	<td class="jlef">
							    <div class="clock">
								    <i class="fa fa-clock-o"></i>	
								</div>
							</td>
							<td><div class="jday">SELASA</div></td>
							<td class="jright"><div class="jday"><?php echo (get_option('selasa')) ? get_option('selasa').'' : '08.00 - 21.00' ?></div></td>
		    			</tr>
						<tr>
					    	<td class="jlef">
							    <div class="clock">
								    <i class="fa fa-clock-o"></i>	
								</div>
							</td>
							<td><div class="jday">RABU</div></td>
							<td class="jright"><div class="jday"><?php echo (get_option('rabu')) ? get_option('rabu').'' : '08.00 - 21.00' ?></div></td>
		    			</tr>
						<tr>
					    	<td class="jlef">
							    <div class="clock">
								    <i class="fa fa-clock-o"></i>	
								</div>
							</td>
							<td><div class="jday">KAMIS</div></td>
							<td class="jright"><div class="jday"><?php echo (get_option('kamis')) ? get_option('kamis').'' : '08.00 - 21.00' ?></div></td>
		    			</tr>
						<tr>
					    	<td class="jlef jlime">
							    <div class="clock">
								    <i class="fa fa-clock-o"></i>	
								</div>
							</td>
							<td class="jlime"><div class="jday">JUMAT</div></td>
							<td class="jright"><div class="jday jlime"><?php echo (get_option('jumat')) ? get_option('jumat').'' : '08.00 - 19.00' ?></div></td>
		    			</tr>
						<tr>
					    	<td class="jlef">
							    <div class="clock">
								    <i class="fa fa-clock-o"></i>	
								</div>
							</td>
							<td><div class="jday">SABTU</div></td>
							<td class="jright"><div class="jday"><?php echo (get_option('sabtu')) ? get_option('sabtu').'' : '08.00 - 21.00' ?></div></td>
		    			</tr>
						<tr>
					    	<td class="jlef jorange">
							    <div class="clock">
								    <i class="fa fa-clock-o"></i>	
								</div>
							</td>
							<td class="jorange"><div class="jday">MINGGU</div></td>
							<td class="jbott"><div class="jday jorange"><?php echo (get_option('minggu')) ? get_option('minggu').'' : '08.00 - 23.00' ?></div></td>
		    			</tr>
					</table>
                    </div>
					<!-- onhour table -->
										
			    </div>
				<!-- additional -->
			
			</div>

			<?php endwhile; ?>
     	<?php endif; ?>
	</div>
</section>

<script>

function getTimeRemaining(endtime){
  var t = Date.parse(endtime) - Date.parse(new Date());
  var seconds = Math.floor( (t/1000) % 60 );
  var minutes = Math.floor( (t/1000/60) % 60 );
  var hours = Math.floor( (t/(1000*60*60)) % 24 );
  var days = Math.floor( t/(1000*60*60*24) );
  return {
    'total': t,
    'days': days,
    'hours': hours,
    'minutes': minutes,
    'seconds': seconds
  };
}

function initializeClock(id, endtime){
  var clock = document.getElementById(id);
  var daysSpan = clock.querySelector('.days');
  var hoursSpan = clock.querySelector('.hours');
  var minutesSpan = clock.querySelector('.minutes');
  var secondsSpan = clock.querySelector('.seconds');

  function updateClock(){
    var t = getTimeRemaining(endtime);

    daysSpan.innerHTML = t.days;
    hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
    minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
    secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

    if(t.total<=0){
      clearInterval(timeinterval);
    }
  }

  updateClock();
  var timeinterval = setInterval(updateClock,1000);
}

var deadline = '<?php echo date("F j Y g:i", strtotime($expired)); ?> UTC+0700';
initializeClock('clockdiv', deadline);

</script>

<script type="text/javascript">
    $("document").ready(function($){
	    $(".ketc, .komc, .vidc, .pric").slideUp();
		$(".iti").click(function(){
		    $(".ketc, .komc, .vidc, .pric").slideUp();
			$(".itic").slideDown();
		});
		$(".ket").click(function(){
		    $(".itic, .komc, .vidc, .pric").slideUp();
			$(".ketc").slideDown();
		});
		$(".kom").click(function(){
		    $(".ketc, .itic, .vidc, .pric").slideUp();
			$(".komc").slideDown();
		});
		$(".vid").click(function(){
		    $(".ketc, .itic, .komc, .pric").slideUp();
			$(".vidc").slideDown();
		});
		$(".pri").click(function(){
		    $(".ketc, .itic, .komc, .vidc").slideUp();
			$(".pric").slideDown();
		});
		});
</script>
	
<?php get_footer(); ?>