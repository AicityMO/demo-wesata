<div class="search_form">
	<form method="get" id="searchform" action="<?php echo site_url('/'); ?>">
		<fieldset>
		    
			<input name="post_type" type="hidden" value="paket" />
			<div class="divsel">
		    	<div class="insel">
				    <input name="s" type="text" placeholder="Cari paket..." value="" />
				</div>
			</div>
			<div class="divsel">
		    	<div class="insel">
		    		<select name="tujuan">
			        	<option value="">Negara / Kota Tujuan</option>
					    	<?php $negterms = get_terms('tujuan', 'orderby=name');
						    	foreach ($negterms AS $term) :
							    	echo "<option value='".$term->slug."'".($_GET['tujuan'] == $term->slug ? ' selected="selected"' : '').">".$term->name."</option>\n";
								endforeach;
							?>
					</select>
				</div>
			</div>
			<div class="divsel">
		    	<div class="insel">
		    		<select name="long">
			        	<option value="">Jumlah Hari</option>
					    	<?php $theterms = get_terms('long', 'orderby=name');
						    	foreach ($theterms AS $term) :
							    	echo "<option value='".$term->slug."'".($_GET['long'] == $term->slug ? ' selected="selected"' : '').">".$term->name."</option>\n";
								endforeach;
							?>
					</select>
				</div>
			</div>
			<div class="divbut">
		    	<div class="insel">
		        	<button type="submit"><i class="fa fa-search"></i></button>
		    	</div>
			</div>
		</fieldset>
	</form>
</div>
