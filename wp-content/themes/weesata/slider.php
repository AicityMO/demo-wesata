<?php
/**
 * Slider tema weesata
 * Author : yayun
 * Facebook : http://facebook.com/ciussgw
 * Whatsapp : 0838-1525-1385
 */
?>

<?php query_posts('post_type=slider&showposts=5'); ?>

<div class="slideshow">

    <!-- SLIDER -->
	<div id="slideshow">
	
    	<?php if (have_posts()) { ?>
    		<?php while (have_posts()): the_post(); ?>
    		<div class="slide clear">
     			<div class="post">
     				<?php if (has_post_thumbnail()) echo ''.get_the_post_thumbnail($post->ID, 'slider',
    					array(
				    		'alt' => trim(strip_tags($post->post_title)),
				    		'title' => trim(strip_tags($post->post_title)),
			    		)).''; 
					?>
    			</div>
    		</div>
            <?php endwhile; ?>
    	<?php } else { ?>
	    	<div class="slide clear">
     			<div class="post">
     				<img src="<?php echo get_template_directory_uri(); ?>/images/slide.jpg"/>
    			</div>
    		</div>
		<?php } ?>

	</div>
	<!-- SLIDER SEKOLAH -->
	
	<div class="sloverlay">
		
	</div>

</div>

<?php wp_reset_query(); ?>
