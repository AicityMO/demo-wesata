<?php
/**
 * Galeri tema weesata
 * Author : yayun
 * Facebook : http://facebook.com/ciussgw
 * Whatsapp : 0838-1525-1385
 */
?>

<?php get_header(); ?>

<section class="wecontent clear">
    <div class="pactour container clear">
	
    	<div class="weleft">
        	<?php if (have_posts()): ?>
	        	<?php while (have_posts()): the_post(); ?>
				
				<!-- left side -->
			    <div class="sttl clear">
			    	<div class="blog-content gall">
			        	<?php the_content(); ?>
					</div>
				</div>
				
				<div class="post-navigation clear">
			    	<?php
					$prev_post = get_adjacent_post(false, '', true);
					$next_post = get_adjacent_post(false, '', false); ?>
					<?php if ($prev_post): $prev_post_url = get_permalink($prev_post->ID); $prev_post_title = $prev_post->post_title; ?>
						<a class="post-prev" href="<?php echo $prev_post_url; ?>"><em><?php _e('Galeri Sebelumnya', 'weesata'); ?></em><span><?php echo $prev_post_title; ?></span></a>
					<?php endif; ?>
					<?php if ($next_post): $next_post_url = get_permalink($next_post->ID); $next_post_title = $next_post->post_title; ?>
						<a class="post-next" href="<?php echo $next_post_url; ?>"><em><?php _e('Galeri Lebih Baru', 'weesata'); ?></em><span><?php echo $next_post_title; ?></span></a>
					<?php endif; ?>
		    	</div>
		
	        	<?php endwhile; ?>
        	<?php endif; ?>	
    	</div>

    	<div class="weright">
    		<div class="rpac">
		 		<div class="ttr">PAKET TOUR</div>
	        		<?php query_posts('post_type=paket&showposts=3&orderby=rand'); ?>
				        <?php if (have_posts()) { ?>
				    	    <?php while (have_posts()): the_post(); ?>
							
							<?php global $post;
							    $expired = get_post_meta($post->ID, '_expidi', true);
								$current_timestamp = date_i18n("j F Y");
								$db_timestamp = date_i18n("j F Y", strtotime($expired));
								?>
					     	    <div class="lst clear">
						     	    <div class="relpac">
								    <?php if (has_post_thumbnail()) { ?>
						        		<?php the_post_thumbnail('little', array(
							        		'alt' => trim(strip_tags($post->post_title)),
									    	'title' => trim(strip_tags($post->post_title)),
									    	)); ?>
						     		<?php } ?>
							    	</div>
									<a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
									<div class="relpri">
								    	<?php if(($db_timestamp-$current_timestamp) > 0) { ?>
							        		<strong>IDR <?php echo get_post_meta($post->ID, '_dsingle', true); ?></strong> <em><del><?php echo get_post_meta($post->ID, '_single', true); ?></del></em>
							      		<?php } else { ?>
								         	<strong>IDR <?php echo get_post_meta($post->ID, '_single', true); ?></strong>
								    	<?php } ?>
									</div>
						    	</div>
					    	<?php endwhile; ?>
				    	<?php } ?>
		        	<?php wp_reset_query(); ?>
			</div>
		
	    	<?php get_sidebar(); ?>
		
		</div>
	</div>
</section>
	
<?php get_footer(); ?>