<?php
/**
 * Post tema Akademi
 * Author : yayun
 * Facebook : http://facebook.com/ciussgw
 * Whatsapp : 0838-1525-1385
 */
?>

<?php get_header(); ?>

<section class="wecontent clear">
    <div class="pactour container clear">
	
    	<div class="weleft">
        	<?php if (have_posts()): ?>
	        	<?php while (have_posts()): the_post(); ?>

				<!-- left side -->
			    <div class="sttl clear">
				    <div class="img400">
					    <?php if (has_post_thumbnail()) echo ''.get_the_post_thumbnail($post->ID, 'landscape',
    				    	array(
				    		'alt' => trim(strip_tags($post->post_title)),
				    		'title' => trim(strip_tags($post->post_title)),
			    		)).''; ?>
						<h2><?php the_title(); ?></h2>
						<div class="met"><span class="mle"><strong>Oleh</strong> : Weesata - <strong>Kategori</strong> : </span><?php the_category(' / '); ?>
				    	</div>
				    
			            <?php printf(__('<div class="tbt"><div class="metz">%s</div><div class="dtms">%s</div></div>', 'weesata'), get_the_time('j'), get_the_time('M Y')); ?>
					
			    	</div>
					<div class="bcovlay">
					</div>
				</div>

				<div class="blog-content clear">
				    <?php the_content(); ?>
					
					<?php $related = get_posts( array( 
				    	'category__in' => wp_get_post_categories($post->ID),
						'numberposts' => 4, 
						'post__not_in' => array($post->ID) 
						) );
						
						setup_postdata($post);
						
						?>
						
						
			    		<div class="relativez">
						    <div class="single sing clear">
						
						<?php if( $related ) foreach( $related as $post ) { ?>

				    	<div class="rltd">
						    <div class="rely">
						    	<?php if (has_post_thumbnail()) { 
					    	    	echo ''.get_the_post_thumbnail($post->ID, 'news',
							        array(
							        	'alt' => trim(strip_tags($post->post_title)), 
								    	'title' => trim(strip_tags($post->post_title)),
							    	)); 
					    		} else { ?>
							        <img src="<?php echo get_template_directory_uri(); ?>/images/default.jpg"/>
						    	<?php } ?>
							
						    	<div class="rel-meta">
							        <span><i class="fa fa-clock-o"></i> <?php echo get_the_time('j M Y'); ?></span>
							        <h3><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
						    	</div>
								
						    	<div class="ccovlay">
						    	</div>
							</div>
				    	</div>
						
	                	<?php } ?>
		
		                    </div>
	                    </div> 
						
						<?php 
						
						wp_reset_postdata(); 
						
						?>

		        		<div class="commlist clear">
			   		    	<div class="comms">
		        				<?php if (comments_open()): ?>
								<div>
			    			    	<div  class="countcoms">
							    	    <i class="fa fa-comment"></i> <?php comments_number(__('0 Komentar', 'weesata'), __('1 Komentar', 'weesata'), __('% Komentar', 'weesata'), '', __('x', 'weesata') ); ?>
									</div>
									<?php comments_template(); ?>
								</div>
				   		     	<?php endif; ?>
			   		    	</div>		
						</div>
					
		    	 </div>
				 
	        	<?php endwhile; ?>
        	<?php endif; ?>
    	</div>
	
    	<div class="weright">
    		<div class="rpac">
		 		<div class="ttr">PAKET TOUR</div>
	        		<?php query_posts('post_type=paket&showposts=3&orderby=rand'); ?>
				        <?php if (have_posts()) { ?>
				    	    <?php while (have_posts()): the_post(); ?>
							
							<?php global $post;
							    $expired = get_post_meta($post->ID, '_expidi', true);
								$current_timestamp = date_i18n("j F Y");
								$db_timestamp = date_i18n("j F Y", strtotime($expired));
								?>
					     	    <div class="lst clear">
						     	    <div class="relpac">
								    <?php if (has_post_thumbnail()) { ?>
						        		<?php the_post_thumbnail('little', array(
							        		'alt' => trim(strip_tags($post->post_title)),
									    	'title' => trim(strip_tags($post->post_title)),
									    	)); ?>
						     		<?php } ?>
							    	</div>
									<a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
									<div class="relpri">
								    	<?php if(($db_timestamp-$current_timestamp) > 0) { ?>
							        		<strong>IDR <?php echo get_post_meta($post->ID, '_dsingle', true); ?></strong> <em><del><?php echo get_post_meta($post->ID, '_single', true); ?></del></em>
							      		<?php } else { ?>
								         	<strong>IDR <?php echo get_post_meta($post->ID, '_single', true); ?></strong>
								    	<?php } ?>
									</div>
						    	</div>
					    	<?php endwhile; ?>
				    	<?php } ?>
		        	<?php wp_reset_query(); ?>
			</div>
		
	    	<?php get_sidebar(); ?>
		
		</div>
	</div>
</section>
	
<?php get_footer(); ?>
