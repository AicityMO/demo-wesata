<?php
/**
 * Post tema weesata
 * Author : yayun
 * Facebook : http://facebook.com/ciussgw
 * Whatsapp : 0838-1525-1385
 */
?>

<?php get_header(); ?>

<section class="wecontent clear">
    <div class="pactour container clear">
	
    	<div class="weleft">
        	<?php if (have_posts()): ?>
        		<?php while (have_posts()): the_post(); ?>
				
			    	<?php global $post;
				    	$got = get_post_meta($post->ID, '_going', true);
						$bat = get_post_meta($post->ID, '_back', true);
					?>
					
			    	<!-- left side -->
					<div class="sttl clear">
			    		<div class="img400">
			    		    <?php if (has_post_thumbnail()) echo ''.get_the_post_thumbnail($post->ID, 'landscape',
    			    	    	array(
				        		'alt' => trim(strip_tags($post->post_title)),
				        		'title' => trim(strip_tags($post->post_title)),
			    	    	)).''; ?>
					    	<div class="imglay">
						    	<h1><?php the_title(); ?></h1>
								<div class="going">
					    			<strong><?php echo date_i18n("j M Y", strtotime($got)); ?> - <?php echo date_i18n("j M Y", strtotime($bat)); ?></strong>
								</div>
								<div class="imgcndn clear">
						    		BERANGKAT DALAM
									<div id="clockdiv">
						    			<div class="unday">		    
						    	    	    <div class="days"></div>
									        <span class="tebo">HARI</span>
							            </div>
										<div class="unhour">
							        		<div class="hours"></div>
								        	<span class="tebo">JAM</span>
										</div>
										<div class="unminute">
							    			<div class="minutes"></div>
											<span class="tebo">MENIT</span>
										</div>
										<div class="unsecond">
									    	<div class="seconds"></div>
								    		<span class="tebo">DETIK</span>
										</div>
									</div>
								</div>
								
							</div>
							
							<div class="pguide">
						    	<img src="<?php echo get_post_meta($post->ID, '_photo', true); ?>" />
								<strong><?php echo get_post_meta($post->ID, '_guide', true); ?></strong><br/>
								( <em>Tour Guide</em> )
							</div>
							
						</div>
					</div>
					
					<div class="blog-content clear">
			    		<?php the_content(); ?>
					</div>
					
				<?php endwhile; ?>
			<?php endif; ?>
		</div>
	
    	<div class="weright">
    		<div class="rpac">
		 		<div class="ttr">PAKET TOUR</div>
	        		<?php query_posts('post_type=paket&showposts=3&orderby=rand'); ?>
				        <?php if (have_posts()) { ?>
				    	    <?php while (have_posts()): the_post(); ?>
							
							<?php global $post;
							    $expired = get_post_meta($post->ID, '_expidi', true);
								$current_timestamp = date_i18n("j F Y");
								$db_timestamp = date_i18n("j F Y", strtotime($expired));
								?>
					     	    <div class="lst clear">
						     	    <div class="relpac">
								    <?php if (has_post_thumbnail()) { ?>
						        		<?php the_post_thumbnail('little', array(
							        		'alt' => trim(strip_tags($post->post_title)),
									    	'title' => trim(strip_tags($post->post_title)),
									    	)); ?>
						     		<?php } ?>
							    	</div>
									<a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
									<div class="relpri">
								    	<?php if(($db_timestamp-$current_timestamp) > 0) { ?>
							        		<strong>IDR <?php echo get_post_meta($post->ID, '_dsingle', true); ?></strong> <em><del><?php echo get_post_meta($post->ID, '_single', true); ?></del></em>
							      		<?php } else { ?>
								         	<strong>IDR <?php echo get_post_meta($post->ID, '_single', true); ?></strong>
								    	<?php } ?>
									</div>
						    	</div>
					    	<?php endwhile; ?>
				    	<?php } ?>
		        	<?php wp_reset_query(); ?>
			</div>
		
	    	<?php get_sidebar(); ?>
		
		</div>
		
<script>

function getTimeRemaining(endtime){
  var t = Date.parse(endtime) - Date.parse(new Date());
  var seconds = Math.floor( (t/1000) % 60 );
  var minutes = Math.floor( (t/1000/60) % 60 );
  var hours = Math.floor( (t/(1000*60*60)) % 24 );
  var days = Math.floor( t/(1000*60*60*24) );
  return {
    'total': t,
    'days': days,
    'hours': hours,
    'minutes': minutes,
    'seconds': seconds
  };
}

function initializeClock(id, endtime){
  var clock = document.getElementById(id);
  var daysSpan = clock.querySelector('.days');
  var hoursSpan = clock.querySelector('.hours');
  var minutesSpan = clock.querySelector('.minutes');
  var secondsSpan = clock.querySelector('.seconds');

  function updateClock(){
    var t = getTimeRemaining(endtime);

    daysSpan.innerHTML = t.days;
    hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
    minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
    secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

    if(t.total<=0){
      clearInterval(timeinterval);
    }
  }

  updateClock();
  var timeinterval = setInterval(updateClock,1000);
}

var deadline = '<?php echo date("F j Y g:i:s", strtotime($got)); ?> UTC+0700';
initializeClock('clockdiv', deadline);

</script>
		
	</div>
</section>
	
<?php get_footer(); ?>
