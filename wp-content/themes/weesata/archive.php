<?php get_header(); ?>

<section class="wecontent clear">
    <div class="pactour container clear">
	    
		<!-- left content -->
    	<div class="weleft">

	        <div class="we-meta">
			<h1>
		    	<?php $post = $posts[0]; 
			    if (is_category()) { 
		    		printf('<span><i class="fa fa-folder"></i> KATEGORI</span> : %s', single_cat_title('', false)); 
				} elseif (is_tag()) { 
		    		printf(__('<span><i class="fa fa-folder"></i> TAG</span> : %s', 'weesata'), single_tag_title('', false));
				} elseif (is_day()) { 
	    			printf(_c('<span><i class="fa fa-folder"></i> ARSIP HARI</span> : %s'), get_the_time('j M Y')); 
				} elseif (is_month()) { 
	    			printf(_c('<span><i class="fa fa-folder"></i> ARSIP BULAN</span> : %s'), get_the_time('F Y')); 
				} elseif (is_year()) { 
	    			printf(_c('<span><i class="fa fa-folder"></i> ARSIP TAHUN</span> : %s'), get_the_time('Y')); 
				} elseif (is_author()) { 
	    			_e('<span><i class="fa fa-folder"></i> ARSIP PENULIS</span>', 'weesata'); 
				} elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { 
		    		_e('<span><i class="fa fa-folder"></i> ARSIP TULISAN</span>', 'weesata'); 
				} ?>
			</h1>
			</div>
			
			<?php get_template_part('inc/loop/loop'); ?>
			<?php get_template_part('pagination'); ?>
			
		</div>
		<!-- left content -->
		
		<!-- right content -->
    	<div class="weright">
    		<div class="rpac">
		 		<div class="ttr">PAKET TOUR</div>
	        		<?php query_posts('post_type=paket&showposts=3&orderby=rand'); ?>
				        <?php if (have_posts()) { ?>
				    	    <?php while (have_posts()): the_post(); ?>
							
							<?php global $post;
							    $expired = get_post_meta($post->ID, '_expidi', true);
								$current_timestamp = date_i18n("j F Y");
								$db_timestamp = date_i18n("j F Y", strtotime($expired));
								?>
					     	    <div class="lst clear">
						     	    <div class="relpac">
								    <?php if (has_post_thumbnail()) { ?>
						        		<?php the_post_thumbnail('little', array(
							        		'alt' => trim(strip_tags($post->post_title)),
									    	'title' => trim(strip_tags($post->post_title)),
									    	)); ?>
						     		<?php } ?>
							    	</div>
									<a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
									<div class="relpri">
								    	<?php if(($db_timestamp-$current_timestamp) > 0) { ?>
							        		<strong>IDR <?php echo get_post_meta($post->ID, '_dsingle', true); ?></strong> <em><del><?php echo get_post_meta($post->ID, '_single', true); ?></del></em>
							      		<?php } else { ?>
								         	<strong>IDR <?php echo get_post_meta($post->ID, '_single', true); ?></strong>
								    	<?php } ?>
									</div>
						    	</div>
					    	<?php endwhile; ?>
				    	<?php } ?>
		        	<?php wp_reset_query(); ?>
		    	</div>
		
	    	<?php get_sidebar(); ?>
		
		</div>
	    <!-- right content -->
	
	</div>
</section>
	
<?php get_footer(); ?>
