<?php
/**
 * Footer tema weesata
 * Author : yayun
 * Facebook : http://facebook.com/ciussgw
 * Whatsapp : 0838-1525-1385
 */
?>

	</div>
	<!-- Container -->
		
	<div class="footbar clear">
	    <div class="container clear">
		     <div class="pack-one relat">
			
			    <div class="foot-bar fleft">
		    		<div class="foot-inn">
			    		<?php if (dynamic_sidebar('Footbar Kiri')):
						$widget_args = array(
				    		'before_widget' => '<div id="%1$s" class="%2$s widget">',
							'after_widget' => '</div>',
							'before_title' => '<h3>',
							'after_title' => '</h3>'
						); ?>	
						<?php endif; ?>
					</div>
				</div>
				
				<div class="foot-bar fcenter">
		    		<div class="foot-inn">
			    		<?php if (dynamic_sidebar('Footbar Tengah')):
						$widget_args = array(
				    		'before_widget' => '<div id="%1$s" class="%2$s widget">',
							'after_widget' => '</div>',
							'before_title' => '<h3>',
							'after_title' => '</h3>'
						); ?>	
						<?php endif; ?>
					</div>
				</div>
				
				<div class="foot-bar fright">
				    <div class="foot-inn">
			    		<?php if (dynamic_sidebar('Footbar Kanan')):
						$widget_args = array(
				    		'before_widget' => '<div id="%1$s" class="%2$s widget">',
							'after_widget' => '</div>',
							'before_title' => '<h3>',
							'after_title' => '</h3>'
						); ?>	
						<?php endif; ?>
					</div>
				</div>
			</div>
			
		</div>
	</div>


	<div class="footer clear">
	    <div class="container clear">
				<?php if (get_option('footer')) { ?>
				    <div class="copyright">
				        <?php echo get_option('footer'); ?>
					</div>
				<?php } else { ?>
				    <div class="copyright">
					Copyright &copy; 2017 <a href="<?php echo home_url(); ?>"><?php bloginfo('name'); ?></a>.
					<span><?php _e('Didukung oleh', 'weesata'); ?> <a href="http://wordpress.org">WordPress</a>. <?php _e('Tema weesata oleh', 'weesata'); ?> <a href="http://ciuss.com">Ciuss</a></span>
				    </div>
				<?php } ?>
		</div>
	</div>
	
	<?php wp_footer(); ?>
        
		<!-- Back to Top -->
		<a href="javascript:" id="return-to-top"><i class="fa fa-plane"></i></a>
	 	<script>
		  // Scroll Top
			$(window).scroll(function() {
				if ($(this).scrollTop() >= 150) {
					$('#return-to-top').fadeIn(200);
					$('.inn').addClass("fade");
					$('.wees').addClass("aksen");
			 	} else {
					$('#return-to-top').fadeOut(200);
					$('.open').css("display","block");
					$('.inn').removeClass("fade");
					$('.wees').removeClass("aksen");
			 	}
			});
			$('#return-to-top').click(function() {
				$('body,html').animate({
					scrollTop : 0
			 	}, 500);
			});
			
		</script>

	</body>
</html>
