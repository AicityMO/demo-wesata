<?php if (have_posts()): ?>

    <div id="loop" class="ploop clear">
    	<?php while (have_posts()): the_post(); ?>
        
    		<!-- pos archive -->
		    <div class="lco">
		     	<?php if (has_post_thumbnail()) { ?>
			    	<div class="thlo">
				    	<a href="<?php the_permalink() ?>"><?php the_post_thumbnail('landscaping', array(
	        				'alt' => trim(strip_tags($post->post_title)),
		        			'title' => trim(strip_tags($post->post_title)),
						)); ?></a>
						<div class="wimg">
						<?php } else { ?>
						<div class="nimg">
						<?php } ?>
					    	<div class="pub">
						    	<?php printf(__('<em>Terbit : %s</em>', 'weesata'), get_the_time('j F Y')); ?>
							</div>
							<h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
							<div class="catz">
						    	<span><?php the_category('</span> <span>'); ?></span>
							</div>
						<?php if (has_post_thumbnail()) { ?>
						</div>
					</div>
				<?php } else { ?>
				    </div>
				<?php } ?>
				
				<div class="blog-content">
			    	<p><?php if (function_exists('smart_excerpt')) smart_excerpt(get_the_excerpt(), 60); ?></p>
					<div class="bcbot">
					     <div class="more">READ MORE</div>
					</div>
				</div>
			</div>
		    <!-- pos archive -->
			
		<?php endwhile; ?>
	</div>

<?php endif; ?>
