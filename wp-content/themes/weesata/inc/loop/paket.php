				    <?php if (have_posts()) { ?>
					    <?php while (have_posts()): the_post(); ?>
						
						    <?php global $post;
							$expired = get_post_meta($post->ID, '_expidi', true);
							$current_timestamp = date_i18n("j F Y");
							$db_timestamp = date_i18n("j F Y", strtotime($expired)); ?>
						
						    <!-- 3 kolom landscape -->
						    <div class="we-3">
							    <div class="pack-img">
								    <?php if (has_post_thumbnail()) { ?>
									<a href="<?php the_permalink() ?>">
						        		<?php the_post_thumbnail('landscape', array(
							        		'alt' => trim(strip_tags($post->post_title)),
									    	'title' => trim(strip_tags($post->post_title)),
									    	)); ?>
								    </a>
						    	    <?php } else { ?>
									<a href="<?php the_permalink() ?>">
			    	    		        <img src="<?php echo get_template_directory_uri(); ?>/images/default.jpg"/>
									</a>
						     		<?php } ?>
									<div class="dets">
								    	<h4><?php the_title(); ?></h4>
										<?php if(($db_timestamp-$current_timestamp) > 0) { ?>
									        Mulai : <strong class="ors">IDR <?php echo get_post_meta($post->ID, '_dsingle', true); ?></strong> <del><em>IDR <?php echo get_post_meta($post->ID, '_single', true); ?></em></del><br/>
									    	Quota : <?php echo get_post_meta($post->ID, '_quota', true); ?> Orang
									    <?php } else { ?>
									    	Mulai : <strong class="ors">IDR <?php echo get_post_meta($post->ID, '_single', true); ?></strong><br/>
									    	Quota : <?php echo get_post_meta($post->ID, '_quota', true); ?> Orang
										<?php } ?>
									    <div class="order">
										    <a href="<?php the_permalink() ?>#order">ORDER</a>
										</div>
									</div>
									
									<?php $terms = get_the_terms( $post->ID , 'long' ); 
										if ( $terms != null ){
											echo '<div class="p-day">';
										    	foreach( $terms as $term ) {
												$term_link = get_term_link( $term, 'long' );
											    	echo '<div class="p-span">' . $term->name . '</div>';
												unset($term); 
										    	} 
											echo '</div>';
										} 
									?>
									
								</div>
							</div>
						    <!-- 3 kolom landscape -->
						
						<?php endwhile; ?>
					<?php } ?>

