<?php if (have_posts()): ?>

    <div id="loop" class="ploop clear">
    	<?php while (have_posts()): the_post(); ?>
            
    		<!-- pos archive -->
		    <div class="blog-content gall">
			    <iframe class="akframe" src="https://www.youtube.com/embed/<?php echo get_post_meta($post->ID, '_embed', true); ?>" frameborder="0" allowfullscreen></iframe>
				<div class="blog-content clear">
				     <?php printf(__('<div class="wvid"><div class="post-datez">%s</div><div class="post-mon">%s</div></div>', 'weesata'),
					      get_the_time('j'), get_the_time('M Y')
				     ); ?>
			    	<h1 class="vvid"><?php the_title(); ?></h1>
				</div>
			</div>
		    <!-- pos archive -->
			
		<?php endwhile; ?>
	</div>

<?php endif; ?>
