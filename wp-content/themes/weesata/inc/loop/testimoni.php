<?php if (have_posts()): ?>

    <div id="loop" class="ploop clear">
    	<?php while (have_posts()): the_post(); ?>
            
			<?php global $post;
			$rating = get_post_meta($post->ID, '_rating', true);
			$count_posts = wp_count_posts('testimoni')->publish;
			$count++; ?>
			
    		<!-- pos archive -->
		    <div class="lco clear">
			
				<div class="ltsm clear">
			       <div class="tap">
				    	<?php the_post_thumbnail('medium', array(
					    	'alt' => trim(strip_tags($post->post_title)),
							'title' => trim(strip_tags($post->post_title)),
						)); ?>
				   </div>
				   
				   <p>"..<?php if (function_exists('smart_excerpt')) smart_excerpt(get_the_excerpt(), 150); ?>.."</p>
				   
				   <span class="star">
	    			    <?php if ( $rating == 'super' ) {
				    	    echo '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>'; 
					    } else if ( $rating == 'bagus' ) {
			     		    echo '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>';
					    } else if ( $rating == 'cukup' ) {
			    		    echo '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>';
					    } else if ( $rating == 'kurang' ) { 
					        echo '<i class="fa fa-star"></i><i class="fa fa-star"></i>';
					    } else if ( $rating == 'buruk' ) {
				    	    echo '<i class="fa fa-star"></i>';
					    } else {
				    	    echo '<i class="fa fa-star"></i>';
					    } ?>
					</span><br/>
					<div class="wgt">
				    	<em><strong><?php the_title(); ?></strong> <span>( <?php echo get_post_meta($post->ID, '_profesi', true); ?> / <?php echo get_post_meta($post->ID, '_kota', true); ?> )</span></em>
					</div>
						
				</div>
					
			</div>
		    <!-- pos archive -->
			
		<?php endwhile; ?>
	</div>

<?php endif; ?>
