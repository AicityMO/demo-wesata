<?php if (have_posts()): ?>

    <div id="loop" class="ploop clear">
    	<?php while (have_posts()): the_post(); ?>
            
			<?php global $post;
				$got = get_post_meta($post->ID, '_going', true);
			?>
    		<!-- pos archive -->
		    <div class="lco schd">
			    <div class="thlo">
		         	<?php if (has_post_thumbnail()) { ?>
			    	
				    	<a href="<?php the_permalink() ?>"><?php the_post_thumbnail('landscaping', array(
	        				'alt' => trim(strip_tags($post->post_title)),
		        			'title' => trim(strip_tags($post->post_title)),
						)); ?></a>
						
					<?php } else { ?>
					
					    <a href="<?php the_permalink() ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/default.jpg"/></a>
					
					<?php } ?>
					<div class="wimg">
					    <div class="pub">
						    	<?php printf(__('<em>Terbit : %s</em>', 'mading'), get_the_time('j F Y')); ?>
						</div>
					    <h2><?php the_title(); ?></h2>
					</div>
				</div>
			</div>
		    <!-- pos archive -->
			
		<?php endwhile; ?>
	</div>

<?php endif; ?>
