<?php
class Data_Perusahaan extends WP_Widget {
	function __construct() {
		parent::__construct(
			'dataperusahaan',
			__('WEE : Data Perusahaan', 'weesata')
		);
	}

	public function widget($args, $instance) {
		extract($args);
		$title = apply_filters('widget_title', $instance['title']);

		echo $before_widget;

		if ($title) echo $before_title.$title.$after_title;
		?>

            <div id="googleMap" style="width:100%; height:220px;"></div>
			<div class="dapro">
		    	<div class="clear">
				    <i class="fa fa-home"></i> 
					<?php echo (get_option('perusahaan')) ? '<div class="prs"><h1>'.get_option('perusahaan').'</h1></div>' : '<div class="prs"><h1>WEESATA Tour & Travel</h1></div>' ?>
				</div>
				<div class="clear">
				    <div class="prs">
				    	<table><tr><td></td></tr>
					    	<?php echo (get_option('siup')) ? '<tr><td><strong>SIUP </strong></td><td> '.get_option('siup').'</td></tr>' : '' ?>
				        	<?php echo (get_option('tdup')) ? '<tr><td><strong>TDUP </strong></td><td> '.get_option('tdup').'</td></tr>' : '' ?>
				         	<?php echo (get_option('situ')) ? '<tr><td><strong>SITU </strong></td><td> '.get_option('situ').'</td></tr>' : '' ?>
				        	<?php echo (get_option('npwp')) ? '<tr><td><strong>NPWP </strong></td><td> '.get_option('npwp').'</td></tr>' : '' ?>
				        </table>
					</div>
				</div>
				<div class="clear">
				    <i class="fa fa-map-marker"></i>
					<div class="prs">
			    		<?php echo (get_option('alamat')) ? ''.get_option('alamat').'' : '' ?> <?php echo (get_option('kecamatan')) ? ' , '.get_option('kecamatan').'' : '' ?> <?php echo (get_option('kabupaten')) ? ' , '.get_option('kabupaten').'' : '' ?> <?php echo (get_option('provinsi')) ? ' , '.get_option('provinsi').'' : '' ?> <?php echo (get_option('kodepos')) ? ' - '.get_option('kodepos').'' : '' ?>
					</div>
				</div>
				<div class="clear">
				    <i class="fa fa-phone"></i> 
					<div class="prs">
				    	<?php echo (get_option('telepon')) ? '<h1>'.get_option('telepon').'</h1>' : '' ?>
					</div>
				</div>
				<div class="clear">
				    <i class="fa fa-fax"></i> 
					<div class="prs">
				    	<?php echo (get_option('fax')) ? '<h1>'.get_option('fax').'</h1>' : '' ?>
					</div>
				</div>
				<div class="clear">
				    <i class="fa fa-envelope"></i> 
					<div class="prs">
				    	<?php echo (get_option('email')) ? '<h1>'.get_option('email').'</h1>' : '' ?>
					</div>
				</div>
			</div>
			
		<?php
		echo $after_widget;
	}

	public function form($instance) {
		if (isset($instance['title'])) $title = esc_attr($instance['title']);
		else $title = __('Data Perusahaan', 'weesata');
		?>

		<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Judul:', 'weesata'); ?>
				<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></label>
		</p>

		<div style="margin-bottom: 5px;">
			<p class="social_options">
			     Widget ini digunakan untuk menampilkan data profile Perusahaan dan Peta di widget, pastikan telah mengisi datanya di Pengaturan tema Weesata. Check <a href="<?php bloginfo('url'); ?>/wp-admin/admin.php?page=weesatas" target="_blank">disini</a>
			</p>
		</div>

		<?php
	}

	public function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);

		return $instance;
	}
}