<?php
class Jadwal_Tour extends WP_Widget {
	function __construct() {
		parent::__construct(
			'jadwaltour',
			__('WEE : Jadwal Tour', 'weesata')
		);
	}

	public function widget($args, $instance) {
		extract($args);

		$title = apply_filters('widget_title', $instance['title']);

		global $post;
		if (get_option('agendawd')) $agendawd = get_option('agendawd');
		else $agendawd = 3;
		$q_args = array(
			'numberposts' => $agendawd,
			'post_type'=> 'schedule', 
			'meta_key' => '_minus', 
			'orderby' => 'meta_value_num',
			'order' => 'ASC',
		);
		$rpthumb_posts = get_posts($q_args);

		echo $before_widget;

		if ($title) echo $before_title.$title.$after_title;
        $count = 0;
		foreach ($rpthumb_posts as $post):
			setup_postdata($post);
			$count++;
			$tgoing = get_post_meta($post->ID, '_going', true);
			$tback = get_post_meta($post->ID, '_back', true);
			
			if ($count == 1 ) { ?>
		    	<div class="inage clear">
			    	<?php if (has_post_thumbnail()) {
				    	echo ''.get_the_post_thumbnail($post->ID, 'landscape',
				        	array(
						    	'alt' => trim(strip_tags($post->post_title)),
								'title' => trim(strip_tags($post->post_title)),
							)); ?>
					<?php } else { ?>
				    	<img src="<?php echo get_template_directory_uri(); ?>/images/default.jpg"/>
					<?php } ?>
					
					<div class="topjd clear">
				    	<em>JADWAL : <?php echo date_i18n("j M Y", strtotime($tgoing)); ?> - <?php echo date_i18n("j M Y", strtotime($tback)); ?></em>
				    	<div class="atime">
				        	<a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
						</div>
			    	</div>
					
					<div class="botjd">
			        	<img src="<?php echo get_post_meta($post->ID, '_photo', true); ?>"/> GUIDE : <?php echo get_post_meta($post->ID, '_guide', true); ?>
		        	</div>
				
		        	<div class="bcovlay">
		        	</div>
	        	</div>   
            <?php } else { ?>
	        	<div class="jadw clear">
		    	    <em>JADWAL : <?php echo date_i18n("j M Y", strtotime($tgoing)); ?> - <?php echo date_i18n("j M Y", strtotime($tback)); ?></em>
		        	<div class="atime">
			    		<a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
			    	</div>	
		    	</div>
	    	<?php } ?>
		<?php
		endforeach;

		echo $after_widget;
	}

	public function form($instance) {
		if (isset($instance['title'])) $title = esc_attr($instance['title']);
		else $title = __('Jadwal Tour', 'weesata');
		?>

		<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Judul:', 'weesata'); ?> <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></label></p>
		<p><label for="agendawd"><?php _e('Jumlah ditampilkan:', 'weesata'); ?> </label><input type="text" name="agendawd" id="agendawd" size="2" value="<?php echo get_option('agendawd'); ?>"/></p>

		<?php
	}
	
	public function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		update_option('agendawd', $_POST['agendawd']);
		return $instance;
	}
}
