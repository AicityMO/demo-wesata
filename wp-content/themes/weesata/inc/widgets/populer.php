<?php
class Paket_Populer extends WP_Widget {
	function __construct() {
		parent::__construct(
			'paketpopuler',
			__('WEE : Paket Populer', 'weesata')
		);
	}

	public function widget($args, $instance) {
		extract($args);

		$title = apply_filters('widget_title', $instance['title']);

		global $post;
		if (get_option('populer')) $populer = get_option('populer');
		else $populer = 3;
		$q_args = array(
			'numberposts' => $populer,
			'post_type'=> 'paket',
			'order' => 'ASC', 
			'meta_key'=>'popular_posts',
			'orderby' => 'meta_value_num'
		);
		$papopuler = get_posts($q_args);

		echo $before_widget;

		if ($title) echo $before_title.$title.$after_title;
		
	    	foreach ($papopuler as $post) {
		    	setup_postdata($post);
				
				echo '<div class="weesid clear">';
				if (has_post_thumbnail()) { 
					 echo '<div class="imgbar">'.get_the_post_thumbnail($post->ID, 'little',
						array(
							'alt' => trim(strip_tags($post->post_title)), 
							'title' => trim(strip_tags($post->post_title)),
							  )).'</div>'; 
				} 
				
				?>
				
				<div class="sitti"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
				</div>
				<div class="afti">
				    <a href="<?php the_permalink() ?>">ORDER</a>
				</div>

	    	<?php 
			    echo '</div>';
	    	}

	    	echo $after_widget;
    }

	public function form($instance) {
		if (isset($instance['title'])) $title = esc_attr($instance['title']);
		else $title = __('Paket Terpopuler', 'weesata');
		?>

		<p>
	    	<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Judul:', 'weesata'); ?> 
	        	<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
			</label>
		</p>
		<p>
	    	<label for="populer"><?php _e('Jumlah ditampilkan:', 'weesata'); ?> </label>
			<input type="text" name="populer" id="populer" size="2" value="<?php echo get_option('populer'); ?>"/>
		</p>

		<?php
	}
	
	public function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		update_option('populer', $_POST['populer']);
		return $instance;
	}
}
