<?php
class Daftar_Paket extends WP_Widget {
	function __construct() {
		parent::__construct(
			'daftarpaket',
			__('WEE : Paket Tour', 'weesata')
		);
	}

	public function widget($args, $instance) {
		extract($args);

		$title = apply_filters('widget_title', $instance['title']);

		global $post;
		if (get_option('dafpak')) $dafpak = get_option('dafpak');
		else $dafpak = 3;
		$q_args = array(
			'numberposts' => $dafpak,
			'post_type'=> 'paket',
			'order' => 'DESC',
			'orderby' => 'rand'
		);
		$padafpak = get_posts($q_args);

		echo $before_widget;

		if ($title) echo $before_title.$title.$after_title;
		
	    	foreach ($padafpak as $post) {
		    	setup_postdata($post);
				
				echo '<div class="weesid clear">';
				if (has_post_thumbnail()) { 
					 echo '<div class="imgbar">'.get_the_post_thumbnail($post->ID, 'little',
						array(
							'alt' => trim(strip_tags($post->post_title)), 
							'title' => trim(strip_tags($post->post_title)),
							  )).'</div>'; 
				} 
				
				?>
				
				<div class="sitti"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
				</div>
				<div class="afti">
				    <a href="<?php the_permalink() ?>">ORDER</a>
				</div>

	    	<?php 
			    echo '</div>';
	    	}

	    	echo $after_widget;
    }

	public function form($instance) {
		if (isset($instance['title'])) $title = esc_attr($instance['title']);
		else $title = __('Paket Tour', 'weesata');
		?>

		<p>
	    	<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Judul:', 'weesata'); ?> 
	        	<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
			</label>
		</p>
		<p>
	    	<label for="dafpak"><?php _e('Jumlah ditampilkan:', 'weesata'); ?> </label>
			<input type="text" name="dafpak" id="dafpak" size="2" value="<?php echo get_option('dafpak'); ?>"/>
		</p>

		<?php
	}
	
	public function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		update_option('dafpak', $_POST['dafpak']);
		return $instance;
	}
}
