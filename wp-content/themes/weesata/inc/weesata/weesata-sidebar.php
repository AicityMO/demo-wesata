<?php

/*
 * Register widget (sidebar).
 *
 */
function weesata_widgets_init() {
	require get_template_directory().'/inc/widgets/post.php';
	register_widget('Pos_Terbaru');
	require get_template_directory().'/inc/widgets/company.php';
	register_widget('Data_Perusahaan');
	require get_template_directory().'/inc/widgets/jadwal.php';
	register_widget('Jadwal_Tour');
	require get_template_directory().'/inc/widgets/videos.php';
	register_widget('Videos');
	require get_template_directory().'/inc/widgets/paket.php';
	register_widget('Daftar_Paket');
	require get_template_directory().'/inc/widgets/populer.php';
	register_widget('Paket_Populer');

	register_sidebar(array(
		'name' => __('Sidebar Utama', 'weesata'),
		'before_widget' => '<div id="%1$s" class="%2$s widget clear">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>'
	));
	
	register_sidebar(array(
		'name' => __('Footbar Kiri', 'weesata'),
		'before_widget' => '<div id="%1$s" class="%2$s widget clear">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>'
	));
	
	register_sidebar(array(
		'name' => __('Footbar Tengah', 'weesata'),
		'before_widget' => '<div id="%1$s" class="%2$s widget clear">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>'
	));

	register_sidebar(array(
		'name' => __('Footbar Kanan', 'weesata'),
		'before_widget' => '<div id="%1$s" class="%2$s widget clear">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>'
	));
	
}
add_action('widgets_init', 'weesata_widgets_init');

?>