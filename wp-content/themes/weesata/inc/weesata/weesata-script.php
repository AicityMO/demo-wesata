<?php
/*
Nama Tema: weesata
Tema URI: http://weesata.ciuss.com/
Facebook: http://facebook.com/ciussgw
Whatsapp: 0838-1525-1385
Copyright: (c) 2017 ciuss.com
*/
 
function weesata_scripts() {
	// Load our main stylesheet.
	wp_enqueue_style('weesata-style', get_stylesheet_uri());

	// Load the Internet Explorer specific stylesheet.
	wp_enqueue_style('weesata-ie', get_template_directory_uri().'/ie.css', array('weesata-style'), '20131217');
	wp_enqueue_style('awesome', get_template_directory_uri() . '/font-awesome/css/font-awesome.min.css');
	wp_enqueue_style('animation', get_template_directory_uri() . '/css/animation.css');
	wp_style_add_data('weesata-ie', 'conditional', 'IE');

	if (is_singular() && comments_open() && get_option('thread_comments')) {
		wp_enqueue_script('comment-reply');
	}

	wp_enqueue_script('cycle', get_template_directory_uri().'/js/jquery.slide.js', array('jquery'), '20170529', false);
	wp_enqueue_script('script', get_template_directory_uri().'/js/script.js', array('jquery'), '20170529', true);
	wp_enqueue_script('maps', get_template_directory_uri().'/js/maps.js', array('jquery'), '20170529', true);
}
add_action('wp_enqueue_scripts', 'weesata_scripts');

?>