<?php

/*
 * Tema weesata Wordpress.
 * Pengaturan baru akan berjalan setelah tema diaktifkan
 * Mengganti pengaturan tema sebelumnya
 */
function weesata_setup() {

	add_theme_support('automatic-feed-links');

	add_theme_support('post-thumbnails');
	add_image_size('slider', 1200, 500, true);
	add_image_size('landscape', 600, 450, true);
	add_image_size('square', 450, 450, true);
	add_image_size('portrait', 300, 400, true);
	add_image_size('little', 160, 120, true);

	register_nav_menus(array(
		'navigation' => __('Tampilkan di Navigasi Header', 'weesata'),
	));

	add_theme_support('html5', array(
		'search-form', 'comment-form', 'comment-list',
	));
}
add_action('after_setup_theme', 'weesata_setup');

if (is_admin() && isset($_GET['activated']) && $pagenow == 'themes.php') {
	update_option('posts_per_page', 9);
	update_option('paging_mode', 'default');
}

add_action('do_meta_boxes', 'replace_featured_image_box');
function replace_featured_image_box()
{
    remove_meta_box( 'postimagediv', 'siswa', 'side' );
    add_meta_box('postimagediv', __('Upload Photo, 300x400pixel'), 'post_thumbnail_meta_box', 'siswa', 'side', 'low');
	remove_meta_box( 'postimagediv', 'gtk', 'side' );
    add_meta_box('postimagediv', __('Upload Photo, 300x400pixel'), 'post_thumbnail_meta_box', 'gtk', 'side', 'low');
}

function wpbeginner_numeric_posts_nav() {

	if( is_singular() )
		return;
	global $wp_query;

	if( $wp_query->max_num_pages <= 1 )
		return;

	$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
	$max   = intval( $wp_query->max_num_pages );
	if ( $paged >= 1 )
		$links[] = $paged;
	if ( $paged >= 3 ) {
		$links[] = $paged - 1;
		$links[] = $paged - 2;
	}

	if ( ( $paged + 2 ) <= $max ) {
		$links[] = $paged + 2;
		$links[] = $paged + 1;
	}

	if ( ! in_array( 1, $links ) ) {
		$class = 1 == $paged ? ' class="active"' : '';
		printf( '<a%s href="%s">%s</a>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );
		if ( ! in_array( 2, $links ) )
			echo ' … ';
	}

	sort( $links );
	foreach ( (array) $links as $link ) {
		$class = $paged == $link ? ' class="active"' : '';
		printf( '<a%s href="%s">%s</a>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
	}

	if ( ! in_array( $max, $links ) ) {
		if ( ! in_array( $max - 1, $links ) )
			echo ' … ' . "\n";
		$class = $paged == $max ? ' class="active"' : '';
		printf( '<a%s href="%s">%s</a>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
	}

}

function shapeSpace_popular_posts($post_id) {
	$count_key = 'popular_posts';
	$count = get_post_meta($post_id, $count_key, true);
	if ($count == '') {
		$count = 0;
		delete_post_meta($post_id, $count_key);
		add_post_meta($post_id, $count_key, '0');
	} else {
		$count++;
		update_post_meta($post_id, $count_key, $count);
	}
}
function shapeSpace_track_posts($post_id) {
	if (!is_single()) return;
	if (empty($post_id)) {
		global $post;
		$post_id = $post->ID;
	}
	shapeSpace_popular_posts($post_id);
}
add_action('wp_head', 'shapeSpace_track_posts');

function getPostViews($postID){ 
    $count_key = 'post_views_count'; 
	$count = get_post_meta($postID, 
	$count_key, true); 
	
	if($count==''){ 
	delete_post_meta($postID, $count_key); 
	add_post_meta($postID, $count_key, '0'); return "0 View"; 
	} 
	return $count; 
} 
	
function setPostViews($postID) { 
    $count_key = 'post_views_count'; 
	$count = get_post_meta($postID, $count_key, true); 
	
	if($count==''){ 
	$count = 0; delete_post_meta($postID, $count_key); 
	add_post_meta($postID, $count_key, '0'); 
	}else{ 
	$count++; 
	update_post_meta($postID, $count_key, $count); 
	} 
}

wp_schedule_event(time(), 'daily', 'end_schedule');
add_action('init','end_schedule');
function end_schedule() {
	$the_query = get_posts( 'post_type=schedule' );	
	foreach($the_query as $single_post) {
		$id=$single_post->ID;
		$hapus=get_post_meta($id, '_going', true );
		$minus = strtotime(get_post_meta($post->ID, '_going', true));
		$hariini=date("d-m-Y");
			// jika jumlah hari ini lebih kecil dari hari expired maka post dihapus
            
			if($hapus>$hariini){ 
		    	$update_post = array(
				'ID' 			=> $id,
				'post_status'	=>	'publish',
				'post_type'	=>	'schedule' );
				wp_update_post($update_post); 
			} else {
			    $update_post = array(
				'ID' 			=> $id,
				'post_status'	=>	'trash',
				'post_type'	=>	'schedule' );
				wp_update_post($update_post);
			}
			
		
	}
}

add_action( 'wpcf7_init', 'custom_dewasa' );
 
function custom_dewasa() {
    wpcf7_add_form_tag( 'dewasa', 'harga_dewasa' ); 
}
 
function harga_dewasa( $tag ) {
global $post;

$diskons = get_post_meta($post->ID, '_expidi', true);
$current = date_i18n("j F Y");
$nodiskons = date_i18n("j F Y", strtotime($diskons));

$single = get_post_meta($post->ID, '_single', true); 
$couple = get_post_meta($post->ID, '_couple', true); 
$triple = get_post_meta($post->ID, '_triple', true); 
$quad = get_post_meta($post->ID, '_quad', true); 
$dsingle = get_post_meta($post->ID, '_dsingle', true); 
$dcouple = get_post_meta($post->ID, '_dcouple', true); 
$dtriple = get_post_meta($post->ID, '_dtriple', true); 
$dquad = get_post_meta($post->ID, '_dquad', true); 

    if(($nodiskons-$current) > 0) {
    return '
	    <label>Harga Paket Dewasa (DISKON)<br/>
    	<select name="dewasa">
		    <option value="">Pilih Paket</option>
	        <option value="'.str_replace(".","",$dsingle).'">Single : IDR '.$dsingle.'</option>
			<option value="'.str_replace(".","",$dcouple).'">Couple : IDR '.$dcouple.'</option>
			<option value="'.str_replace(".","",$dtriple).'">Triple : IDR '.$dtriple.'</option>
			<option value="'.str_replace(".","",$dquad).'">Quad : IDR '.$dquad.'</option>
		</select></label>
		<input name="paket" type="hidden" value=""/>';
	} else {
    return '
	    <label>Harga Paket Dewasa<br/>
    	<select name="dewasa">
		    <option value="">Pilih Paket</option>
	        <option value="'.str_replace(".","",$single).'">Single : IDR '.$single.'</option>
			<option value="'.str_replace(".","",$couple).'">Couple : IDR '.$couple.'</option>
			<option value="'.str_replace(".","",$triple).'">Triple : IDR '.$triple.'</option>
			<option value="'.str_replace(".","",$quad).'">Quad : IDR '.$quad.'</option>
		</select></label>';
	}
		
}

add_action( 'wpcf7_init', 'custom_anak' );
 
function custom_anak() {
    wpcf7_add_form_tag( 'anak', 'harga_anak' ); 
}
 
function harga_anak( $tag ) {
global $post;

$diskons = get_post_meta($post->ID, '_expidi', true);
$current = date_i18n("j F Y");
$nodiskons = date_i18n("j F Y", strtotime($diskons));

$single1 = get_post_meta($post->ID, '_single1', true); 
$couple1 = get_post_meta($post->ID, '_couple1', true); 
$triple1 = get_post_meta($post->ID, '_triple1', true); 
$quad1 = get_post_meta($post->ID, '_quad1', true);
$dsingle1 = get_post_meta($post->ID, '_dsingle1', true); 
$dcouple1 = get_post_meta($post->ID, '_dcouple1', true); 
$dtriple1 = get_post_meta($post->ID, '_dtriple1', true); 
$dquad1 = get_post_meta($post->ID, '_dquad1', true);

    if(($nodiskons-$current) > 0) {
    return '
	    <label>Harga Paket Anak (DISKON)<br/>
    	<select name="anak">
		    <option value="">Pilih Paket</option>
	        <option value="'.str_replace(".","",$dsingle1).'">Single : IDR '.$dsingle1.'</option>
			<option value="'.str_replace(".","",$dcouple1).'">Couple : IDR '.$dcouple1.'</option>
			<option value="'.str_replace(".","",$dtriple1).'">Triple : IDR '.$dtriple1.'</option>
			<option value="'.str_replace(".","",$dquad1).'">Quad : IDR '.$quad1.'</option>
		</select></label>
		';
	} else {
    return '
	    <label>Harga Paket Anak<br/>
    	<select name="anak">
		    <option value="">Pilih Paket</option>
	        <option value="'.str_replace(".","",$single1).'">Single : IDR '.$single1.'</option>
			<option value="'.str_replace(".","",$couple1).'">Couple : IDR '.$couple1.'</option>
			<option value="'.str_replace(".","",$triple1).'">Triple : IDR '.$triple1.'</option>
			<option value="'.str_replace(".","",$quad1).'">Quad : IDR '.$quad1.'</option>
		</select></label>
		';
	}
		
}

add_action( 'wpcf7_init', 'custom_private' );
 
function custom_private() {
    wpcf7_add_form_tag( 'private', 'harga_private' ); 
}

function harga_private( $tag ) {
global $post;

$single = get_post_meta($post->ID, '_single', true);
$harga = str_replace(".","",$single);
$harga2 = 2*$harga;
$harga3 = 3*$harga;
$harga4 = 4*$harga;
$harga5 = 5*$harga;
$harga6 = 6*$harga;
$harga7 = 7*$harga;
$harga8 = 8*$harga;
$harga9 = 9*$harga;
$harga10 = 10*$harga;
    return '
	    <label>Private Tour IDR '.$single.'/Org<br/>
    	<select name="private">
		    <option value="">Pilih Paket</option>
	        <option value="'.$harga.'">1 Orang ('.$harga.')</option>
			<option value="'.$harga2.'">2 Orang ('.$harga2.')</option>
			<option value="'.$harga3.'">3 Orang ('.$harga3.')</option>
			<option value="'.$harga4.'">4 Orang ('.$harga4.')</option>
			<option value="'.$harga5.'">5 Orang ('.$harga5.')</option>
			<option value="'.$harga6.'">6 Orang ('.$harga6.')</option>
			<option value="'.$harga7.'">7 Orang ('.$harga7.')</option>
			<option value="'.$harga8.'">8 Orang ('.$harga8.')</option>
			<option value="'.$harga9.'">9 Orang ('.$harga9.')</option>
			<option value="'.$harga10.'">10 Orang ('.$harga10.')</option>
		</select></label>
		<input name="paket" type="hidden" value=""/>';
		
}


?>