<?php

/**
 * Pengaturan tema weesata
 * Author : yayun
 * Facebook : http://facebook.com/ciussgw
 * Whatsapp : 0838-1525-1385
 */
 
add_action( 'admin_menu', 'weesata_add_page' );
function weesata_add_page() {
    $weesata_options_page = add_menu_page( 'Weesata', '<span style="color: #8c3; font-weight: bold">WEESATA</span>', 'manage_options', 'weesatas', 'options_page', 'dashicons-index-card', 2 );
} 

function options_page() {
	// Cek update input pengaturan
	if ($_POST['update_options'] == 'true') {
	    
		update_option('perusahaan', $_POST['perusahaan']);
		update_option('siup', $_POST['siup']);
		update_option('tdup', $_POST['tdup']);
		update_option('situ', $_POST['situ']);
		update_option('npwp', $_POST['npwp']);
		update_option('maps', $_POST['maps']);
		update_option('apikey', $_POST['apikey']);
		update_option('alamat', $_POST['alamat']);
		update_option('kecamatan', $_POST['kecamatan']);
		update_option('kabupaten', $_POST['kabupaten']);
		update_option('provinsi', $_POST['provinsi']);
		update_option('kodepos', $_POST['kodepos']);
		update_option('telepon', $_POST['telepon']);
		update_option('fax', $_POST['fax']);
		update_option('email', $_POST['email']);
		
		update_option('facebook', $_POST['facebook']);
		update_option('twitter', $_POST['twitter']);
		update_option('google', $_POST['google']);
		update_option('youtube', $_POST['youtube']);
		
		update_option('paket', $_POST['paket']);
		update_option('pesan', stripslashes($_POST['pesan']));
		update_option('deskripsi', stripslashes($_POST['deskripsi']));
		update_option('lihat', $_POST['lihat']);
		update_option('online', $_POST['online']);
		update_option('cek', $_POST['cek']);
		update_option('berita', $_POST['berita']);
		update_option('partner', $_POST['partner']);
		update_option('footer', stripslashes($_POST['footer']));
		update_option('private', $_POST['private']);
		
		update_option('logo_url', $_POST['logo_url']);
		update_option('favicon', $_POST['favicon']);
		update_option('background', $_POST['background']);
		update_option('weestyle', $_POST['weestyle']);
		update_option('boxed', $_POST['boxed']);
		update_option('fixed', $_POST['fixed']);
		update_option('transparan', $_POST['transparan']);
		update_option('boxedbg', $_POST['boxedbg']);
		update_option('white', $_POST['white']);
		update_option('ukuran', $_POST['ukuran']);
		update_option('ulang', $_POST['ulang']);
		update_option('posisi', $_POST['posisi']);
		update_option('bgfixed', $_POST['bgfixed']);
	}
	?>
	
	<style>
	@-moz-keyframes weesata {
    0% {
        background: rgba(0,0,0,0.9) url('<?php echo get_template_directory_uri(); ?>/images/a1.png') center no-repeat;
    }
    11% {
        background: rgba(0,0,0,0.9) url('<?php echo get_template_directory_uri(); ?>/images/a9.png') center no-repeat;
    }
    22% {
        background: rgba(0,0,0,0.9) url('<?php echo get_template_directory_uri(); ?>/images/a8.png') center no-repeat;
    }
    33% {
        background: rgba(0,0,0,0.9) url('<?php echo get_template_directory_uri(); ?>/images/a7.png') center no-repeat;
    }
    44% {
        background: rgba(0,0,0,0.9) url('<?php echo get_template_directory_uri(); ?>/images/a6.png') center no-repeat;
    }
	55% {
        background: rgba(0,0,0,0.9) url('<?php echo get_template_directory_uri(); ?>/images/a5.png') center no-repeat;
    }
	66% {
        background: rgba(0,0,0,0.9) url('<?php echo get_template_directory_uri(); ?>/images/a4.png') center no-repeat;
    }
	77% {
        background: rgba(0,0,0,0.9) url('<?php echo get_template_directory_uri(); ?>/images/a3.png') center no-repeat;
    }
	88% {
        background: rgba(0,0,0,0.9) url('<?php echo get_template_directory_uri(); ?>/images/a2.png') center no-repeat;
    }
	100% {
        background: rgba(0,0,0,0.9) url('<?php echo get_template_directory_uri(); ?>/images/a1.png') center no-repeat;
    }
    }
	
	@-webkit-keyframes weesata {
    0% {
        background: rgba(0,0,0,0.9) url('<?php echo get_template_directory_uri(); ?>/images/a1.png') center no-repeat;
    }
    11% {
        background: rgba(0,0,0,0.9) url('<?php echo get_template_directory_uri(); ?>/images/a9.png') center no-repeat;
    }
    22% {
        background: rgba(0,0,0,0.9) url('<?php echo get_template_directory_uri(); ?>/images/a8.png') center no-repeat;
    }
    33% {
        background: rgba(0,0,0,0.9) url('<?php echo get_template_directory_uri(); ?>/images/a7.png') center no-repeat;
    }
    44% {
        background: rgba(0,0,0,0.9) url('<?php echo get_template_directory_uri(); ?>/images/a6.png') center no-repeat;
    }
	55% {
        background: rgba(0,0,0,0.9) url('<?php echo get_template_directory_uri(); ?>/images/a5.png') center no-repeat;
    }
	66% {
        background: rgba(0,0,0,0.9) url('<?php echo get_template_directory_uri(); ?>/images/a4.png') center no-repeat;
    }
	77% {
        background: rgba(0,0,0,0.9) url('<?php echo get_template_directory_uri(); ?>/images/a3.png') center no-repeat;
    }
	88% {
        background: rgba(0,0,0,0.9) url('<?php echo get_template_directory_uri(); ?>/images/a2.png') center no-repeat;
    }
	100% {
        background: rgba(0,0,0,0.9) url('<?php echo get_template_directory_uri(); ?>/images/a1.png') center no-repeat;
    }
    }
	.severlay {
    -moz-animation: weesata .8s linear infinite;
    -webkit-animation: weesata .8s linear infinite
}
.seet {margin: 20px 20px 0 0; background: #fff; padding: 0; box-shadow: 0 0 10px #aaa; position: relative;}
.seet .severlay {position: absolute; top: 0; left: 0; right: 0; bottom: 0; z-index: -1;}
.outer {clear: both; display: block;}
#msub {position: fixed; top: 50%; right: 40px; z-index: 200; background: #65bd77; color: #fff; border: 0; border-radius: 3px; height:30px; line-height: 30px; display: none; padding: 0 15px; font-weight: bold; cursor:pointer;}
.tab_block {position: relative; background: #25313e; border-top: 5px solid #89cc97; clear: both; height: 50px; padding: 10px 15px;}
.mlogo {max-height:50px;}
#save {position: absolute; top: 15px; right: 20px; z-index: 200; background: #65bd77; color: #fff; border: 0; border-radius: 3px; height:30px; line-height: 30px; padding: 0 15px; font-weight: bold; cursor:pointer;}
.set_block {position: relative; background: #fff; clear: both;}
.inn {position: relative;}
.lmenu {position: absolute; top:0; left:0; bottom: 0; width: 220px; background: #41586e;}
.curs {cursor: pointer; padding: 0 15px; font-weight: bold; line-height: 40px; color: #fff;}
.welcome {background: #89cc97;}
.curs:hover {background: #89cc97; color: #fff;}
.curs a, .curs a:hover, .curs a:active, .curs a:focus {color: #fff; text-decoration: none; outline: 0 ! important;}
.rmenu {margin-left: 220px; min-height: 600px;}
.wel {padding: 20px; text-align: center;}
.wel img {width:100px; height: 100px; padding: 10px; border-radius: 60px; box-shadow: 0 0 10px #ccc;}
.content {font-size: 14px; max-width: 80%; margin: 0 auto 250px;}
.btable {padding: 0 0 40px;}	
td {padding: 10px 15px ; border-bottom: 1px solid #eee;}
td.tl {width: 150px; background: #f7f7f7;}
.top td.text { width: 150px; vertical-align: top; background: #f7f7f7; padding: 10px 15px ; border-right: 1px solid #eee;}
.custom-image {max-width:100%;}		
.custom_media_url {margin-bottom:10px; clear:right;}
.custom_media_upload {background: #65bd77 ! important; color: #fff ! important; text-transform: uppercase; font weight: bold;}	
td label {text-transform: uppercase; font-weight: bold; font-weight: 13px;}
.top .head {padding: 20px 15px ;}
.w20 {width: 20%; float: left;}
.pw {padding: 6px 0; margin: 0 8px 8px 0; color: #fff; border-radius: 3px;}
.bold {font-size: 14px; font-weight: bold; text-transform: uppercase;}
input[type=radio]{
  width     : 2em;
}
.donker {background: #036}
.blue {background: #0b82aa;}
.green {background: #0d9c18;}
.red {background: #d42007;}
.orange {background: #ff9900;}
.cyan {background: #085a71;}
.purple {background: #8c49a5;}
.forest {background: #03562b;}
.tosca {background: #0aa277;}
.grey {background: #777;}

.successModal {
display: block;
position: fixed;
top: 45%;
left: 25%;
width: 300px;
height: auto;
padding: 5px 20px;
border: 3px solid green;
background-color: #EFE;
z-index:1002;
overflow: auto;
-moz-border-radius: 15px; 
-webkit-border-radius: 15px;
-moz-box-shadow: 5px 5px 10px #cfcfcf;
-webkit-box-shadow: 5px 5px 10px #cfcfcf;
}

    </style>
	
	<div class="seet">
	    <div class="severlay"></div>
	    <div class="outer">
	    	<?php wp_enqueue_style('awesome', get_template_directory_uri() . '/font-awesome/css/font-awesome.css');	
                  			?>
            <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.min.js"></script>
            <script type="text/javascript">
		    	$("document").ready(function($){
			    	$("#change, #layout, #setting, #styles").slideUp();
					$(".welcome").click(function(){
				    	$(".welcome").css("background","#89cc97");
						$("#welcome").slideDown();
						$("#change, #layout, #setting, #styles").slideUp();
						$(".change, .layout, .setting, .styles").css("background","none");
					});
					$(".setting").click(function(){
			    		$(".setting").css("background","#89cc97");
						$("#setting").slideDown();
						$("#change, #layout, #welcome, #styles").slideUp();
						$(".change, .layout, .welcome, .styles").css("background","none");
					});
					$(".change").click(function(){
		    			$(".change").css("background","#89cc97");
						$("#change").slideDown();
						$("#setting, #layout, #welcome, #styles").slideUp();
						$(".setting, .layout, .welcome, .styles").css("background","none");
					});
					$(".layout").click(function(){
				    	$(".layout").css("background","#89cc97");
						$("#layout").slideDown();
						$("#change, #setting, #welcome, #styles").slideUp();
						$(".change, .setting, .welcome, .styles").css("background","none");
					});
					$(".styles").click(function(){
				    	$(".styles").css("background","#89cc97");
						$("#styles").slideDown();
						$("#change, #setting, #welcome, #layout").slideUp();
						$(".change, .setting, .welcome, .layout").css("background","none");
					});
					$(".submit").click(function(){
				    	$(".severlay").css("z-index","300");
					});
				});
			</script>

	    	<script>
		    	$(window).scroll(function() {
			    	if ($(this).scrollTop() >= 150) {
			    		$('#msub').fadeIn(200);
			     	} else {
			    		$('#msub').fadeOut(200);
			    	}
		    	});
	    	</script>
			
			<?php if(function_exists( 'wp_enqueue_media' )){
		    	wp_enqueue_media();
			} else {
		    	wp_enqueue_style('thickbox');
				wp_enqueue_script('media-upload');
				wp_enqueue_script('thickbox');
			}
			?>
			
			<script>
		    	jQuery(document).ready(function($){
		    		$('.custom_logo').click(function() {
				    	var send_attachment_bkp = wp.media.editor.send.attachment;
						var button = $(this);
						wp.media.editor.send.attachment = function(props, attachment) {
						jQuery(".custom_media_logo").attr('src', attachment.url);
						$(button).prev().val(attachment.url);
						wp.media.editor.send.attachment = send_attachment_bkp;
						}
						wp.media.editor.open(button);
						return false;  
					});
					$('.custom_icon').click(function() {
				    	var send_attachment_bkp = wp.media.editor.send.attachment;
						var button = $(this);
						wp.media.editor.send.attachment = function(props, attachment) {
						jQuery(".custom_media_icon").attr('src', attachment.url);
						$(button).prev().val(attachment.url);
						wp.media.editor.send.attachment = send_attachment_bkp;
						}
						wp.media.editor.open(button);
						return false;  
					});
					$('.custom_bg').click(function() {
				    	var send_attachment_bkp = wp.media.editor.send.attachment;
						var button = $(this);
						wp.media.editor.send.attachment = function(props, attachment) {
						jQuery(".custom_media_bg").attr('src', attachment.url);
						$(button).prev().val(attachment.url);
						wp.media.editor.send.attachment = send_attachment_bkp;
						}
						wp.media.editor.open(button);
						return false;  
					});
					$('.box_bg').click(function() {
				    	var send_attachment_bkp = wp.media.editor.send.attachment;
						var button = $(this);
						wp.media.editor.send.attachment = function(props, attachment) {
						jQuery(".custom_box_bg").attr('src', attachment.url);
						$(button).prev().val(attachment.url);
						wp.media.editor.send.attachment = send_attachment_bkp;
						}
						wp.media.editor.open(button);
						return false;  
					});
				});
			</script>
			
			<form method="post" action="">
		    	<input class="submit" type="submit" id="msub" value="SIMPAN" />
		    	<input type="hidden" name="update_options" value="true" />
				<div class="tab_block">
			    	<img src="<?php echo get_template_directory_uri(); ?>/images/weesata.png" class="mlogo" />
	             	<input id="save" class="submit" type="submit" value="SIMPAN" />
				</div>

				<div class="set_block">
			    	<div class="inn">
				    	<div class="lmenu">
					    	<div class="welcome curs">DASBOR TEMA</div>
							<div class="setting curs">INFORMASI UMUM</div>
							<div class="change curs">REPLACE TEXT</div>
							<div class="layout curs">LOGO & FAVICON</div>
							<div class="styles curs">LAYOUT & STYLE</div>
							<div class="external curs"><a href="#">BUTUH BANTUAN?</a></div>
						</div>

						<div class="rmenu">
					    	<div id="welcome">
						    	<div class="wel">
							    	<h3>
							    		Selamat datang <?php $user = wp_get_current_user(); printf( __( ', <em>%s</em> di Dasbor weesata', 'weesata' ), esc_html( $user->user_login ) ) . ''; ?>
							    	</h3>
									
									<?php if ( $user ) : ?>
									    <br/><img src="<?php echo esc_url( get_avatar_url( $user->ID ) ); ?>" /><br/><br/><br/>
										<?php endif; ?><div class="content">
										Ini adalah panel pengaturan untuk tema weesata, administrator website dapat melengkapi atau merubah data sesuai dengan keperluan. Jika ada kendala seputar tema silahkan kunjungi halaman Bantuan tema weesata untuk menemukan solusi, jika masih tetap belum menemukan solusi silahkan hubungi developer tema.<br/><br/>
										
										facebook : <a href="http://facebook.com/ciussgw">yayun</a> | <i class="fa fa-whatsapp"></i> 0838-1525-1385 | <i class="fa fa-envelope"></i> yayun@ciuss.com<br/>
									</div>
								</div>
							</div>

							<div id="setting">
						    	<div class="btable">
								    
							    	<table width="100%">
									    <tr valign="top">
									        <td class="head" colspan="2">
									    		<h3 style="font-size: 16px;">DATA PROFIL PERUSAHAAN</h3>
									    	</td>
								    	</tr>
								    	<tr valign="top">
									    	<td class="tl"><label for="perusahaan"><?php _e('Nama Perusahaan', 'weesata'); ?></label></td>
											<td>
										    	<input type="text" name="perusahaan" id="perusahaan" size="50" value="<?php echo get_option('perusahaan'); ?>"/><span class="description"> <?php _e('contoh, CV. Weesata Tour & Travel', 'weesata'); ?></span>
									    	</td>
										</tr>
										<tr valign="top">
								    		<td class="tl"><label for="siup"><?php _e('SIUP', 'weesata'); ?></label></td>
									    	<td>
									        	<input type="text" name="siup" id="siup" size="50" value="<?php echo get_option('siup'); ?>"/><span class="description"> <?php _e('contoh, 551.21/3071/I/DISPAR', 'weesata'); ?></span>
									    	</td>
										</tr>
										<tr valign="top">
								    		<td class="tl"><label for="tdup"><?php _e('TDUP', 'weesata'); ?></label></td>
									    	<td>
									        	<input type="text" name="tdup" id="tdup" size="50" value="<?php echo get_option('tdup'); ?>"/><span class="description"> <?php _e('contoh, 01/01/52/DPMPTSP/2017', 'weesata'); ?></span>
									    	</td>
										</tr>
										<tr valign="top">
								    		<td class="tl"><label for="situ"><?php _e('SITU', 'weesata'); ?></label></td>
									    	<td>
									        	<input type="text" name="situ" id="situ" size="50" value="<?php echo get_option('situ'); ?>"/><span class="description"> <?php _e('contoh, 11/309/1430/DS/DPMPTSP/2017', 'weesata'); ?></span>
									    	</td>
										</tr>
										<tr valign="top">
								    		<td class="tl"><label for="npwp"><?php _e('NPWP', 'weesata'); ?></label></td>
									    	<td>
									        	<input type="text" name="npwp" id="npwp" size="50" value="<?php echo get_option('npwp'); ?>"/><span class="description"> <?php _e('contoh, 02.798.056.4-903.000', 'weesata'); ?></span>
									    	</td>
										</tr>
										
										<tr valign="top"> 
								    		<td class="tl"><label for="alamat"><?php _e('Alamat Kantor', 'weesata'); ?></label></td>
										    <td>
											    <input type="text" name="alamat" id="alamat" size="50" value="<?php echo get_option('alamat'); ?>"/><span class="description"> <?php _e('contoh, Jl. Lintas Liwa No. 39', 'weesata'); ?></span>
											</td>
										</tr>
										
										<tr valign="top">
									    	<td class="tl"><label for="maps"><?php _e('Koordinat', 'weesata'); ?></label></td>
											<td>
										    	<input type="text" name="maps" id="maps" size="50" value="<?php echo get_option('maps'); ?>"/><span class="description"> <?php _e('contoh, -5.932330 , 105.992419 (lat dan lang dipisah koma)', 'weesata'); ?></span>
											</td>
										</tr>
										
										<tr valign="top">
									    	<td class="tl"><label for="apikey"><?php _e('API Key Maps', 'weesata'); ?></label></td>
											<td>
										    	<input type="text" name="apikey" id="apikey" size="50" value="<?php echo get_option('apikey'); ?>"/><span class="description"> <?php _e('dapatan kode API Key dari ', 'weesata'); ?><a href="https://developers.google.com/maps/web/" target="_blank">Google Dev</a></span>
									    	</td>
										</tr>
										
										<tr valign="top">
									     	<td class="tl"><label for="kecamatan"><?php _e('Kecamatan', 'weesata'); ?></label></td>
											<td>
										    	<input type="text" name="kecamatan" id="kecamatan" size="50" value="<?php echo get_option('kecamatan'); ?>"/><span class="description"> <?php _e('contoh, Sumber Jaya', 'weesata'); ?></span>
											</td>
										</tr>
										
										<tr valign="top">
									    	<td class="tl"><label for="kabupaten"><?php _e('Kabupaten', 'weesata'); ?></label></td>
											<td>
										    	<input type="text" name="kabupaten" id="kabupaten" size="50" value="<?php echo get_option('kabupaten'); ?>"/><span class="description"> <?php _e('contoh, Lampung Barat', 'weesata'); ?></span>
											</td>
										</tr>
										
										<tr valign="top">
									    	<td class="tl"><label for="provinsi"><?php _e('Provinsi', 'weesata'); ?></label></td>
											<td>
										    	<input type="text" name="provinsi" id="provinsi" size="50" value="<?php echo get_option('provinsi'); ?>"/><span class="description"> <?php _e('contoh, Lampung', 'weesata'); ?></span>
									    	</td>
								    	</tr>
										
										<tr valign="top">
									    	<td class="tl"><label for="kodepos"><?php _e('Kode Pos', 'weesata'); ?></label></td>
										    	<td><input type="text" name="kodepos" id="kodepos" size="50" value="<?php echo get_option('kodepos'); ?>"/><span class="description"> <?php _e('contoh, 34871', 'weesata'); ?></span>
											</td>
										</tr>
										
										<tr valign="top">
									    	<td class="tl"><label for="telepon"><?php _e('Telepon', 'weesata'); ?></label></td>
											<td>
										    	<input type="text" name="telepon" id="telepon" size="50" value="<?php echo get_option('telepon'); ?>"/><span class="description"> <?php _e('contoh, 083815251385', 'weesata'); ?></span>
											</td>
										</tr>
										
										<tr valign="top">
									    	<td class="tl"><label for="fax"><?php _e('Fax', 'weesata'); ?></label></td>
											<td>
										    	<input type="text" name="fax" id="fax" size="50" value="<?php echo get_option('fax'); ?>"/><span class="description"> <?php _e('contoh, 0724-1234567', 'weesata'); ?></span>
											</td>
										</tr>
										
										<tr valign="top"> 
									    	<td class="tl"><label for="email"><?php _e('Email', 'weesata'); ?></label></td>
											<td>
										    	<input type="text" name="email" id="email" size="50" value="<?php echo get_option('email'); ?>"/><span class="description"> <?php _e('contoh, yayun.ciuss@gmail.com', 'weesata'); ?></span>
											</td>
										</tr>
										
										<tr valign="top"> 
								    		<td class="tl"><label for="facebook"><?php _e('Facebook', 'weesata'); ?></label></td>
											<td>
										    	<input type="text" name="facebook" id="facebook" size="50" value="<?php echo get_option('facebook'); ?>"/><span class="description"> <?php _e('contoh, http://facebook.com/ciussgw', 'weesata'); ?></span>
											</td>
										</tr>
										
										<tr valign="top">
									    	<td class="tl"><label for="twitter"><?php _e('Twitter', 'weesata'); ?></label></td>
											<td>
										    	<input type="text" name="twitter" id="twitter" size="50" value="<?php echo get_option('twitter'); ?>"/><span class="description"> <?php _e('contoh, http://twitter.com/selfishyayun', 'weesata'); ?></span>
											</td>
										</tr>
										
										<tr valign="top">
									    	<td class="tl"><label for="youtube"><?php _e('Instagram', 'weesata'); ?></label></td>
											<td>
										    	<input type="text" name="instagram" id="instagram" size="50" value="<?php echo get_option('instagram'); ?>"/><span class="description"> <?php _e('contoh, http://instagram.com/', 'weesata'); ?></span>
											</td>
										</tr>
										
									 	<tr valign="top"> 
									    	<td class="tl"><label for="google"><?php _e('Google+', 'weesata'); ?></label></td>
											<td>
										    	<input type="text" name="google" id="google" size="50" value="<?php echo get_option('google'); ?>"/><span class="description"> <?php _e('contoh, http://plus.google.com/', 'weesata'); ?></span>
									    	</td>
								    	</tr>
										
										<tr valign="top">
									    	<td class="tl"><label for="youtube"><?php _e('Youtube', 'weesata'); ?></label></td>
											<td>
										    	<input type="text" name="youtube" id="youtube" size="50" value="<?php echo get_option('youtube'); ?>"/><span class="description"> <?php _e('contoh, http://youtube.com/', 'weesata'); ?></span>
											</td>
										</tr>
									</table>
								
								    
							    	<table width="100%">
									    <tr valign="top">
									        <td class="head" colspan="2">
									    		<h3 style="font-size: 16px;">JAM KERJA KANTOR</h3>
									    	</td>
								    	</tr>
								    	<tr valign="top">
									    	<td class="tl"><label for="senin"><?php _e('SENIN', 'weesata'); ?></label></td>
											<td>
										    	<input type="text" name="senin" id="senin" size="50" value="<?php echo get_option('senin'); ?>"/><span class="description"> <?php _e('contoh, 08.00 - 21.00', 'weesata'); ?></span>
									    	</td>
										</tr>
										<tr valign="top">
									    	<td class="tl"><label for="selasa"><?php _e('SELASA', 'weesata'); ?></label></td>
											<td>
										    	<input type="text" name="selasa" id="selasa" size="50" value="<?php echo get_option('selasa'); ?>"/><span class="description"> <?php _e('contoh, 08.00 - 21.00', 'weesata'); ?></span>
									    	</td>
										</tr>
										<tr valign="top">
									    	<td class="tl"><label for="rabu"><?php _e('RABU', 'weesata'); ?></label></td>
											<td>
										    	<input type="text" name="rabu" id="rabu" size="50" value="<?php echo get_option('rabu'); ?>"/><span class="description"> <?php _e('contoh, 08.00 - 21.00', 'weesata'); ?></span>
									    	</td>
										</tr>
										<tr valign="top">
									    	<td class="tl"><label for="kamis"><?php _e('KAMIS', 'weesata'); ?></label></td>
											<td>
										    	<input type="text" name="kamis" id="kamis" size="50" value="<?php echo get_option('kamis'); ?>"/><span class="description"> <?php _e('contoh, 08.00 - 21.00', 'weesata'); ?></span>
									    	</td>
										</tr>
										<tr valign="top">
									    	<td class="tl"><label for="jumat"><?php _e('JUMAT', 'weesata'); ?></label></td>
											<td>
										    	<input type="text" name="jumat" id="jumat" size="50" value="<?php echo get_option('jumat'); ?>"/><span class="description"> <?php _e('contoh, 08.00 - 21.00', 'weesata'); ?></span>
									    	</td>
										</tr>
										<tr valign="top">
									    	<td class="tl"><label for="sabtu"><?php _e('SABTU', 'weesata'); ?></label></td>
											<td>
										    	<input type="text" name="sabtu" id="sabtu" size="50" value="<?php echo get_option('sabtu'); ?>"/><span class="description"> <?php _e('contoh, 08.00 - 21.00', 'weesata'); ?></span>
									    	</td>
										</tr>
										<tr valign="top">
									    	<td class="tl"><label for="minggu"><?php _e('MINGGU', 'weesata'); ?></label></td>
											<td>
										    	<input type="text" name="minggu" id="minggu" size="50" value="<?php echo get_option('minggu'); ?>"/><span class="description"> <?php _e('contoh, 08.00 - 21.00', 'weesata'); ?></span>
									    	</td>
										</tr>
								    </table>
								</div>
		                	</div>
							
							<div id="change">
						    	<div class="btable">
							    	<table width="100%">
									<tr valign="top">
									    <td class="head" colspan="2">
											Pengaturan ganti text digunakan untuk mengganti beberapa text default, khususnya text judul / heading yang ada di halaman Beranda website.
										</td>
									</tr>
									<tr valign="top">
								    	<td class="tl"><label for="paket"><?php _e('Paling Populer', 'weesata'); ?></label></td>
										<td> 
									    	<input type="text" name="paket" id="paket" size="50" value="<?php echo get_option('paket'); ?>"/><span class="description"> <?php _e('contoh, Paket Terpopuler', 'weesata'); ?></span>
										</td>
									</tr>
									<tr valign="top">
								    	<td class="tl"><label for="pesan"><?php _e('Pesan Sekarang...', 'weesata'); ?></label></td>
										<td> 
										<?php $settings = array(
									    	'teeny' => true,
											'textarea_rows' => 4,
											'tabindex' => 1
											);
											$content = get_option('pesan');
											wp_editor(stripslashes($content), 'pesan', $settings);
										?>
										Replace text : <em>Pesan Sekarang, jangan sampe kehabisan Kuota !!!</em>
										</td>
									</tr>
									<tr valign="top">
								    	<td class="tl"><label for="deskripsi"><?php _e('Kami yang...', 'weesata'); ?></label></td>
										<td> 
										<?php $settings = array(
									    	'teeny' => true,
											'textarea_rows' => 4,
											'tabindex' => 1
											);
											$content = get_option('deskripsi');
											wp_editor(stripslashes($content), 'deskripsi', $settings);
										?>
										Replace text : <em>Kami selalu berikan promo-promo menarik, mengunjungi tempat-tempat indah yang akan memanjakan liburan Anda</em>
										</td>
									</tr>
									<tr valign="top">
								    	<td class="tl"><label for="lihat"><?php _e('Lihat Paket', 'weesata'); ?></label></td>
										<td> 
									    	<input type="text" name="lihat" id="lihat" size="50" value="<?php echo get_option('lihat'); ?>"/><span class="description"> <?php _e('contoh, Cek Paket', 'weesata'); ?></span>
										</td>
									</tr>
									<tr valign="top">
								    	<td class="tl"><label for="online"><?php _e('Online Hour', 'weesata'); ?></label></td>
										<td> 
									    	<input type="text" name="online" id="online" size="50" value="<?php echo get_option('online'); ?>"/><span class="description"> <?php _e('contoh, Jam Kerja', 'weesata'); ?></span>
										</td>
									</tr>
									<tr valign="top">
								    	<td class="tl"><label for="cek"><?php _e('Cek Agenda', 'weesata'); ?></label></td>
										<td> 
									    	<input type="text" name="cek" id="cek" size="50" value="<?php echo get_option('cek'); ?>"/><span class="description"> <?php _e('contoh, Lihat Agenda', 'weesata'); ?></span>
										</td>
									</tr>
									<tr valign="top">
								    	<td class="tl"><label for="berita"><?php _e('Berita Terbaru', 'weesata'); ?></label></td>
										<td> 
									    	<input type="text" name="berita" id="berita" size="50" value="<?php echo get_option('berita'); ?>"/><span class="description"> <?php _e('contoh, Artikel Terbaru', 'weesata'); ?></span>
										</td>
									</tr>
									<tr valign="top">
								    	<td class="tl"><label for="partner"><?php _e('Partner Kami', 'weesata'); ?></label></td>
										<td> 
									    	<input type="text" name="partner" id="partner" size="50" value="<?php echo get_option('partner'); ?>"/><span class="description"> <?php _e('contoh, Kepala Yayasan', 'weesata'); ?></span>
										</td>
									</tr>
								    <tr valign="top">
								    	<td class="tl"><label for="pesan"><?php _e('Private Tour...', 'weesata'); ?></label></td>
										<td> 
										<?php $settings = array(
									    	'teeny' => true,
											'textarea_rows' => 4,
											'tabindex' => 1
											);
											$content = get_option('private');
											wp_editor(stripslashes($content), 'private', $settings);
										?>
										Replace text : <em>Private Tour adalah....</em>
										</td>
									</tr>
									<tr valign="top">
								    	<td class="tl"><label for="footer"><?php _e('Footer / Copyright', 'weesata'); ?></label></td>
										<td> 
										<?php $settings = array(
									    	'teeny' => true,
											'textarea_rows' => 4,
											'tabindex' => 1
											);
											$content = get_option('footer');
											wp_editor(stripslashes($content), 'footer', $settings);
										?>
										Replace text : <em>Copyright © 2017 <?php bloginfo('name'); ?>. Didukung oleh WordPress. Tema weesata oleh Ciuss</em>
										</td>
									</tr>
				    		    	</table>
				    	    	</div>
				            </div>
				
							<div id="layout">
					    		<div class="btable">
	    			    			<table width="100%">
	    			    		    	<tr>
				    		    	    	<td class="tl"><label>Ganti Logo</label></td>
									     	<td>
									        	<img class="custom_media_logo custom-image" src="<?php echo get_option('logo_url'); ?>"/><br/>
								    			<!-- Upload button and text field -->
								    			<input class="custom_media_url" id="logo_url" type="text" name="logo_url" value="<?php echo get_option('logo_url'); ?>"> <a href="#" class="button custom_logo custom_media_upload">Upload Logo</a><br/>
							    				<span class="description"><strong>PENTING</strong> : Siapkan gambar logo 360x100 pixel atau skala kelipatannya. Gunakan gambar berformat <strong>PNG transparan</strong> (tembus pandang) karena header tema ini menggunakan background tembus pandang (opacity)</span>
												<br/><br/>
												Tema <strong>Weesata</strong> mempunyai header unik dengan sisi kiri memiliki background warna mengikuti style aksen tema. Misal memilih aksen hijau maka header sebelah kiri menjadi hijau. Namun jika ingin background sisi kiri polos berwarna putih silahkan pilih <strong>BACKGROUND WHITE</strong> pada menu <strong>LAYOUT & STYLE</strong> di sisi kiri.<br/><br/>
								    		</td>
							    		</tr>
							    		<tr>
							    	    	<td class="tl"><label>Ganti Favicon</label></td>
							    			<td>
							    		    	<img class="custom_media_icon custom-image" src="<?php echo get_option('favicon'); ?>"/><br/>
								    			<!-- Upload button and text field -->
								    			<input class="custom_media_url" id="favicon" type="text" name="favicon" value="<?php echo get_option('favicon'); ?>"> <a href="#" class="button custom_icon custom_media_upload">Upload Favicon</a><br/>
								    			<span class="description"><strong>PENTING</strong> : Untuk favicon (ikon header website) pergunakan gambar persegi dengan ukuran 1:1 (misal 64x64 pixel) dan format jpg, gif, atau png</span>
								    		</td>
							    		</tr>
										<tr>
				    		    		<td class="tl"><label>Overlay Background</label></td>
										<td>
									    	<img class="custom_media_bg custom-image" src="<?php echo get_option('background'); ?>"/><br/>
								    			<!-- Upload button and text field -->
								    			<input class="custom_media_url" id="background" type="text" name="background" value="<?php echo get_option('background'); ?>"> <a href="#" class="button custom_bg custom_media_upload">Upload Logo</a><br/>
							    				<span class="description"><strong>PENTING</strong> : Siapkan gambar logo 360x100 pixel atau skala kelipatannya. Gunakan gambar berformat <strong>PNG transparan</strong> (tembus pandang) karena header tema ini menggunakan background tembus pandang (opacity)</span>
								    		</td>
							    		</tr>
						    		</table>
					    		</div>
					    	</div>
			
					    	<div id="styles">
					    		<div class="btable">
	    			    			<table width="100%">
					    			    <tr>
					    			    	<td class="tl"><label>Layout Tema</label></td>
						    				<td>
						    			    	<div style="width: 50%; float: left;">
											    	<input type="radio" name="boxed" value="noboxed" <?php echo (get_option('boxed') == 'noboxed') ? 'checked="checked"' : ''; ?>/><span class="bold"><?php _e('Full-width', 'weesata'); ?></span> 
												</div>
												<div style="width: 50%; float: left;">
											    	<input type="radio" name="boxed" value="boxed" <?php echo (get_option('boxed') == 'boxed') ? 'checked="checked"' : ''; ?>/><span class="bold"><?php _e('Boxed', 'weesata'); ?></span>
												</div>
							    			</td>
							    		</tr>
										<tr>
				    		    	    	<td class="tl"><label>Boxed Background</label></td>
									    	<td>
									    	<img class="custom_box_bg custom-image" src="<?php echo get_option('boxedbg'); ?>"/><br/>
								    			<!-- Upload button and text field -->
								    			<input class="custom_media_url" id="boxedbg" type="text" name="boxedbg" value="<?php echo get_option('boxedbg'); ?>"> <a href="#" class="button box_bg custom_media_upload">Upload Gambar</a><br/>
							    				<span class="description"><strong>PENTING</strong> : Gambar background boxed hanya akan muncul jika website diatur pada mode Boxed. Lengkapi juga pengaturan dibawah ini. BG Position, BG Repeat, dan BG Size</span>
								    		</td>
							    		</tr>
										<tr>
							    	    	<td class="tl"><label>BG Position</label></td>
							    			<td> 
								        		<div style="width: 33.33%; float: left;">
											    	<input type="radio" name="posisi" value="left" <?php echo (get_option('posisi') == 'left') ? 'checked="checked"' : ''; ?>/><span class="bold"><?php _e('Kiri', 'weesata'); ?></span>
												</div>
												<div style="width: 33.33%; float: left;">
											    	<input type="radio" name="posisi" value="center" <?php echo (get_option('posisi') == 'center') ? 'checked="checked"' : ''; ?>/><span class="bold"><?php _e('Tengah', 'weesata'); ?></span>
												</div>
												<div style="width: 33.33%; float: left;">
											    	<input type="radio" name="posisi" value="right" <?php echo (get_option('posisi') == 'right') ? 'checked="checked"' : ''; ?>/><span class="bold"><?php _e('Kanan', 'weesata'); ?></span>
												</div>
							    			</td>
							    		</tr>
										<tr>
							    	    	<td class="tl"><label>BG Repeat</label></td>
							    			<td> 
								        		<div style="width: 50%; float: left;">
											    	<input type="radio" name="ulang" value="repeat" <?php echo (get_option('ulang') == 'repeat') ? 'checked="checked"' : ''; ?>/><span class="bold"><?php _e('Berulang', 'weesata'); ?></span>
												</div>
												<div style="width: 50%; float: left;">
											    	<input type="radio" name="ulang" value="no-repeat" <?php echo (get_option('ulang') == 'no-repeat') ? 'checked="checked"' : ''; ?>/><span class="bold"><?php _e('Single', 'weesata'); ?></span>
												</div>
							    			</td>
							    		</tr>
										<tr>
							    	    	<td class="tl"><label>BG Size</label></td>
							    			<td> 
								        		<div style="width: 50%; float: left;">
											    	<input type="radio" name="ukuran" value="100%" <?php echo (get_option('ukuran') == '100%') ? 'checked="checked"' : ''; ?>/><span class="bold"><?php _e('Cover 100%', 'weesata'); ?></span>
												</div>
												<div style="width: 50%; float: left;">
											    	<input type="radio" name="ukuran" value="auto" <?php echo (get_option('ukuran') == 'auto') ? 'checked="checked"' : ''; ?>/><span class="bold"><?php _e('Auto Normal', 'weesata'); ?></span>
												</div>
							    			</td>
							    		</tr>
										<tr>
							    	    	<td class="tl"><label>BG Attachment</label></td>
							    			<td> 
								        		<div style="width: 50%; float: left;">
											    	<input type="radio" name="bgfixed" value="fixed" <?php echo (get_option('bgfixed') == 'fixed') ? 'checked="checked"' : ''; ?>/><span class="bold"><?php _e('BG Fixed', 'weesata'); ?></span>
												</div>
												<div style="width: 50%; float: left;">
											    	<input type="radio" name="bgfixed" value="scroll" <?php echo (get_option('bgfixed') == 'scroll') ? 'checked="checked"' : ''; ?>/><span class="bold"><?php _e('BG Scroll', 'weesata'); ?></span>
												</div>
							    			</td>
							    		</tr>
										<tr>
				    		    	    	<td class="tl"><label>Background White</label></td>
											<td>
											    <div style="width: 50%; float: left;">
											    	<input type="radio" name="white" value="white" <?php echo (get_option('white') == 'white') ? 'checked="checked"' : ''; ?>/><span class="bold"><?php _e('Header White', 'weesata'); ?></span>
												</div>
												<div style="width: 50%; float: left;">
											    	<input type="radio" name="white" value="normal" <?php echo (get_option('white') == 'normal') ? 'checked="checked"' : ''; ?>/><span class="bold"><?php _e('Header Aksen', 'weesata'); ?></span>
												</div>
												<div class="clear">
												    <br/>
								    		    	<span class="description"><strong>PENTING</strong> : Memilih background Header kiri dengan warna Putih polos atau Aksen mengikuti tema</span>
								    		        <br/><br/>
												</div>
											</td>
							    		</tr>
										<tr>
							    	    	<td class="tl"><label>Menu Mobile</label></td>
							    			<td> 
								        		<div style="width: 50%; float: left;">
											    	<input type="radio" name="fixed" value="nofixed" <?php echo (get_option('fixed') == 'nofixed') ? 'checked="checked"' : ''; ?>/><span class="bold"><?php _e('Scroll Menu', 'weesata'); ?></span>
												</div>
												<div style="width: 50%; float: left;">
											    	<input type="radio" name="fixed" value="fixed" <?php echo (get_option('fixed') == 'fixed') ? 'checked="checked"' : ''; ?>/><span class="bold"><?php _e('Fixed Menu', 'weesata'); ?></span>
												</div>
							    			</td>
							    		</tr>
										<tr>
							    	    	<td class="tl"><label>Header Transparan</label></td>
							    			<td> 
								        		<div style="width: 50%; float: left;">
											    	<input type="radio" name="transparan" value="transparan" <?php echo (get_option('transparan') == 'transparan') ? 'checked="checked"' : ''; ?>/><span class="bold"><?php _e('Transparan', 'weesata'); ?></span>
												</div>
												<div style="width: 50%; float: left;">
											    	<input type="radio" name="fullblack" value="fullblack" <?php echo (get_option('transparan') == 'fullblack') ? 'checked="checked"' : ''; ?>/><span class="bold"><?php _e('Full Black', 'weesata'); ?></span>
												</div>
							    			</td>
							    		</tr>
	    					    		<tr>
				 			    	      	<td class="tl"><label>Warna Tema</label></td>
							    			<td>
											    <div class="w20">
    												<div class="pw donker">
								    	        	<input type="radio" name="weestyle" value="donker" <?php echo (get_option('weestyle') == 'donker') ? 'checked="checked"' : ''; ?>/><span class="bold"><?php _e('Donker', 'weesata'); ?></span>
											    	</div>
												</div>
												<div class="w20">
    												<div class="pw blue">
								    	        	<input type="radio" name="weestyle" value="blue" <?php echo (get_option('weestyle') == 'blue') ? 'checked="checked"' : ''; ?>/><span class="bold"><?php _e('Blue', 'weesata'); ?></span>
													</div>
												</div>
												<div class="w20">
    												<div class="pw green">
								    	        	<input type="radio" name="weestyle" value="green" <?php echo (get_option('weestyle') == 'green') ? 'checked="checked"' : ''; ?>/><span class="bold"><?php _e('Green', 'weesata'); ?></span>
													</div>
												</div>
												<div class="w20">
    												<div class="pw grey">
								    	        	<input type="radio" name="weestyle" value="default" <?php echo (get_option('weestyle') == 'default') ? 'checked="checked"' : ''; ?>/><span class="bold"><?php _e('Default', 'weesata'); ?></span>
													</div>
												</div>
												<div class="w20">
    												<div class="pw orange">
								    	        	<input type="radio" name="weestyle" value="orange" <?php echo (get_option('weestyle') == 'orange') ? 'checked="checked"' : ''; ?>/><span class="bold"><?php _e('Orange', 'weesata'); ?></span>
													</div>
												</div>
												<div class="w20">
    												<div class="pw purple">
								    	        	<input type="radio" name="weestyle" value="purple" <?php echo (get_option('weestyle') == 'purple') ? 'checked="checked"' : ''; ?>/><span class="bold"><?php _e('Purple', 'weesata'); ?></span>
													</div>
												</div>
												<div class="w20">
    												<div class="pw red">
								    	        	<input type="radio" name="weestyle" value="red" <?php echo (get_option('weestyle') == 'red') ? 'checked="checked"' : ''; ?>/><span class="bold"><?php _e('Red', 'weesata'); ?></span>
													</div>
												</div>
							    	    	</td>
							        	</tr>
						        	</table>
						    	</div>
					    	</div>
				
				
				
		            	</div>
	            	</div>
            	</div>
		    </form>
			
		</div>
	</div>
	<?php } ?>
