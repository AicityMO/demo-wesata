<?php 

add_action( 'init', 'create_post_type' );

function create_post_type() {

/*** POST TYPE SLIDER HOMEPAGE ***/		

	register_post_type( 'slider',		
	array(			
	    'menu_icon' => 'dashicons-images-alt2',
	    'labels' => array(				
	    'name' => __( 'Slideshow' ),				
	    'singular_name' => __( 'Slideshow' ),        
	    'add_new' => __( 'Tambah Slide?' ),	
	    'add_new_item' => __( 'Tambah Slide' ),	
	    'edit' => __( 'Edit' ),	 
	    'edit_item' => __( 'Edit Slide' ),	
	    'new_item' => __( 'Slide Baru' ),	
	    'view' => __( 'Lihat Slide' ),	
	    'view_item' => __( 'Lihat Slide' ),	
	    'search_items' => __( 'Cari Slide' ),	
	    'not_found' => __( 'Tidak ada Slide ditemukan' ),	
	    'not_found_in_trash' => __( 'Tidak ada Slide di folder Trash' ),	
	    'parent' => __( 'Parent Super Duper' ),			
	    ),		                	
		'public' => true,           					            
		'has_archive' => true,        			            
		'supports' => array( 'title', 'thumbnail'),        			            
		'exclude_from_search' => false, 	 
		 )	
    );
	
// SEKILAS INFO

	register_post_type( 'sekilas-info',		
	array(			
	    'menu_icon' => 'dashicons-controls-back',
	    'labels' => array(				
	    'name' => __( 'Sekilas Info' ),				
	    'singular_name' => __( 'Sekilas Info' ),        
	    'add_new' => __( 'Data info baru?' ),	
	    'add_new_item' => __( 'Tambah info baru' ),	
	    'edit' => __( 'Edit' ),	 
	    'edit_item' => __( 'Edit info' ),	
	    'new_item' => __( 'Item Baru' ),	
	    'view' => __( 'Lihat info' ),	
	    'view_item' => __( 'Lihat info' ),	
	    'search_items' => __( 'Cari info' ),	
	    'not_found' => __( 'Tidak ada info ditemukan' ),	
	    'not_found_in_trash' => __( 'Tidak ada info di folder Trash' ),	
	    'parent' => __( 'Parent Super Duper' ),			
	    ),		                	
		'public' => true,           					            
		'has_archive' => true,        			            
		'supports' => array( 'editor'),        			            
		'exclude_from_search' => false, 	 
		 )	
    );	

/*** POST TYPE PAKET TOUR ***/		

	register_post_type( 'paket',		
	array(			
	    'menu_icon' => 'dashicons-index-card',
	    'labels' => array(				
	    'name' => __( 'Paket Pilihan' ),				
	    'singular_name' => __( 'Paket Pilihan' ),        
	    'add_new' => __( 'Tambah Paket?' ),	
	    'add_new_item' => __( 'Tambah Paket' ),	
	    'edit' => __( 'Edit' ),	 
	    'edit_item' => __( 'Edit Paket' ),	
	    'new_item' => __( 'Paket Baru' ),	
	    'view' => __( 'Lihat Paket' ),	
	    'view_item' => __( 'Lihat Paket' ),	
	    'search_items' => __( 'Cari Paket' ),	
	    'not_found' => __( 'Tidak ada Paket ditemukan' ),	
	    'not_found_in_trash' => __( 'Tidak ada Paket di folder Trash' ),	
	    'parent' => __( 'Parent Super Duper' ),			
	    ),		                	
		'public' => true,           					            
		'has_archive' => true,        			            
		'supports' => array( 'title', 'editor', 'thumbnail', 'comments'),        			            
		'exclude_from_search' => false, 	 
		 )	
    );
	
 
    add_action('admin_init', 'paketour', 1);
	function paketour() {
	    add_meta_box('paketour_jadwal', 'Data Paket Tour', 'paketour_jadwal', 'paket', 'normal', 'default');
	}

	function paketour_jadwal() {
	    global $post;
	    echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' .
	    wp_create_nonce( plugin_basename(__FILE__) ) . '" />';

	    $quota = get_post_meta($post->ID, '_quota', true);
		$single = get_post_meta($post->ID, '_single', true);
		$couple = get_post_meta($post->ID, '_couple', true);
		$triple = get_post_meta($post->ID, '_triple', true);
		$quad = get_post_meta($post->ID, '_quad', true);
		$single1 = get_post_meta($post->ID, '_single1', true);
		$couple1 = get_post_meta($post->ID, '_couple1', true);
		$triple1 = get_post_meta($post->ID, '_triple1', true);
		$quad1 = get_post_meta($post->ID, '_quad1', true);
		
		$dsingle = get_post_meta($post->ID, '_dsingle', true);
		$dcouple = get_post_meta($post->ID, '_dcouple', true);
		$dtriple = get_post_meta($post->ID, '_dtriple', true);
		$dquad = get_post_meta($post->ID, '_dquad', true);
		$dsingle1 = get_post_meta($post->ID, '_dsingle1', true);
		$dcouple1 = get_post_meta($post->ID, '_dcouple1', true);
		$dtriple1 = get_post_meta($post->ID, '_dtriple1', true);
		$dquad1 = get_post_meta($post->ID, '_dquad1', true);
		$diskon = get_post_meta($post->ID, '_diskon', true);
		$expidi = get_post_meta($post->ID, '_expidi', true);

		echo '<p>Quota Peserta (<em>jumlah peserta dalam satu tour</em>)</p>';
	    echo '<input type="text" name="_quota" value="' . $quota  . '" class="widefat" />';
		echo '<div class="onoff"><span class="dd button-primary">Promo Diskon</span><span class="hd button-primary">Batalkan Diskon</span>';
		echo '<input type="hidden" name="_diskon" value="'.$diskon.'" class="diskon hid widefat" />';
		echo '<p class="tt">Harga untuk dewasa (<em>masukkan format angka, contoh 10.000.000</em>).</p>';
		echo '<table class="tt full">
			    	<tr>
                		<td>
						    <div class="quart">
							    <div class="halfin">
					    	    	<input type="text" placeholder="Harga Single" name="_single" value="' . $single  . '" class="widefat" />
								</div>
							</div>
							<div class="quart">
							    <div class="halfin">
							    	<input type="text" placeholder="Harga Couple" name="_couple" value="' . $couple  . '" class="widefat" />
								</div>
							</div>
							<div class="quart">
							    <div class="halfin">
							    	<input type="text" placeholder="Harga Triple" name="_triple" value="' . $triple  . '" class="widefat" />
								</div>
							</div>
							<div class="quart">
							    <div class="halfin">
							    	<input type="text" placeholder="Harga Quad" name="_quad" value="' . $quad  . '" class="widefat" />
								</div>
							</div>
						</td>
					</tr>
			   </table>';
		echo '<p class="dh '.$diskon.'">Batas waktu Diskon (<em>masukkan tanggal waktu diskon berakhir</em>)</p>';
        echo '<input type="text" name="_expidi" value="'.$expidi.'" class="dh expidi widefat" />';
        echo '<p class="dh '.$diskon.'">Harga diskon dewasa (<em>masukkan format angka, contoh 8.000.000</em>)</p>';
		echo '<table class="dh full">
			    	<tr>
					    <td>
						    <div class="quart">
							    <div class="halfin">
					    	    	<input type="text" placeholder="Diskon Single" name="_dsingle" value="' . $dsingle  . '" class="diskon widefat" />
								</div>
							</div>
							<div class="quart">
							    <div class="halfin">
							    	<input type="text" placeholder="Diskon Couple" name="_dcouple" value="' . $dcouple  . '" class="diskon widefat" />
								</div>
							</div>
							<div class="quart">
							    <div class="halfin">
							    	<input type="text" placeholder="Diskon Triple" name="_dtriple" value="' . $dtriple  . '" class="diskon widefat" />
								</div>
							</div>
							<div class="quart">
							    <div class="halfin">
							    	<input type="text" placeholder="Diskon Quad" name="_dquad" value="' . $dquad  . '" class="diskon widefat" />
								</div>
							</div>					
						</td>
					</tr>
			   </table>';
			   
		echo '<p class="tt">Harga untuk anak (<em>masukkan format angka, contoh 7.000.000</em>).</p>';
		echo '<table class="tt full">
			    	<tr>
					    <td>
						    <div class="quart">
							    <div class="halfin">
					    	    	<input type="text" placeholder="Harga Single" name="_single1" value="' . $single1  . '" class="widefat" />
								</div>
							</div>
							<div class="quart">
							    <div class="halfin">
							    	<input type="text" placeholder="Harga Couple" name="_couple1" value="' . $couple1  . '" class="widefat" />
								</div>
							</div>
							<div class="quart">
							    <div class="halfin">
							    	<input type="text" placeholder="Harga Triple" name="_triple1" value="' . $triple1  . '" class="widefat" />
								</div>
							</div>
							<div class="quart">
							    <div class="halfin">
							    	<input type="text" placeholder="Harga Quad" name="_quad1" value="' . $quad1  . '" class="widefat" />
								</div>
							</div>					
						</td>
					</tr>
			   </table>';
		echo '<p class="ah '.$diskon.'">Harga diskon anak (<em>masukkan format angka, contoh 5.000.000</em>)</p>';
		echo '<table class="ah full">
			    	<tr>
					    <td>
						    <div class="quart">
							    <div class="halfin">
					    	    	<input type="text" placeholder="Diskon Single" name="_dsingle1" value="' . $dsingle1  . '" class="diskon widefat" />
								</div>
							</div>
							<div class="quart">
							    <div class="halfin">
							    	<input type="text" placeholder="Diskon Couple" name="_dcouple1" value="' . $dcouple1  . '" class="diskon widefat" />
								</div>
							</div>
							<div class="quart">
							    <div class="halfin">
							    	<input type="text" placeholder="Diskon Triple" name="_dtriple1" value="' . $dtriple1  . '" class="diskon widefat" />
								</div>
							</div>
							<div class="quart">
							    <div class="halfin">
							    	<input type="text" placeholder="Diskon Quad" name="_dquad1" value="' . $dquad1 . '" class="diskon widefat" />
								</div>
							</div>					
						</td>
					</tr>
			   </table>
			</div>';
			
			        
		wp_enqueue_script('jquery-ui-datepicker');
		wp_enqueue_style('jquery-ui-css', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
			
	    wp_enqueue_style( 'wee-package', get_template_directory_uri() . '/css/package.css', true);
	?>
	
		
	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.min.js"></script>
	
	<script type="text/javascript">
    		jQuery(document).ready(function($) {
			    $('.expidi').datepicker({
				dateFormat : 'dd-mm-yy' });
			});
	</script>
	
	<?php if ($diskon == 'disc') { ?>
        	<script type="text/javascript">
		    	$("document").ready(function($){
				    $(".hd").show();
					$(".dh, .ah").show();
					$(".dd, .tt").hide();
					$(".hd").click(function(){
				    	$(".dh, .ah").hide();
						$(".dd, .tt").show();
						$(".hd").hide();
						$('.diskon, .expidi').val('');
					});
					$(".dd").click(function(){
				    	$(".dh, .ah").show();
						$(".dd, .tt").hide();
						$(".hd").show();
						$('.hid').val('disc');
					});
				});
			</script>
	<?php } else { ?>
        	<script type="text/javascript">
		    	$("document").ready(function($){
				    $(".hd, .dh, .ah").hide();
					$(".dd").click(function(){
				    	$(".dh, .ah").show();
						$(".dd, .tt").hide();
						$(".hd").show();
						$('.hid').val('disc');
					});
					$(".hd").click(function(){
				    	$(".dh, .ah").hide();
						$(".dd, .tt").show();
						$(".hd").hide();
						$('.diskon, .expidi').val('');
					});
				});
			</script>
	<?php } 
		
	}

	function paketour_meta($post_id, $post) {
	    if ( !wp_verify_nonce( $_POST['eventmeta_noncename'], plugin_basename(__FILE__) )) {
	    return $post->ID;
	    }

	    if ( !current_user_can( 'edit_post', $post->ID ))
	        return $post->ID;

	    $events_meta['_quota'] = $_POST['_quota'];
		$events_meta['_single'] = $_POST['_single'];
		$events_meta['_couple'] = $_POST['_couple'];
		$events_meta['_triple'] = $_POST['_triple'];
		$events_meta['_quad'] = $_POST['_quad'];
		$events_meta['_single1'] = $_POST['_single1'];
		$events_meta['_couple1'] = $_POST['_couple1'];
		$events_meta['_triple1'] = $_POST['_triple1'];
		$events_meta['_quad1'] = $_POST['_quad1'];
		
		$events_meta['_dsingle'] = $_POST['_dsingle'];
		$events_meta['_dcouple'] = $_POST['_dcouple'];
		$events_meta['_dtriple'] = $_POST['_dtriple'];
		$events_meta['_dquad'] = $_POST['_dquad'];
		$events_meta['_dsingle1'] = $_POST['_dsingle1'];
		$events_meta['_dcouple1'] = $_POST['_dcouple1'];
		$events_meta['_dtriple1'] = $_POST['_dtriple1'];
		$events_meta['_dquad1'] = $_POST['_dquad1'];
		$events_meta['_diskon'] = $_POST['_diskon'];
		$events_meta['_expidi'] = $_POST['_expidi'];

	    foreach ($events_meta as $key => $value) {      
		    if( $post->post_type == 'revision' ) return;
	        $value = implode(',', (array)$value); 
	        if(get_post_meta($post->ID, $key, FALSE)) { 
	            update_post_meta($post->ID, $key, $value);
	        } else { 
	            add_post_meta($post->ID, $key, $value);
	        }
	        if(!$value) delete_post_meta($post->ID, $key); 
	    }

	}
	add_action('save_post', 'paketour_meta', 1, 2); 
	
    add_action('admin_init', 'videotour', 1);
	
	function videotour() {
	    add_meta_box('vidtour', 'Video Paket Tour', 'vidtour', 'paket', 'normal', 'default');
	}

	function vidtour() {
	    global $post;
	    echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' .
	    wp_create_nonce( plugin_basename(__FILE__) ) . '" />';

	    $embed = get_post_meta($post->ID, '_embed', true);

		echo '<p>Masukkan ID Video Youtube</p>';
	    echo '<input type="text" name="_embed" value="' . $embed  . '" class="widefat" />';
		if ($embed == !'') {
		echo '<p>Preview Video</p>';
		echo '<iframe style="width: 100%; height: 400px;" src="https://www.youtube.com/embed/' . $embed  . '" frameborder="0" allowfullscreen></iframe>';
        } 
	}

	function vidtour_meta($post_id, $post) {
	    if ( !wp_verify_nonce( $_POST['eventmeta_noncename'], plugin_basename(__FILE__) )) {
	    return $post->ID;
	    }

	    if ( !current_user_can( 'edit_post', $post->ID ))
	        return $post->ID;
		
		$events_meta['_embed'] = $_POST['_embed'];

	    foreach ($events_meta as $key => $value) {      
		    if( $post->post_type == 'revision' ) return;
	        $value = implode(',', (array)$value); 
	        if(get_post_meta($post->ID, $key, FALSE)) { 
	            update_post_meta($post->ID, $key, $value);
	        } else { 
	            add_post_meta($post->ID, $key, $value);
	        }
	        if(!$value) delete_post_meta($post->ID, $key); 
	    }

	}
	add_action('save_post', 'vidtour_meta', 1, 2); 
	
	add_action('admin_init', 'hhs_add_meta_boxes', 1);
	function hhs_add_meta_boxes() {
    	add_meta_box( 'repeatable-fields', 'Itinerary / Detail Perjalanan', 'hhs_repeatable_meta_box_display', 'paket', 'normal', 'default');
    }

	function hhs_repeatable_meta_box_display() {
    	global $post;
    	$repeatable_fields = get_post_meta($post->ID, 'repeatable_fields', true);
    	wp_nonce_field( 'hhs_repeatable_meta_box_nonce', 'hhs_repeatable_meta_box_nonce' );
    	?>
    
    	<script type="text/javascript">
     	jQuery(document).ready(function( $ ){
    		$( '#add-row' ).on('click', function() {
			var row = $( '.empty-row.screen-reader-text' ).clone(true);
			row.removeClass( 'empty-row screen-reader-text' );
			row.insertBefore( '#repeatable-fieldset-one tbody>tr:last' );
			return false;
		});
  	
		$( '.remove-row' ).on('click', function() {
			$(this).parents('tr').remove();
			return false;
		});
    	});
    	</script>
  
    	<table id="repeatable-fieldset-one">
			
         	<?php if ( $repeatable_fields ) :
			foreach ( $repeatable_fields as $field ) { ?>
             	<tr>
            		<td>
					    <div class="clear">
						    <div class="half">
							    <div class="halfin">
				                	<input type="text" placeholder="Hari ke.." class="widefat" name="hari[]" value="<?php if($field['hari'] != '') echo esc_attr( $field['hari'] ); ?>" />
								</div>
							</div>
							<div class="half">
							    <div class="halfin">
				                	<input type="text" placeholder="Destinasi.." class="widefat" name="dest[]" value="<?php if ($field['dest'] != '') echo esc_attr( $field['dest'] ); ?>" />
								</div>
							</div>
							<div class="half">
							    <div class="halfin">
				                	<input type="text" placeholder="Jam (waktu).." class="widefat" name="jam[]" value="<?php if ($field['jam'] != '') echo esc_attr( $field['jam'] ); ?>" />
								</div>
							</div>
						</div>
						<div class="full">
						    <div class="halfin">
					            <textarea placeholder="Detail.." class="widefat" name="det[]"><?php if($field['det'] != '') echo esc_attr( $field['det'] ); ?></textarea>
							</div>
						</div>
		    			<a class="button remove-row" href="#">Hapus</a><br/><br/>
					</td>
				</tr>
				
			<?php } else : ?>
			
	    		<tr>
            		<td>
					    <div class="clear">
						    <div class="half">
							    <div class="halfin">
			    	            	<input type="text" class="widefat" name="hari[]" />
								</div>
							</div>
							<div class="half">
							    <div class="halfin">
						        	<input type="text" class="widefat" name="dest[]" />
							    </div>
							</div>
							<div class="half">
							    <div class="halfin">
						        	<input type="text" class="widefat" name="jam[]" />
							    </div>
							</div>
						</div>
						<div class="full">
						    <div class="halfin">
					            <textarea placeholder="Detail.." class="widefat" name="det[]"></textarea>
							</div>
						</div>
				    	<a class="button remove-row" href="#">Hapus</a><br/><br/>
					</td>
				</tr>
				
			<?php endif; ?>
	
            	<!-- empty hidden one for jQuery -->
            	<tr class="empty-row screen-reader-text">
            		<td>
					    <div class="clear">
						    <div class="half">
							    <div class="halfin">
			    	            	<input type="text" class="widefat" name="hari[]" />
								</div>
							</div>
							<div class="half">
							    <div class="halfin">
						        	<input type="text" class="widefat" name="dest[]" />
							    </div>
							</div>
							<div class="half">
							    <div class="halfin">
						        	<input type="text" class="widefat" name="jam[]" />
							    </div>
							</div>
						</div>
						<div class="full">
						    <div class="halfin">
					            <textarea placeholder="Detail.." class="widefat" name="det[]"></textarea>
							</div>
						</div>
				    	<a class="button remove-row" href="#">Hapus</a><br/><br/>
					</td>
				</tr>
		</table>
		
		<div class="addnew"><a id="add-row" class="button button-primary button-large" href="#">Tambah Baru</a></div> 
			
	<?php
	
    }

	add_action('save_post', 'hhs_repeatable_meta_box_save');

	function hhs_repeatable_meta_box_save($post_id) {
    	if ( ! isset( $_POST['hhs_repeatable_meta_box_nonce'] ) ||
        	! wp_verify_nonce( $_POST['hhs_repeatable_meta_box_nonce'], 'hhs_repeatable_meta_box_nonce' ) )
	    	return;
	
    	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
	    	return;
	
    	if (!current_user_can('edit_post', $post_id))
	    	return;
	
    	$old = get_post_meta($post_id, 'repeatable_fields', true);
    	$new = array();
	
    	$haris = $_POST['hari'];
    	$dests = $_POST['dest'];
		$jams = $_POST['jam'];
		$dets = $_POST['det'];
	
    	$count = count( $haris );
	
    	for ( $i = 0; $i < $count; $i++ ) {
	    	if ( $haris[$i] != '' ) {
	    		$new[$i]['hari'] = stripslashes( strip_tags( $haris[$i] ) );
		
	    		if ( $dests[$i] == 'http://' ) {
	   			$new[$i]['dest'] = '';
				$new[$i]['jam'] = '';
				$new[$i]['det'] = '';
		        	} else {
				$new[$i]['dest'] = stripslashes( $dests[$i] ); 
				$new[$i]['jam'] = stripslashes( $jams[$i] ); 
				$new[$i]['det'] = stripslashes( $dets[$i] );
				}
	    	}
    	}
		
    	if ( !empty( $new ) && $new != $old )
    		update_post_meta( $post_id, 'repeatable_fields', $new );
    	elseif ( empty($new) && $old )
    		delete_post_meta( $post_id, 'repeatable_fields', $old );
	}

	
	function add_gallery_metabox($post_type) {
        add_meta_box( 'gallery-metabox', 'Gallery Slide', 'gallery_meta_callback', 'paket', 'normal', 'default');
	}
	
	add_action('add_meta_boxes', 'add_gallery_metabox');
	
	function gallery_meta_callback($post) {
        wp_nonce_field( basename(__FILE__), 'gallery_meta_nonce' );
        $ids = get_post_meta($post->ID, 'vdw_gallery_id', true);
    ?>
    
        <p>Gambar akan ditampilkan sebagai slide, harap upload gambar berukuran sama.</p>

        <ul id="gallery-metabox-list" class="clear">
            <?php if ($ids) : foreach ($ids as $key => $value) : $image = wp_get_attachment_image_src($value); ?>
			<li>
		    	<input type="hidden" name="vdw_gallery_id[<?php echo $key; ?>]" value="<?php echo $value; ?>">
                <img class="image-preview" src="<?php echo $image[0]; ?>">
		    	<a class="change-image " href="#" data-uploader-title="Ganti" data-uploader-button-text="Change image">Ganti</a>
                <a class="remove-image" href="#">Hapus</a>
			</li>
            <?php endforeach; endif; ?>
        </ul>
		
		<a class="gallery-add button button-primary" href="#" data-uploader-title="Add image(s) to gallery" data-uploader-button-text="Add image(s)">Tambah Gambar</a>

    	<script>
	    	jQuery(function($) {
		    	var file_frame;
				$(document).on('click', '#gallery-metabox a.gallery-add', function(e) {
			    	e.preventDefault();
					if (file_frame) file_frame.close();
					file_frame = wp.media.frames.file_frame = wp.media({
				    	title: $(this).data('uploader-title'),
						button: {
					    	text: $(this).data('uploader-button-text'),
						},
					multiple: true
					});
					
					file_frame.on('select', function() {
				    	var listIndex = $('#gallery-metabox-list li').index($('#gallery-metabox-list li:last')),
						selection = file_frame.state().get('selection');
						selection.map(function(attachment, i) {
					    	attachment = attachment.toJSON(),
							index      = listIndex + (i + 1);
							
							$('#gallery-metabox-list').append('<li><input type="hidden" name="vdw_gallery_id[' + index + ']" value="' + attachment.id + '"><img class="image-preview" src="' + attachment.sizes.thumbnail.url + '"><a class="change-image" href="#" data-uploader-title="Ganti" data-uploader-button-text="Change image">Ganti</a><a class="remove-image" href="#">Hapus</a></li>');
						});
					});
					
					makeSortable();
					file_frame.open();
				});
				
				$(document).on('click', '#gallery-metabox a.change-image', function(e) {
			    	e.preventDefault();
					var that = $(this);
					if (file_frame) file_frame.close();
					file_frame = wp.media.frames.file_frame = wp.media({
			    		title: $(this).data('uploader-title'),
						button: {
					    	text: $(this).data('uploader-button-text'),
						},
				    	multiple: false
	    			});
					
					file_frame.on( 'select', function() {
				    	attachment = file_frame.state().get('selection').first().toJSON();
						that.parent().find('input:hidden').attr('value', attachment.id);
						that.parent().find('img.image-preview').attr('src', attachment.sizes.thumbnail.url);
					});
					
					file_frame.open();
				});
				
				function resetIndex() {
			    	$('#gallery-metabox-list li').each(function(i) {
				    	$(this).find('input:hidden').attr('name', 'vdw_gallery_id[' + i + ']');
			    	});
		    	}
			
	    		function makeSortable() {
		        	$('#gallery-metabox-list').sortable({ 
		    	    	opacity: 0.6,
		    			stop: function() {
		    	    		resetIndex();
			    		}
			    	});
		    	}
			
		    	$(document).on('click', '#gallery-metabox a.remove-image', function(e) {
		        	e.preventDefault();
			    	$(this).parents('li').animate({ opacity: 0 }, 200, function() {
	    	    		$(this).remove();
			    		resetIndex();
			    	});
		    	});
			
		     	makeSortable();
	    	});
    	</script>
	
	<?php }
	
	function gallery_meta_save($post_id) {
        if (!isset($_POST['gallery_meta_nonce']) || !wp_verify_nonce($_POST['gallery_meta_nonce'], basename(__FILE__))) return;
        if (!current_user_can('edit_post', $post_id)) return;
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return;
        if(isset($_POST['vdw_gallery_id'])) {
		    update_post_meta($post_id, 'vdw_gallery_id', $_POST['vdw_gallery_id']);
		} else {
		    delete_post_meta($post_id, 'vdw_gallery_id');
		}
	}
	
	add_action('save_post', 'gallery_meta_save');

	
/*** POST TYPE BROSUR ***/
	
	register_post_type( 'brosur',		
	array(			
	    'menu_icon' => 'dashicons-arrow-down-alt2',
	    'labels' => array(				
	    'name' => __( 'Brosur Tour' ),				
	    'singular_name' => __( 'Brosur Tour' ),        
	    'add_new' => __( 'Tambah Brosur?' ),	
	    'add_new_item' => __( 'Tambah Brosur' ),	
	    'edit' => __( 'Edit' ),	 
	    'edit_item' => __( 'Edit Brosur' ),	
	    'new_item' => __( 'Brosur Baru' ),	
	    'view' => __( 'Lihat Brosur' ),	
	    'view_item' => __( 'Lihat Brosur' ),	
	    'search_items' => __( 'Cari Brosur' ),	
	    'not_found' => __( 'Tidak ada Brosur ditemukan' ),	
	    'not_found_in_trash' => __( 'Tidak ada Brosur di folder Trash' ),	
	    'parent' => __( 'Parent Super Duper' ),			
	    ),		                	
		'public' => true,           					            
		'has_archive' => true,        			            
		'supports' => array( 'title', 'editor' ),        			            
		'exclude_from_search' => false, 
		'register_meta_box_cb' => 'add_brosur',
		 )	
    );
	

    function add_brosur() {
        add_meta_box('brosur_upload', 'Upload file untuk brosur, bisa berupa file gambar maupun file archive untuk di download oleh pengunjung website', 'brosur_upload', 'brosur', 'normal', 'default');
    }


    function brosur_upload() {
        global $post;
        echo '<input type="hidden" name="podcastmeta_noncename" id="podcastmeta_noncename" value="'.
        wp_create_nonce(plugin_basename(__FILE__)).
        '" />';
		
        global $wpdb;
        $dbrosur = get_post_meta($post -> ID, $key = 'brosur', true);
    	$sizebrosur = get_post_meta($post -> ID, $key = 'size', true);
    	$typebrosur = get_post_meta($post -> ID, $key = 'type', true);
    	$iconbrosur = get_post_meta($post -> ID, $key = 'icon', true);
        $media_file = get_post_meta($post -> ID, $key = '_wp_attached_file', true);
            if (!empty($media_file)) {
            $dbrosur = $media_file;
        } 
		
		?>


        <script type = "text/javascript">
	    	var file_frame;
			jQuery('#upload_image_button').live('click', function(podcast) {
		    	podcast.preventDefault();
				if (file_frame) {
                    file_frame.open();
                    return;
                }
				
				file_frame = wp.media.frames.file_frame = wp.media({
                    title: jQuery(this).data('uploader_title'),
                    button: {
                        text: jQuery(this).data('uploader_button_text'),
                    },
                    multiple: false
                });
				
				file_frame.on('select', function(){
			    	attachment = file_frame.state().get('selection').first().toJSON();
					var all = JSON.stringify( attachment );      
       		        var url = attachment.url;
       		        var subtype = attachment.subtype;
      		        var filesizeHumanReadable = attachment.filesizeHumanReadable;
      		        var icon = attachment.icon;
					var namefile = document.getElementById("brosur");
					var sizefile = document.getElementById("size");
	        		var typefile = document.getElementById("type");
	         		var iconfile = document.getElementById("icon");

        		    namefile.value = url; 
					sizefile.value = filesizeHumanReadable; 
					typefile.value = subtype; 
					iconfile.value = icon;
    		    });
				
				file_frame.open();
			});
		</script>
    
		<p>Unggah file melalui form di bawah ini atau pilih file dari Pustaka Media / Media Library </p>
		<input type="text" name="brosur" placeholder="tekan tombol upload dibawah..." id="brosur" value="<?php echo $dbrosur; ?>" class="widefat" />
		<input type="hidden" name="size" placeholder="tekan tombol upload dibawah..." id="size" value="<?php echo $sizebrosur; ?>" class="widefat" />
    	<input type="hidden" name="type" placeholder="tekan tombol upload dibawah..." id="type" value="<?php echo $typebrosur; ?>" class="widefat" />
    	<input type="hidden" name="icon" placeholder="tekan tombol upload dibawah..." id="icon" value="<?php echo $iconbrosur; ?>" class="widefat" />
		<p><input id ="upload_image_button" type="button" value="Upload" class="button button-primary button-large" /></p>
		<input type="hidden" name="img_txt_id" id="img_txt_id" value ="" />
    
		<?php
 	    function admin_scripts() {
	    	wp_enqueue_script('media-upload');
			wp_enqueue_script('thickbox');
		}
		
		function admin_styles() {
            wp_enqueue_style('thickbox');
		}
		
		add_action('admin_print_scripts', 'admin_scripts');
		add_action('admin_print_styles', 'admin_styles');
	}
	
	function save_podcasts_meta($post_id, $post) {
        if (!wp_verify_nonce($_POST['podcastmeta_noncename'], plugin_basename(__FILE__))) {
            return $post -> ID;
        }
		
		if (!current_user_can('edit_post', $post -> ID))
            return $post -> ID;
			$podcasts_meta['brosur'] = $_POST['brosur'];
			$podcasts_meta['size'] = $_POST['size'];
			$podcasts_meta['type'] = $_POST['type'];
			$podcasts_meta['icon'] = $_POST['icon'];
			
			foreach($podcasts_meta as $key => $value) {
     	        if ($post -> post_type == 'revision') return;
                $value = implode(',', (array) $value);
                if (get_post_meta($post -> ID, $key, FALSE)) { 
                    update_post_meta($post -> ID, $key, $value);
                } else { 
                    add_post_meta($post -> ID, $key, $value);
                }
                if (!$value) delete_post_meta($post -> ID, $key); 
            }
	}
		
	add_action('save_post', 'save_podcasts_meta', 1, 2); 
	
/*** POST TYPE AGENDA ***/
	
	register_post_type( 'schedule',		
	array(			
	    'menu_icon' => 'dashicons-calendar-alt',
	    'labels' => array(				
	    'name' => __( 'Jadwal Tour' ),				
	    'singular_name' => __( 'Jadwal Tour' ),        
	    'add_new' => __( 'Tambah Jadwal?' ),	
	    'add_new_item' => __( 'Tambah Jadwal' ),	
	    'edit' => __( 'Edit' ),	 
	    'edit_item' => __( 'Edit Jadwal' ),	
	    'new_item' => __( 'Jadwal Baru' ),	
	    'view' => __( 'Lihat Jadwal' ),	
	    'view_item' => __( 'Lihat Jadwal' ),	
	    'search_items' => __( 'Cari Jadwal' ),	
	    'not_found' => __( 'Tidak ada Jadwal ditemukan' ),	
	    'not_found_in_trash' => __( 'Tidak ada Jadwal di folder Trash' ),	
	    'parent' => __( 'Parent Super Duper' ),			
	    ),		                	
		'public' => true,           					            
		'has_archive' => true,        			            
		'supports' => array( 'title', 'editor', 'thumbnail'),        			            
		'exclude_from_search' => false, 	 
		'register_meta_box_cb' => 'weevents',
		 )	
    );
	
	function weevents() {
	    add_meta_box('wee_events', 'Data Schedule Tour', 'wee_events', 'schedule', 'normal', 'default');
	}

	function wee_events() {
	    global $post;
	    echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' .
	    wp_create_nonce( plugin_basename(__FILE__) ) . '" />';
		
		$minus = strtotime(get_post_meta($post->ID, '_going', true));
	    $going = get_post_meta($post->ID, '_going', true);
		$back = get_post_meta($post->ID, '_back', true);
		$jam = get_post_meta($post->ID, '_jam', true);
		$guide = get_post_meta($post->ID, '_guide', true);
		$photo = get_post_meta($post->ID, '_photo', true);
		$rute = get_post_meta($post->ID, '_rute', true);
		$maskapai = get_post_meta($post->ID, '_maskapai', true);
		$hotel = get_post_meta($post->ID, '_hotel', true);
		$rentcar = get_post_meta($post->ID, '_rentcar', true);
		$today=date("Y-m-d");

			echo '<table class="full">
			    	<tr>
					    <td style="vertical-align: top;">
						<div class="clear">
						    <div class="half">
							    <div class="halfin">
								    <p>Tanggal Berangkat</p>
			    	            	<input type="text" name="_going" value="' . $going  . '" class="going widefat" />
								</div>
							</div>
							<div class="half">
							    <div class="halfin">
								    <p>Jam Berangkat</p>';
?>
	    	<select name="_jam" id="jam" class="widefat">
		    <option value="00:00" <?php selected( $jam, '00:00' ); ?>>00:00</option>
	    	<option value="01:00" <?php selected( $jam, '01:00' ); ?>>01:00</option>
			<option value="02.00" <?php selected( $jam, '02:00' ); ?>>02:00</option>
			<option value="03.00" <?php selected( $jam, '03:00' ); ?>>03:00</option>
			<option value="04:00" <?php selected( $jam, '04:00' ); ?>>04:00</option>
			<option value="05:00" <?php selected( $jam, '05:00' ); ?>>05:00</option>
			<option value="06.00" <?php selected( $jam, '06:00' ); ?>>06:00</option>
	    	<option value="07:00" <?php selected( $jam, '07:00' ); ?>>07:00</option>
			<option value="08.00" <?php selected( $jam, '08:00' ); ?>>08:00</option>
			<option value="09.00" <?php selected( $jam, '09:00' ); ?>>09:00</option>
			<option value="10:00" <?php selected( $jam, '10:00' ); ?>>10:00</option>
	    	<option value="11:00" <?php selected( $jam, '11:00' ); ?>>11:00</option>
			<option value="12.00" <?php selected( $jam, '12:00' ); ?>>12:00</option>
			<option value="13.00" <?php selected( $jam, '13:00' ); ?>>13:00</option>
			<option value="14:00" <?php selected( $jam, '14:00' ); ?>>14:00</option>
			<option value="15.00" <?php selected( $jam, '15:00' ); ?>>15:00</option>
	    	<option value="16:00" <?php selected( $jam, '16:00' ); ?>>16:00</option>
			<option value="17.00" <?php selected( $jam, '17:00' ); ?>>17:00</option>
			<option value="18.00" <?php selected( $jam, '18:00' ); ?>>18:00</option>
			<option value="19:00" <?php selected( $jam, '19:00' ); ?>>19:00</option>
			<option value="20.00" <?php selected( $jam, '20:00' ); ?>>20:00</option>
	    	<option value="21:00" <?php selected( $jam, '21:00' ); ?>>21:00</option>
			<option value="22.00" <?php selected( $jam, '22:00' ); ?>>22:00</option>
			<option value="23.00" <?php selected( $jam, '23:00' ); ?>>23:00</option>
            </select>
	    	<?php     
							echo '</div>
							</div>
							<div class="half">
							    <div class="halfin">
								     <p>Tanggal Pulang</p>
			    	            	<input type="text" name="_back" value="' . $back  . '" class="back widefat" />
								</div>
							</div>
				    	</div>
						<p>Rute Perjalanan (misal : <em>Jakarta - Seoul - Nami Island - Petite France - Seoul - Jakarta</em>)</p>
	                    <input type="text" name="_rute" value="' . $rute  . '" class="widefat" />
						
						<div class="clear">
						    <div class="half">
							    <div class="halfin">
								    <p>Maskapai (opsional)</p>
									<input type="text" name="_maskapai" value="' . $maskapai  . '" class="widefat" />
								</div>
							</div>
							<div class="half">
							    <div class="halfin">
								    <p>Hotel (opsional)</p>
									<input type="text" name="_hotel" value="' . $hotel  . '" class="widefat" />
								</div>
							</div>
							<div class="half">
							    <div class="halfin">
								    <p>Rent Car (opsional)</p>
									<input type="text" name="_rentcar" value="' . $rentcar  . '" class="widefat" />
								</div>
							</div>
						</div>
						
						</td>
						<td width="150">
						
						    <div class="clear">
							    <p>Pendamping / Guide</p>
			    	            <input type="text" name="_guide" value="' . $guide  . '" class="guide widefat" />
						        <img class="photo" src="' . $photo  . '" />
						    	<input type="hidden" name="_photo" placeholder="tekan tombol upload dibawah..." id="_photo" value="' . $photo  . '" class="custom_media_url" />
						    	<input id ="upload_image_button" type="button" value="Photo 200x200 px" class="button button-primary button-large" />
							</div>
				    	</td>
			    	</tr>
		    	</table>';
	    ?>
	
	    <script type = "text/javascript">
            var file_frame;
			jQuery('#upload_image_button').live('click', function(podcast) {
		    	podcast.preventDefault();
				if (file_frame) {
			    	file_frame.open();
					return;
				}
				
				file_frame = wp.media.frames.file_frame = wp.media({
			    	title: jQuery(this).data('uploader_title'),
					button: {
				    	text: jQuery(this).data('uploader_button_text'),
					},
					multiple: false
				});
				
				file_frame.on('select', function(){
			    	attachment = file_frame.state().get('selection').first().toJSON();
					jQuery(".photo").attr('src', attachment.url);
					var all = JSON.stringify( attachment ); 
					var url = attachment.url;
					var namefile = document.getElementById("_photo");
					namefile.value = url; 
				});
				
				file_frame.open();
			});
		</script>
		
     	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.min.js"></script>
	
    	<script type="text/javascript">
    		jQuery(document).ready(function($) {
			    $('.going, .back').datepicker({
				dateFormat : 'dd-mm-yy' });
			});
    	</script>
	
    	<?php
	
        wp_enqueue_script('media-upload');
        wp_enqueue_script('thickbox');
		wp_enqueue_script('jquery-ui-datepicker');
        wp_enqueue_style('thickbox');
		wp_enqueue_style('wee-package', get_template_directory_uri() . '/css/package.css', true);
		wp_enqueue_style('jquery-ui-css', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');

	
	}

	function we_events_meta($post_id, $post) {
	    if ( !wp_verify_nonce( $_POST['eventmeta_noncename'], plugin_basename(__FILE__) )) {
	    return $post->ID;
	    }

	    if ( !current_user_can( 'edit_post', $post->ID ))
	        return $post->ID;
		
		if(isset($_POST["_jam"])) {
        $events_meta['_jam'] = $_POST['_jam'];
		}

	    $events_meta['_going'] = $_POST['_going'];
		$events_meta['_minus'] = strtotime($_POST['_going']);
		$events_meta['_jam'] = $_POST['_jam'];
		$events_meta['_back'] = $_POST['_back'];
		$events_meta['_guide'] = $_POST['_guide'];
		$events_meta['_photo'] = $_POST['_photo'];
		$events_meta['_rute'] = $_POST['_rute'];
		$events_meta['_maskapai'] = $_POST['_maskapai'];
		$events_meta['_hotel'] = $_POST['_hotel'];
		$events_meta['_rentcar'] = $_POST['_rentcar'];

	    foreach ($events_meta as $key => $value) {      
		    if( $post->post_type == 'revision' ) return;
	        $value = implode(',', (array)$value); 
	        if(get_post_meta($post->ID, $key, FALSE)) { 
	            update_post_meta($post->ID, $key, $value);
	        } else { 
	            add_post_meta($post->ID, $key, $value);
	        }
	        if(!$value) delete_post_meta($post->ID, $key); 
	    }

	}

	add_action('save_post', 'we_events_meta', 1, 2);
	
	
/*** POST TYPE PARTNER ***/		

	register_post_type( 'partner',		
	array(			
	    'menu_icon' => 'dashicons-editor-table',
	    'labels' => array(				
	    'name' => __( 'Partner' ),				
	    'singular_name' => __( 'Partner' ),        
	    'add_new' => __( 'Tambah Partner?' ),	
	    'add_new_item' => __( 'Tambah Partner' ),	
	    'edit' => __( 'Edit' ),	 
	    'edit_item' => __( 'Edit Partner' ),	
	    'new_item' => __( 'Partner Baru' ),	
	    'view' => __( 'Lihat Partner' ),	
	    'view_item' => __( 'Lihat Partner' ),	
	    'search_items' => __( 'Cari Partner' ),	
	    'not_found' => __( 'Tidak ada Partner ditemukan' ),	
	    'not_found_in_trash' => __( 'Tidak ada Partner di folder Trash' ),	
	    'parent' => __( 'Parent Super Duper' ),			
	    ),		                	
		'public' => true,           					            
		'has_archive' => true,        			            
		'supports' => array( 'title', 'thumbnail'),        			            
		'exclude_from_search' => false, 	 
		 )	
    );
	
/*** POST TYPE GALERI ***/
	
	register_post_type( 'galeri',		
	array(			
	    'menu_icon' => 'dashicons-format-gallery',
	    'labels' => array(				
	    'name' => __( 'Galeri' ),				
	    'singular_name' => __( 'Galeri' ),        
	    'add_new' => __( 'Tambah Galeri?' ),	
	    'add_new_item' => __( 'Tambah Galeri' ),	
	    'edit' => __( 'Edit' ),	 
	    'edit_item' => __( 'Edit Galeri' ),	
	    'new_item' => __( 'Item Baru' ),	
	    'view' => __( 'Lihat Galeri' ),	
	    'view_item' => __( 'Lihat Galeri' ),	
	    'search_items' => __( 'Cari Galeri' ),	
	    'not_found' => __( 'Tidak ada Galeri ditemukan' ),	
	    'not_found_in_trash' => __( 'Tidak ada Galeri di folder Trash' ),	
	    'parent' => __( 'Parent Super Duper' ),			
	    ),		                	
		'public' => true,           					            
		'has_archive' => true,        			            
		'supports' => array( 'title', 'editor', 'thumbnail','excerpt'),        			            
		'exclude_from_search' => false, 
		 )	
    );
	
/*** POST TYPE VIDEO ***/
	
	register_post_type( 'video',		
	array(			
	    'menu_icon' => 'dashicons-video-alt3',
	    'labels' => array(				
	    'name' => __( 'Video' ),				
	    'singular_name' => __( 'Video' ),        
	    'add_new' => __( 'Tambah Video?' ),	
	    'add_new_item' => __( 'Tambah Video' ),	
	    'edit' => __( 'Edit' ),	 
	    'edit_item' => __( 'Edit Video' ),	
	    'new_item' => __( 'Item Baru' ),	
	    'view' => __( 'Lihat Video' ),	
	    'view_item' => __( 'Lihat Video' ),	
	    'search_items' => __( 'Cari Video' ),	
	    'not_found' => __( 'Tidak ada Video ditemukan' ),	
	    'not_found_in_trash' => __( 'Tidak ada Video di folder Trash' ),	
	    'parent' => __( 'Parent Super Duper' ),			
	    ),		                	
		'public' => true,           					            
		'has_archive' => true,        			            
		'supports' => array( 'title', 'editor' ),        			            
		'exclude_from_search' => false, 	 
		'register_meta_box_cb' => 'vid',
		 )	
    );
	
		function vid() {
	    add_meta_box('sch_vid', 'Video Galeri', 'sch_vid', 'video', 'side', 'default');
	}

	function sch_vid() {
	    global $post;
	    echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' .
	    wp_create_nonce( plugin_basename(__FILE__) ) . '" />';

	    $embed = get_post_meta($post->ID, '_embed', true);
		
	        echo '<p>Untuk memudahkan penambahan video, gunakan embed video dari Youtube, cukup masukkan ID Video</p>';
			echo '<p>ID Video Youtube</p>';
	        echo '<input type="text" name="_embed" value="' . $embed  . '" class="widefat" />';
	}

	function vid_meta($post_id, $post) {
	    if ( !wp_verify_nonce( $_POST['eventmeta_noncename'], plugin_basename(__FILE__) )) {
	    return $post->ID;
	    }

	    if ( !current_user_can( 'edit_post', $post->ID ))
	        return $post->ID;
			
	    $events_meta['_embed'] = $_POST['_embed'];

	    foreach ($events_meta as $key => $value) {         
		    if( $post->post_type == 'revision' ) return;
	        $value = implode(',', (array)$value); 
	        if(get_post_meta($post->ID, $key, FALSE)) { 
	            update_post_meta($post->ID, $key, $value);
	        } else { 
	            add_post_meta($post->ID, $key, $value);
	        }
	        if(!$value) delete_post_meta($post->ID, $key);
	    }

	}

	add_action('save_post', 'vid_meta', 1, 2); 
	
	
/*** POST TYPE TE3STIMONI ***/
	
	register_post_type( 'testimoni',		
	array(			
	    'menu_icon' => 'dashicons-format-status',
	    'labels' => array(				
	    'name' => __( 'Testimoni' ),				
	    'singular_name' => __( 'Testimoni' ),        
	    'add_new' => __( 'Tambah Testimoni?' ),	
	    'add_new_item' => __( 'Tambah Testimoni' ),	
	    'edit' => __( 'Edit' ),	 
	    'edit_item' => __( 'Edit Testimoni' ),	
	    'new_item' => __( 'Testimoni Baru' ),	
	    'view' => __( 'Lihat Testimoni' ),	
	    'view_item' => __( 'Lihat Testimoni' ),	
	    'search_items' => __( 'Cari Testimoni' ),	
	    'not_found' => __( 'Tidak ada Testimoni ditemukan' ),	
	    'not_found_in_trash' => __( 'Tidak ada Testimoni di folder Trash' ),	
	    'parent' => __( 'Parent Super Duper' ),			
	    ),		                	
		'public' => true,           					            
		'has_archive' => true,        			            
		'supports' => array( 'title', 'editor', 'thumbnail' ),        			            
		'exclude_from_search' => false, 
		'register_meta_box_cb' => 'tes',
		 )	
    );
	
		function tes() {
	    add_meta_box('tess', 'Data Testimoni', 'tess', 'testimoni', 'normal', 'default');
	}

	function tess() {
	    global $post;
	    echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' .
	    wp_create_nonce( plugin_basename(__FILE__) ) . '" />';

	    $nama = get_post_meta($post->ID, '_nama', true);
		$profesi = get_post_meta($post->ID, '_profesi', true);
		$kota = get_post_meta($post->ID, '_kota', true);
		$rating = get_post_meta($post->ID, '_rating', true);
		
	        echo '<p>Nama Lengkap</p>';
	        echo '<input type="text" name="_nama" value="' . $nama  . '" class="widefat" />';
			echo '<p>Profesi</p>';
	        echo '<input type="text" name="_profesi" value="' . $profesi  . '" class="widefat" />';
			echo '<p>Kota Asal</p>';
	        echo '<input type="text" name="_kota" value="' . $kota  . '" class="widefat" />';
			echo '<p>Berikan Rating</p>';?>
			<select name="_rating" id="rating" class="widefat">
		    <option value="super" <?php selected( $rating, 'super' ); ?>>Super</option>
	    	<option value="bagus" <?php selected( $rating, 'bagus' ); ?>>Bagus</option>
			<option value="cukup" <?php selected( $rating, 'cukup' ); ?>>Cukup</option>
			<option value="kurang" <?php selected( $rating, 'kurang' ); ?>>Kurang</option>
			<option value="buruk" <?php selected( $rating, 'buruk' ); ?>>Buruk</option>	 
            </select>			
        <?php 
	}

	function tes_meta($post_id, $post) {
	    if ( !wp_verify_nonce( $_POST['eventmeta_noncename'], plugin_basename(__FILE__) )) {
	    return $post->ID;
	    }

	    if ( !current_user_can( 'edit_post', $post->ID ))
	        return $post->ID;
			
	    $events_meta['_nama'] = $_POST['_nama'];
		$events_meta['_profesi'] = $_POST['_profesi'];
		$events_meta['_kota'] = $_POST['_kota'];
		$events_meta['_rating'] = $_POST['_rating'];

	    foreach ($events_meta as $key => $value) {         
		    if( $post->post_type == 'revision' ) return;
	        $value = implode(',', (array)$value); 
	        if(get_post_meta($post->ID, $key, FALSE)) { 
	            update_post_meta($post->ID, $key, $value);
	        } else { 
	            add_post_meta($post->ID, $key, $value);
	        }
	        if(!$value) delete_post_meta($post->ID, $key);
	    }

	}

	add_action('save_post', 'tes_meta', 1, 2); 
	
	
	
}

	add_action( 'init', 'long_taxonomy', 0 );
	function long_taxonomy() {
	  $labels = array(
	    'name' => _x( 'Pilihan Jumlah Hari', 'taxonomy general name' ),
	    'singular_name' => _x( 'Pilihan Tour', 'taxonomy singular name' ),
	    'search_items' =>  __( 'Cari Pilihan' ),
	    'all_items' => __( 'Semua Pilihan' ),
	    'parent_item' => null,
	    'parent_item_colon' => null,
	    'edit_item' => __( 'Edit Pilihan' ),
	    'update_item' => __( 'Update Pilihan' ),
	    'add_new_item' => __( 'Tambah Baru' ),
	    'new_item_name' => __( 'Tambah Pilihan Baru' ),
	    'menu_name' => __( 'Pilihan Tour' ),
	  );   
	  
	  register_taxonomy('long',array('paket'), array(
	    'hierarchical' => true,
	    'labels' => $labels,
	    'show_ui' => true,
	    'show_admin_column' => true,
	    'query_var' => true,
	  ));
	}
	
	add_action( 'init', 'tujuan_taxonomy', 0 );
	function tujuan_taxonomy() {
	  $labels = array(
	    'name' => _x( 'Negara / Kota Tujuan', 'taxonomy general name' ),
	    'singular_name' => _x( 'Negara / Kota Tujuan', 'taxonomy singular name' ),
	    'search_items' =>  __( 'Cari Tujuan' ),
	    'all_items' => __( 'Semua Tujuan' ),
	    'parent_item' => null,
	    'parent_item_colon' => null,
	    'edit_item' => __( 'Edit Tujuan' ),
	    'update_item' => __( 'Update Tujuan' ),
	    'add_new_item' => __( 'Tambah Baru' ),
	    'new_item_name' => __( 'Tambah Tujuan Baru' ),
	    'menu_name' => __( 'Tujuan Tujuan' ),
	  );   
	  
	  register_taxonomy('tujuan',array('paket'), array(
	    'hierarchical' => true,
	    'labels' => $labels,
	    'show_ui' => true,
	    'show_admin_column' => true,
	    'query_var' => true,
	  ));
	}

?>