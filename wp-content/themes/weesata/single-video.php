<?php
/**
 * Video tema weesata
 * Author : yayun
 * Facebook : http://facebook.com/ciussgw
 * Whatsapp : 0838-1525-1385
 */
?>

<?php get_header(); ?>

<section class="wecontent clear">
    <div class="pactour container clear">
	
    	<div class="weleft">
        	<?php if (have_posts()): ?>
	        	<?php while (have_posts()): the_post(); ?>
				
				<!-- left side -->
			    <div class="sttl clear">
			    	<div class="blog-content gall">
			    		<iframe class="akframe" src="https://www.youtube.com/embed/<?php echo get_post_meta($post->ID, '_embed', true); ?>" frameborder="0" allowfullscreen></iframe>
					
					<div class="blog-content">
					    <div class="clear">
				        	<?php printf(__('<div class="wvid"><div class="post-datez">%s</div><div class="post-mon">%s</div></div>', 'weesata'),
					        	get_the_time('j'), get_the_time('M Y')
				        	); ?>
					    	<h1 class="vvid"><?php the_title(); ?></h1>
						</div>
			        	<?php the_content(); ?>
					</div>
					</div>
				</div>
			
		
	        	<?php endwhile; ?>
        	<?php endif; ?>	
    	</div>

    	<div class="weright">
    		<div class="rpac">
		 		<div class="ttr">PAKET TOUR</div>
	        		<?php query_posts('post_type=paket&showposts=3&orderby=rand'); ?>
				        <?php if (have_posts()) { ?>
				    	    <?php while (have_posts()): the_post(); ?>
							
							<?php global $post;
							    $expired = get_post_meta($post->ID, '_expidi', true);
								$current_timestamp = date_i18n("j F Y");
								$db_timestamp = date_i18n("j F Y", strtotime($expired));
								?>
					     	    <div class="lst clear">
						     	    <div class="relpac">
								    <?php if (has_post_thumbnail()) { ?>
						        		<?php the_post_thumbnail('little', array(
							        		'alt' => trim(strip_tags($post->post_title)),
									    	'title' => trim(strip_tags($post->post_title)),
									    	)); ?>
						     		<?php } ?>
							    	</div>
									<a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
									<div class="relpri">
								    	<?php if(($db_timestamp-$current_timestamp) > 0) { ?>
							        		<strong>IDR <?php echo get_post_meta($post->ID, '_dsingle', true); ?></strong> <em><del><?php echo get_post_meta($post->ID, '_single', true); ?></del></em>
							      		<?php } else { ?>
								         	<strong>IDR <?php echo get_post_meta($post->ID, '_single', true); ?></strong>
								    	<?php } ?>
									</div>
						    	</div>
					    	<?php endwhile; ?>
				    	<?php } ?>
		        	<?php wp_reset_query(); ?>
			</div>
		
	    	<?php get_sidebar(); ?>
		
		</div>
	</div>
</section>
	
<?php get_footer(); ?>
