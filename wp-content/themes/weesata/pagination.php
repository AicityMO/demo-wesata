<?php
/**
 * Pagination tema weesata
 * Author : yayun
 * Facebook : http://facebook.com/ciussgw
 * Whatsapp : 0838-1525-1385
 */
?>

<div class="dpag clear">
	<div class="pagination">
		<?php previous_posts_link(__('<i class="fa fa-angle-left"></i><span class="text"> Lebih Lama</span>', 'weesata')); ?>
		<?php wpbeginner_numeric_posts_nav(); ?>
		<?php next_posts_link(__('<span class="text">Lebih Baru </span><i class="fa fa-angle-right"></i>', 'weesata')); ?>
	</div>
</div>