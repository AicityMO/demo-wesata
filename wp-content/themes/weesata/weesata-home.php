<?php

/**
 * Template Name: Tema Homepage
 */

/**
 * Index tema Akademi
 * Author : yayun
 * Facebook : http://facebook.com/ciussgw
 * Whatsapp : 0838-1525-1385
 */
?>

<?php get_header(); ?>

<section class="weefirst clear">
    <div class="container">
    	<div class="pack-one">
			<?php query_posts('post_type=paket&showposts=6'); ?>
				    <?php if (have_posts()) { ?>
					    <?php while (have_posts()): the_post(); ?>
						
						    <?php global $post;
							$packexpired = get_post_meta($post->ID, '_expidi', true);
							$ttoday = date_i18n("j F Y");
							$texpired = date_i18n("j F Y", strtotime($packexpired)); ?>
						
						    <!-- 3 kolom landscape -->
						    <div class="we-3">
							    <div class="pack-img">
								    <?php if (has_post_thumbnail()) { ?>
									<a href="<?php the_permalink() ?>">
						        		<?php the_post_thumbnail('landscape', array(
							        		'alt' => trim(strip_tags($post->post_title)),
									    	'title' => trim(strip_tags($post->post_title)),
									    	)); ?>
								    </a>
						    	    <?php } else { ?>
									<a href="<?php the_permalink() ?>">
			    	    		        <img src="<?php echo get_template_directory_uri(); ?>/images/default.jpg"/>
									</a>
						     		<?php } ?>
									<div class="dets">
								    	<h4><?php the_title(); ?></h4>
										<?php if(($texpired-$ttoday) > 0) { ?>
									        Mulai : <strong class="ors">IDR <?php echo get_post_meta($post->ID, '_dsingle', true); ?></strong> <del><em>IDR <?php echo get_post_meta($post->ID, '_single', true); ?></em></del><br/>
									    	Quota : <?php echo get_post_meta($post->ID, '_quota', true); ?> Orang
									    <?php } else { ?>
									    	Mulai : <strong class="ors">IDR <?php echo get_post_meta($post->ID, '_single', true); ?></strong><br/>
									    	Quota : <?php echo get_post_meta($post->ID, '_quota', true); ?> Orang
										<?php } ?>
									    <div class="order">
										    <a href="<?php the_permalink() ?>#order">ORDER</a>
										</div>
									</div>
									
									<?php $terms = get_the_terms( $post->ID , 'long' ); 
										if ( $terms != null ){
											echo '<div class="p-day">';
										    	foreach( $terms as $term ) {
												$term_link = get_term_link( $term, 'long' );
											    	echo '<div class="p-span">' . $term->name . '</div>';
												unset($term); 
										    	} 
											echo '</div>';
										} 
									?>
									
								</div>
							</div>
						    <!-- 3 kolom landscape -->
						
						<?php endwhile; ?>
					<?php } ?>
			<?php wp_reset_query(); ?>
        </div>
	</div>
</section>

<section class="weesecond">
    <div class="container clear">
    	<div class="pack-one">
		    <h3 class="toptitle"><?php echo (get_option('paket')) ? get_option('paket').'' : 'PALING POPULER' ?></h3>
			<?php query_posts('post_type=paket&showposts=3&meta_key=popular_posts&orderby=meta_value_num'); ?>
				    <?php if (have_posts()) { ?>
					    <?php while (have_posts()): the_post(); ?>
						
						    <?php global $post;
							$popexpired = get_post_meta($post->ID, '_expidi', true);
							$poptoday = date_i18n("j F Y");
							$popexpir = date_i18n("j F Y", strtotime($popexpired)); ?>
						
						    <!-- 3 kolom landscape -->
						    <div class="we-3">
							    <div class="pack-img">
								    <?php if (has_post_thumbnail()) { ?>
									<a href="<?php the_permalink() ?>">
						        		<?php the_post_thumbnail('landscape', array(
							        		'alt' => trim(strip_tags($post->post_title)),
									    	'title' => trim(strip_tags($post->post_title)),
									    	)); ?>
								    </a>
						    	    <?php } else { ?>
									<a href="<?php the_permalink() ?>">
			    	    		        <img src="<?php echo get_template_directory_uri(); ?>/images/default.jpg"/>
									</a>
						     		<?php } ?>
									<div class="dets">
								    	<h4><?php the_title(); ?></h4>
										<?php if(($popexpir-$poptoday) > 0) { ?>
									        Mulai : <strong class="ors">IDR <?php echo get_post_meta($post->ID, '_dsingle', true); ?></strong> <del><em>IDR <?php echo get_post_meta($post->ID, '_single', true); ?></em></del><br/>
									    	Quota : <?php echo get_post_meta($post->ID, '_quota', true); ?> Orang
									    <?php } else { ?>
									    	Mulai : <strong class="ors">IDR <?php echo get_post_meta($post->ID, '_single', true); ?></strong><br/>
									    	Quota : <?php echo get_post_meta($post->ID, '_quota', true); ?> Orang
										<?php } ?>
									    <div class="order">
										    <a href="<?php the_permalink() ?>#order">ORDER</a>
										</div>
									</div>
									
									<?php $terms = get_the_terms( $post->ID , 'long' ); 
										if ( $terms != null ){
											echo '<div class="p-day">';
										    	foreach( $terms as $term ) {
												$term_link = get_term_link( $term, 'long' );
											    	echo '<div class="p-span">' . $term->name . '</div>';
												unset($term); 
										    	} 
											echo '</div>';
										} 
									?>
									
								</div>
							</div>
						    <!-- 3 kolom landscape -->
							
						<?php endwhile; ?>
					<?php } ?>
			<?php wp_reset_query(); ?>
		</div>
	</div>
</section>

<section class="weethird" <?php if (get_option('background')) { ?> style="background: url(<?php echo get_option('background'); ?>) center; background-size: cover;" <?php } ?>>
    <div class="weelay">
    </div>
    <div class="container clear">
    	<div class="pack-one relat">
		    
			<!-- 2 kolom -->
		    <div class="we-23">
			    <div class="third-co">
		    	    <h1 class="prom">
					    <?php echo (get_option('pesan')) ? get_option(stripslashes('pesan')).'' : 'Pesan Sekarang, <span>jangan sampe kehabisan</span> Kuota !!!' ?>
					</h1>
		    		<h5 class="des">
					    <?php echo (get_option(stripslashes('deskripsi'))) ? get_option('deskripsi').'' : 'Kami selalu berikan promo-promo menarik, mengunjungi tempat-tempat indah yang akan memanjakan liburan Anda' ?>
					</h5>
		    		<div class="butn">
				        <a href="/paket/"><?php echo (get_option('lihat')) ? get_option('lihat').'' : 'LIHAT PAKET' ?></a>
			    	</div>
				</div>
			</div>
			
		    <div class="we-3">
				<div class="co-onhour">
				    <div class="co-bot">
			    		<i class="fa fa-phone"></i> <?php echo (get_option('telepon')) ? get_option('telepon').'' : '0838-1525-1385' ?> <div class="onh"><?php echo (get_option('online')) ? get_option('online').'' : 'ONLINE HOURS' ?></div>
					</div>
					<table class="jad">
		    			<tr>
					    	<td class="jlef">
							    <div class="clock">
								    <i class="fa fa-clock-o"></i>	
								</div>
							</td>
							<td><div class="jday">SENIN</div></td>
							<td class="jright"><div class="jday"><?php echo (get_option('senin')) ? get_option('senin').'' : '08.00 - 21.00' ?></div></td>
		    			</tr>
						<tr>
					    	<td class="jlef">
							    <div class="clock">
								    <i class="fa fa-clock-o"></i>	
								</div>
							</td>
							<td><div class="jday">SELASA</div></td>
							<td class="jright"><div class="jday"><?php echo (get_option('selasa')) ? get_option('selasa').'' : '08.00 - 21.00' ?></div></td>
		    			</tr>
						<tr>
					    	<td class="jlef">
							    <div class="clock">
								    <i class="fa fa-clock-o"></i>	
								</div>
							</td>
							<td><div class="jday">RABU</div></td>
							<td class="jright"><div class="jday"><?php echo (get_option('rabu')) ? get_option('rabu').'' : '08.00 - 21.00' ?></div></td>
		    			</tr>
						<tr>
					    	<td class="jlef">
							    <div class="clock">
								    <i class="fa fa-clock-o"></i>	
								</div>
							</td>
							<td><div class="jday">KAMIS</div></td>
							<td class="jright"><div class="jday"><?php echo (get_option('kamis')) ? get_option('kamis').'' : '08.00 - 21.00' ?></div></td>
		    			</tr>
						<tr>
					    	<td class="jlef jlime">
							    <div class="clock">
								    <i class="fa fa-clock-o"></i>	
								</div>
							</td>
							<td class="jlime"><div class="jday">JUMAT</div></td>
							<td class="jright"><div class="jday jlime"><?php echo (get_option('jumat')) ? get_option('jumat').'' : '08.00 - 19.00' ?></div></td>
		    			</tr>
						<tr>
					    	<td class="jlef">
							    <div class="clock">
								    <i class="fa fa-clock-o"></i>	
								</div>
							</td>
							<td><div class="jday">SABTU</div></td>
							<td class="jright"><div class="jday"><?php echo (get_option('sabtu')) ? get_option('sabtu').'' : '08.00 - 21.00' ?></div></td>
		    			</tr>
						<tr>
					    	<td class="jlef jorange">
							    <div class="clock">
								    <i class="fa fa-clock-o"></i>	
								</div>
							</td>
							<td class="jorange"><div class="jday">MINGGU</div></td>
							<td class="jbott"><div class="jday jorange"><?php echo (get_option('minggu')) ? get_option('minggu').'' : '08.00 - 23.00' ?></div></td>
		    			</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="weefourth">
 
    <div class="container clear">
    	<div class="pack-one relat">
		    <div class="partt">
	            <div class="ttt"><?php echo (get_option('partner')) ? get_option('partner').'' : 'PARTNER KAMI' ?></div>
        	</div>
			<?php query_posts('post_type=partner&showposts=24&orderby=rand'); ?>
				    <?php if (have_posts()) { ?>
					    <?php while (have_posts()): the_post(); ?>
						    <div class="we-8">
							    <div class="pack-img gre">
								    <?php if (has_post_thumbnail()) { ?>
						        		<?php the_post_thumbnail('little', array(
							        		'alt' => trim(strip_tags($post->post_title)),
									    	'title' => trim(strip_tags($post->post_title)),
									    	)); ?>
						     		<?php } ?>
								</div>
							</div>
						<?php endwhile; ?>
					<?php } ?>
			<?php wp_reset_query(); ?>
		
		</div>
	</div>
</section>

<section class="weesche">
    <div class="container clear">
    	<div class="pack-one">
		
			<div class="we-2">
				<div class="pack-img">
		         	<?php query_posts('post_type=schedule&showposts=1&order=ASC&meta_key=_minus&orderby=meta_value&post_status=publish'); ?>
			    	    <?php if (have_posts()) { ?>
				    	    <?php while (have_posts()): the_post(); ?>
							
							
							    <?php global $post;
								$latest = get_post_meta($post->ID, '_going', true); 
								$hapus=get_post_meta($id, '_going', true );
	                        	$minus = strtotime(get_post_meta($post->ID, '_going', true));
		
								?>
								<div class="schelay">
									<?php the_post_thumbnail('landscape', array(
							        	'alt' => trim(strip_tags($post->post_title)),
									    'title' => trim(strip_tags($post->post_title)),
									)); ?>
									<div class="td">
										<h4><?php echo date_i18n("j", strtotime($latest)); ?></h4>
										<?php echo date_i18n("F", strtotime($latest)); ?>
									</div>
									<div class="guide">
									    <img src="<?php echo get_post_meta($post->ID, '_photo', true); ?>"/>
										<small>GUIDE</small>
									</div>
									<div id="clockz">
										<h4><?php the_title(); ?></h4>
										Segera berangkat dalam <strong><span class="dayz"></span> Hari</strong> + <span class="hourz"></span>:<span class="minutez"></span> <span class="secondz"></span>" lagi
										<br/><br/>
										Detail : <em><?php echo get_post_meta($post->ID, '_rute', true); ?></em>
									</div>
									<div class="cek">
										<a href="./schedule/"><?php echo (get_option('cek')) ? get_option('cek').'' : 'CEK AGENDA' ?></a>
									</div>
									<div class="aglay">
									</div>
								</div>
								
<script>

function getTimeRemaining(endtime){
  var t = Date.parse(endtime) - Date.parse(new Date());
  var seconds = Math.floor( (t/1000) % 60 );
  var minutes = Math.floor( (t/1000/60) % 60 );
  var hours = Math.floor( (t/(1000*60*60)) % 24 );
  var days = Math.floor( t/(1000*60*60*24) );
  return {
    'total': t,
    'days': days,
    'hours': hours,
    'minutes': minutes,
    'seconds': seconds
  };
}

function initializeClock(id, endtime){
  var clock = document.getElementById(id);
  var daysSpan = clock.querySelector('.dayz');
  var hoursSpan = clock.querySelector('.hourz');
  var minutesSpan = clock.querySelector('.minutez');
  var secondsSpan = clock.querySelector('.secondz');

  function updateClock(){
    var t = getTimeRemaining(endtime);

    daysSpan.innerHTML = t.days;
    hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
    minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
    secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

    if(t.total<=0){
      clearInterval(timeinterval);
    }
  }

  updateClock();
  var timeinterval = setInterval(updateClock,1000);
}

var deadline = '<?php echo date("F j Y", strtotime($latest)); ?> <?php echo get_post_meta($post->ID, '_jam', true); ?> UTC+0700';
initializeClock('clockz', deadline);

</script>
									
				    		<?php endwhile; ?>
				    	<?php } ?>
		        	<?php wp_reset_query(); ?>
				</div>
			</div>

			<div class="we-2">
				<div class="pack-img">
				    <div class="new-news">
					    <?php echo (get_option('berita')) ? get_option('berita').'' : 'BERITA TERBARU' ?>
					</div>
		         	<?php query_posts('post_type=post&showposts=3'); ?>
			    	    <?php if (have_posts()) { ?>
				    	    <?php while (have_posts()): the_post(); ?>
									
								<div class="news">
									<div class="listz-img">
										<?php the_post_thumbnail('little', array(
							        		'alt' => trim(strip_tags($post->post_title)),
									    	'title' => trim(strip_tags($post->post_title)),
									    )); ?>
									</div>
									<div class="listz">
										<h4><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4>
										<?php printf(__('Terbit : <span>%s</span>', 'weesata'), get_the_time('l, j M Y')); ?>
									</div>
									<div class="newslay">
									</div>
								</div>
									
				    		<?php endwhile; ?>
				    	<?php } ?>
		        	<?php wp_reset_query(); ?>
				</div>
			</div>


		</div>
	</div>
</section>

<section class="weetestimo" <?php if (get_option('background')) { ?> style="background: url(<?php echo get_option('background'); ?>) center; background-size: cover;" <?php } ?>>
    <div class="weelay">
    </div>
    <div class="container clear">
    	<div class="pack-one relat clear">
				
            <div class="testi">
		    	<?php query_posts('post_type=testimoni&showposts=3'); ?>
				    <?php if (have_posts()) { ?>
					    <?php $count = 0; ?>
					    <?php while (have_posts()): the_post(); 
						    
							global $post;
							$rating = get_post_meta($post->ID, '_rating', true);
							$count_posts = wp_count_posts('testimoni')->publish;
						    $count++; ?>
							
						    <!-- 3 kolom landscape -->
						    <div class="<?php if ( $count_posts == 1 ) {
							    echo 'singletestimoni-'.$count; 
								} else if ( $count_posts == 2 ) { 
								echo 'duotestimoni-'.$count; 
								} else { 
								echo 'testimoni-'.$count;
								} ?>
								">
							    <div class="psay">
							    	<div class="spans">"..<?php if (function_exists('smart_excerpt')) smart_excerpt(get_the_excerpt(), 50); ?>.."</div>
									<div class="star">
									    <?php if ( $rating == 'super' ) {
									    	echo '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>'; 
										} else if ( $rating == 'bagus' ) { 
									    	echo '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>';
										} else if ( $rating == 'cukup' ) { 
									    	echo '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>';
										} else if ( $rating == 'kurang' ) { 
									    	echo '<i class="fa fa-star"></i><i class="fa fa-star"></i>';
									    } else if ( $rating == 'buruk' ) { 
									    	echo '<i class="fa fa-star"></i>';
										} else {
									    	echo '<i class="fa fa-star"></i>'; 
										} ?>
								    	 - <em><?php the_title(); ?> <span>( <?php echo get_post_meta($post->ID, '_profesi', true); ?> / <?php echo get_post_meta($post->ID, '_kota', true); ?> )</span></em>
									</div>
								
			    	    		        <?php the_post_thumbnail('medium', array(
							        		'alt' => trim(strip_tags($post->post_title)),
									    	'title' => trim(strip_tags($post->post_title)),
									    )); ?>
									
								</div>
							</div>
						    <!-- 3 kolom landscape -->
							
				    	<?php endwhile; ?>
				    <?php } ?>
		        <?php wp_reset_query(); ?>
				
            </div>
	    </div>
    </div>

</section>

	
<?php get_footer(); ?>
