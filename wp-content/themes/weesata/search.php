<?php
/**
 * Search tema mading
 * Author : yayun
 * Facebook : http://facebook.com/ciussgw
 * Whatsapp : 0838-1525-1385
 */
?>

<?php get_header(); ?>

<section class="wecontent clear">
    <div class="container">
		
     	<?php if (have_posts()): ?>
			    <?php if (get_post_type(get_the_ID()) == 'post') { ?>
                	<div class="weleft">
		            	<?php get_template_part('inc/loop/loop'); ?>
		             	<?php get_template_part('pagination'); ?>
	            	</div>
				<?php } else if (get_post_type(get_the_ID()) == 'paket') { ?>
                	<div class="pack-one clear">
		            	<?php get_template_part('inc/loop/paket'); ?>
					</div>
					<div class="pack-one clear">
		             	<?php get_template_part('pagination'); ?>
	            	</div>
				<?php } else { ?>
	            	<div class="nf404">
				    	<div class="head404">
			            	<h1><?php _e("<span>Hasil..?!</span>", 'weesata'); ?></h1>
	                 	</div>
	                	<div class="content404">
		                	Hasil pencarian untuk "<?php the_search_query(); ?>" tidak menemukan hasil.. silahkan coba pencarian lainnya
	                	</div>
	            	</div>
				<?php } ?>
				
		<?php else: ?>
				
	            	<div class="nf404">	
				    	<div class="head404">
			            	<h1><?php _e("<span>Hasil..?!</span>", 'weesata'); ?></h1>
	                 	</div>
	                	<div class="content404">
		                	Hasil pencarian untuk "<?php the_search_query(); ?>" tidak menemukan hasil.. silahkan coba pencarian lainnya
	                	</div>
	            	</div>
					
       	<?php endif; ?>
		
		<?php if (have_posts()) { ?>
		
            	<?php if (get_post_type(get_the_ID()) == 'post') { ?>
				
				<div class="weright">
    	        	<div class="rpac">
		 	        	<div class="ttr">PAKET TOUR</div>
						<?php query_posts('post_type=paket&showposts=3&orderby=rand'); ?>
				        <?php if (have_posts()) { ?>
				    	    <?php while (have_posts()): the_post(); ?>
							
							<?php global $post;
							    $expired = get_post_meta($post->ID, '_expidi', true);
								$current_timestamp = date_i18n("j F Y");
								$db_timestamp = date_i18n("j F Y", strtotime($expired));
								?>
					     	    <div class="lst clear">
						     	    <div class="relpac">
								    <?php if (has_post_thumbnail()) { ?>
						        		<?php the_post_thumbnail('little', array(
							        		'alt' => trim(strip_tags($post->post_title)),
									    	'title' => trim(strip_tags($post->post_title)),
									    	)); ?>
						     		<?php } ?>
							    	</div>
									<a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
									<div class="relpri">
								    	<?php if(($db_timestamp-$current_timestamp) > 0) { ?>
							        		<strong>IDR <?php echo get_post_meta($post->ID, '_dsingle', true); ?></strong> <em><del><?php echo get_post_meta($post->ID, '_single', true); ?></del></em>
							      		<?php } else { ?>
								         	<strong>IDR <?php echo get_post_meta($post->ID, '_single', true); ?></strong>
								    	<?php } ?>
									</div>
						    	</div>
					    	<?php endwhile; ?>
				    	<?php } ?>
					    <?php wp_reset_query(); ?>
					</div>
						
					<?php get_sidebar(); ?>
				</div>
                
				<?php } ?>
				
		<?php } ?>	
				
	</div>
</section>
	
<?php get_footer(); ?>
