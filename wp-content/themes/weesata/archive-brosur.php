<?php
/**
 * Arsip brosur tema weesata
 * Author : yayun
 * Facebook : http://facebook.com/ciussgw
 * Whatsapp : 0838-1525-1385
 */
?>

<?php get_header(); ?>

<section class="wecontent clear">
    <div class="container">
	
    	<div class="weleft">
			<?php query_posts('post_type=brosur&showposts=100'); ?>
		    	<?php if (have_posts()) { ?>
		    		<div id="loop" class="list archives clear">
				    	<table class="down">
				    	    <tr>
				    		    <td class="ctr"><strong>IKON</strong></td>
				    			<td class="ctr"><strong>KETERANGAN</strong></td>
				    			<td class="ctr"><strong>DOWNLOAD</strong></td>
				    		</tr>
							
   			    	     	<?php while (have_posts()): the_post(); ?>
        
				    		<tr>
				    		    <td>
				    			     <img src="<?php echo get_post_meta($post->ID, 'icon', true); ?>"/>
				    			</td>
				    	    	<td>
					    		     <strong><?php the_title(); ?></strong><br/>
					    			 <em>Type : <?php echo get_post_meta($post->ID, 'type', true); ?></em><br/>
					    			 <?php the_content(); ?>
					    		</td>
					    		<td> <a href="<?php echo get_post_meta($post->ID, 'brosur', true); ?>">Download</a><br/>
					    		<em>(<?php echo get_post_meta($post->ID, 'size', true); ?>)</em></td>
				    		</tr>
		
				    		<?php endwhile; ?>
			    		</table>
					</div>
			    <?php } ?>
			<?php wp_reset_query(); ?>
		</div>
	
	    <div class="weright">
    		<div class="rpac">
		 		<div class="ttr">PAKET TOUR</div>
	        		<?php query_posts('post_type=paket&showposts=3&orderby=rand'); ?>
				        <?php if (have_posts()) { ?>
				    	    <?php while (have_posts()): the_post(); ?>
							
							<?php global $post;
							    $expired = get_post_meta($post->ID, '_expidi', true);
								$current_timestamp = date_i18n("j F Y");
								$db_timestamp = date_i18n("j F Y", strtotime($expired));
								?>
					     	    <div class="lst clear">
						     	    <div class="relpac">
								    <?php if (has_post_thumbnail()) { ?>
						        		<?php the_post_thumbnail('little', array(
							        		'alt' => trim(strip_tags($post->post_title)),
									    	'title' => trim(strip_tags($post->post_title)),
									    	)); ?>
						     		<?php } ?>
							    	</div>
									<a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
									<div class="relpri">
								    	<?php if(($db_timestamp-$current_timestamp) > 0) { ?>
							        		<strong>IDR <?php echo get_post_meta($post->ID, '_dsingle', true); ?></strong> <em><del><?php echo get_post_meta($post->ID, '_single', true); ?></del></em>
							      		<?php } else { ?>
								         	<strong>IDR <?php echo get_post_meta($post->ID, '_single', true); ?></strong>
								    	<?php } ?>
									</div>
						    	</div>
					    	<?php endwhile; ?>
				    	<?php } ?>
		        	<?php wp_reset_query(); ?>
			</div>
		
	    	<?php get_sidebar(); ?>
		
		</div>
	
	</div>
</section>
	
<?php get_footer(); ?>