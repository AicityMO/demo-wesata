<?php
/**
 * Tema weesata, tema CMS Wordpress untuk website Tour & Travel
 * Rilis 21 Agustus 2017
 * Author : yayun
 * Facebook : http://facebook.com/ciussgw
 * Whatsapp : 0838-1525-1385
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
		<title><?php is_front_page() ? bloginfo('name') : wp_title(''); ?></title>
		<meta http-equiv="Content-language" content="<?php bloginfo('language'); ?>" />
		<meta name="viewport" content="width=device-width" />
		<meta name="revisit-after" content="7 days" />
		<meta http-equiv="Expires" content="0" />
		<meta http-equiv="Pragma" content="no-cache" />
		<meta http-equiv="Cache-Control" content="no-cache" />
		<meta http-equiv="Copyright" content="<?php bloginfo('name'); ?>" />
		<link rel="profile" href="http://gmpg.org/xfn/11" />
		<?php if (get_option('favicon')) { ?>
	    	<link rel="shortcut icon" href="<?php echo get_option('favicon'); ?>" type="image/x-icon" />
		<?php } else { ?>
		    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/icon.png" type="image/x-icon" />
		<?php } ?>
		<link id="weestyle" href="<?php echo get_template_directory_uri(); ?>/css/<?php echo (get_option('weestyle')) ? get_option('weestyle') : 'default' ?>.css" type="text/css" rel="stylesheet" />

		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.min.js"></script>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/newstickers.js"></script>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/accordion.js"></script>
		<?php wp_head(); ?>
		
		<script>
           $(function(){
               $("ul#wees-ticker").liScroll();
           });
        </script>
		
		<script type="text/javascript">
		(function($) {
			$(function() {
				$('#slideshow').cycle({
					fx: 'fade',
					timeout: <?php echo (get_option('ss_timeout')) ? get_option('ss_timeout') : '7000' ?>,
					next: '#rarr',
					prev: '#larr'
				});
			})
		})(jQuery)
		</script>
		
		<script type="text/javascript">
		(function($) {
			$(function() {
				$('#slideim').cycle({
					fx: 'fade',
					timeout: <?php echo (get_option('ss_timeout')) ? get_option('ss_timeout') : '7000' ?>,
					next: '#rarr',
					prev: '#larr'
				});
			})
		})(jQuery)
		</script>
		
		<script type="text/javascript">
	    	$(document).ready(function () {
	    		$('ul').accordion();
	     	});
		</script>
		
		<script type="text/javascript">
    		$("document").ready(function($){
	    		$(".mob").slideUp();
				$(".full").hide();
				$(".fmenu").click(function(){
		    		$(".mob").slideToggle();
				});
				$("#search").click(function(){
		    		$(".ca").slideToggle();
					$(".onhour, .sosmed, .kon, .mob").slideUp();
				});
				
				$("#jam").click(function(){
		    		$(".onhour").slideToggle();
					$(".ca, .sosmed, .kon, .mob").slideUp();
				});
				
				$("#sss").click(function(){
		    		$(".sosmed").slideToggle();
					$(".onhour, .ca, .kon, .mob").slideUp();
				});
				
				$("#xclose").click(function(){
					$(".onhour").slideUp();
				});
				
				$("#conte").click(function(){
		    		$(".kon").slideToggle();
					$(".onhour, .sosmed, .ca, .mob").slideUp();
				});
				
				$("#navi").click(function(){
		    		$(".mob").slideToggle();
					$(".onhour, .sosmed, .ca, .kon").slideUp();
				});
				
				$(".opens").mouseenter(function(){
		    		$(".layout, .styling, .mmenu").css("left","0");
					$(".opens").hide();
					$(".closes").show();
				});
				
				$(".fa-cog").click(function(){
		    		$(".layout, .styling, .mmenu").css("left","0");
					$(".styling").css("left","0");
					$(".opens").hide();
					$(".closes").show();
				});
				
				$(".closes").click(function(){
		    		$(".layout, .styling, .mmenu").css("left","-75px");
					$(".styling").css("left","-75px");
					$(".opens").show();
					$(".closes").hide();
				});
				
				$(".box").click(function(){
		    		$(".wees").removeClass("noboxed").addClass("boxed");
					$(".full").show();
					$(".box").hide();
				});
				$(".full").click(function(){
		    		$(".wees").removeClass("boxed").addClass("noboxed");
					$(".box").show();
					$(".full").hide();
				});
				$(".mfix").click(function(){
		    		$(".wees").removeClass("nofixed").addClass("fixed");
					$(".scroll").show();
					$(".mfix").hide();
				});
				$(".scroll").click(function(){
		    		$(".wees").removeClass("fixed").addClass("nofixed");
					$(".mfix").show();
					$(".scroll").hide();
				});
				$(".donker").click(function(){
		    		$("#weestyle").attr("href", "<?php echo get_template_directory_uri(); ?>/css/donker.css");
				});
				$(".orange").click(function(){
		    		$("#weestyle").attr("href", "<?php echo get_template_directory_uri(); ?>/css/orange.css");
				});
				$(".red").click(function(){
		    		$("#weestyle").attr("href", "<?php echo get_template_directory_uri(); ?>/css/red.css");
				});
				$(".blue").click(function(){
		    		$("#weestyle").attr("href", "<?php echo get_template_directory_uri(); ?>/css/blue.css");
				});
				$(".cyan").click(function(){
		    		$("#weestyle").attr("href", "<?php echo get_template_directory_uri(); ?>/css/cyan.css");
				});
				$(".green").click(function(){
		    		$("#weestyle").attr("href", "<?php echo get_template_directory_uri(); ?>/css/green.css");
				});
				$(".forest").click(function(){
		    		$("#weestyle").attr("href", "<?php echo get_template_directory_uri(); ?>/css/forest.css");
				});
				$(".purple").click(function(){
		    		$("#weestyle").attr("href", "<?php echo get_template_directory_uri(); ?>/css/purple.css");
				});
				$(".tosca").click(function(){
		    		$("#weestyle").attr("href", "<?php echo get_template_directory_uri(); ?>/css/tosca.css");
				});
				$(".grey").click(function(){
		    		$("#weestyle").attr("href", "<?php echo get_template_directory_uri(); ?>/css/grey.css");
				});
				$(".obot").slideUp();
				$(".ot").click(function(){
				    $(".ot").removeClass("pactive").addClass("dactive");
					$(".pt").removeClass("dactive").addClass("pactive");
		    		$(".otop").slideDown();
					$(".obot").slideUp();
				});
				$(".pt").click(function(){
				    $(".pt").removeClass("pactive").addClass("dactive");
					$(".ot").removeClass("dactive").addClass("pactive");
		    		$(".obot").slideDown();
					$(".otop").slideUp();
				});
				$(".weeshare").slideUp();
				$(".bag").click(function(){
		    		$(".weeshare").slideToggle();
				});
			});
		</script>
		
     	<script>
        	var myCenter=new google.maps.LatLng(<?php echo (get_option('maps')) ? get_option('maps').'' : '-5.932330,105.992419' ?>);
     	 	function initMap() {
	         	var mapProp = {
		    	center:myCenter,
		    	zoom:15,
		    	mapTypeId:google.maps.MapTypeId.ROADMAP
	         	};
		
    	    	var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
    	    	var marker=new google.maps.Marker({
             		position:myCenter,
    	    		title: 'Click to zoom',
    		    	icon:"<?php echo get_template_directory_uri(); ?>/images/maps.png"
    	    	});
		
	        	marker.setMap(map);
	    	}
	    	google.maps.event.addDomListener(window, 'load', initMap);
    	</script>
		
	
	<!-- Tema weesata dari ciuss.com -->

	</head>
	
	<body <?php body_class(); ?> <?php if (get_option('boxed')) { ?> style="background: url(<?php echo get_option('boxedbg'); ?>) <?php echo get_option('posisi'); ?> <?php echo get_option('bgfixed'); ?> <?php echo get_option('ulang'); ?>; background-size: <?php echo get_option('ukuran'); ?>;" <?php } ?>>
	    <div class="wees <?php echo (get_option('fixed')) ? get_option('fixed').'' : 'nofixed' ?> <?php echo (get_option('boxed')) ? get_option('boxed').'' : 'noboxed' ?>">
    	
		<div class="wrapper">
	    	<div class="mob">
    	        <?php wp_nav_menu(array('theme_location' => 'navigation', 'container' => 'div', 'container_class' => 'mobi', 'menu_class' => 'accordion', 'menu_id' => 'acc', 'fallback_cb' => false)); ?>
	        </div>
		
		    <!-- NAVIGASI TEMA WEESATA -->
		    <div class="nav-weesata <?php echo (get_option('transparan')) ? get_option('transparan').'' : 'fullblack' ?>">
			    <div class="wee-nav clear">
				
			    	<div class="inn">
					    <div class="favicon">
						    <a href="<?php echo home_url(); ?>">
						        <img id="icon" src="<?php echo (get_option('favicon')) ? get_option('favicon') : get_template_directory_uri().'/images/icon.png' ?>" alt="<?php bloginfo('name'); ?>"/>
						    </a>
						</div>
					    <div class="logo <?php echo (get_option('white')) ? get_option('white').'' : 'normal' ?>">
						    <div class="logo-img">
				            	<a href="<?php echo home_url(); ?>">
							    	<img id="logo" src="<?php echo (get_option('logo_url')) ? get_option('logo_url') : get_template_directory_uri().'/images/logo.png' ?>" alt="<?php bloginfo('name'); ?>"/>
								</a>
							</div>
		     	        </div>
						<div class="men <?php echo (get_option('white')) ? get_option('white').'' : 'normal' ?>">
					    	<div class="toplist">
					            <?php wp_nav_menu(array('theme_location' => 'navigation', 'container' => 'div', 'container_class' => 'nav', 'menu_class' => 'dd', 'menu_id' => 'dd', 'fallback_cb' => false)); ?>
				        	</div>
							<div class="kon <?php echo (get_option('white')) ? get_option('white').'' : 'normal' ?>">
					            <span class="phn"><i class="fa fa-phone"></i> <?php echo (get_option('telepon')) ? get_option('telepon').'' : '0838-1525-1385' ?></span>
								<span class="ff">fax : <strong><?php echo (get_option('fax')) ? get_option('fax').'' : '0723-4670077' ?></strong></span> <span class="de">/</span> <span class="ee"><span class="em">email : </span><?php echo (get_option('email')) ? get_option('email').'' : 'yayun@ciuss.com' ?></span>
				        	</div>
							<div class="ps">
								<i id="search" class="fa fa-search"></i> <i id="conte" class="fa fa-chevron-down"></i> <i id="jam" class="fa fa-clock-o"></i> <i id="sss" class="fa fa-twitter"></i> <i id="navi" class="fa fa-navicon"></i>
							</div>
							<div class="ca">
							    <?php get_search_form(); ?>
							</div>
		    		    	<div class="onhour">
	    	    	        	<div class="tops"><?php echo (get_option('online')) ? get_option('online').'' : 'ONLINE HOURS' ?> <i id="xclose" class="fa fa-close"></i></div>
								<div class="daytime">SENIN <span><?php echo (get_option('senin')) ? get_option('senin').'' : '08.00 - 21.00' ?></span></div>
								<div class="daytime">SELASA <span><?php echo (get_option('selasa')) ? get_option('selasa').'' : '08.00 - 21.00' ?></span></div>
								<div class="daytime">RABU <span><?php echo (get_option('rabu')) ? get_option('rabu').'' : '08.00 - 21.00' ?></span></div>
								<div class="daytime">KAMIS <span><?php echo (get_option('kamis')) ? get_option('kamis').'' : '08.00 - 21.00' ?></span></div>
								<div class="daytime">JUMAT <span class="fri"><?php echo (get_option('jumat')) ? get_option('jumat').'' : '08.00 - 21.00' ?></span></div>
								<div class="daytime">SABTU <span><?php echo (get_option('sabtu')) ? get_option('sabtu').'' : '08.00 - 21.00' ?></span></div>
								<div class="daytime">MINGGU <span class="sun"><?php echo (get_option('minggu')) ? get_option('minggu').'' : '08.00 - 23.00' ?></span></div>
		    	            </div>
							<div class="sosmed">
	    	    	        	<div class="tops">IKUTI</div>
								<?php if (get_option('twitter')) { ?>
					            	<a href="<?php echo (get_option('twitter')) ? get_option('twitter').'' : '' ?>" target="_blank">
							    		<i class="fa fa-twitter"></i>
									</a>
								<?php } ?>
					        	<?php if (get_option('facebook')) { ?>
					            	<a href="<?php echo (get_option('facebook')) ? get_option('facebook').'' : '' ?>" target="_blank">
					            	    <i class="fa fa-facebook"></i>
					            	</a>
				        		<?php } ?>
								
								<?php if (get_option('instagram')) { ?>
							    	<a href="<?php echo (get_option('instagram')) ? get_option('instagram').'' : '' ?>" target="_blank">
					            	    <i class="fa fa-envelope"></i>
					            	</a>
								<?php } ?>
								<?php if (get_option('google')) { ?>
						    		<a href="<?php echo (get_option('google')) ? get_option('google').'' : '' ?>" target="_blank">
						                <i class="fa fa-google-plus"></i>
					            	</a>
								<?php } ?>
								<?php if (get_option('youtube')) { ?>
						    		<a href="<?php echo (get_option('youtube')) ? get_option('youtube').'' : '' ?>" target="_blank">
							    	    <i class="fa fa-youtube"></i>
					    			</a>
								<?php } ?>
		    	            </div>
						</div>
		    		</div>
					
				</div>
			</div>
			<!-- NAVIGASI TEMA WEESATA -->
			
			<?php if (is_front_page()) {  
	        	get_template_part('slider'); 
	    	} else { ?>
			    <div class="" <?php if (get_option('background')) { ?>
				    style="height: 300px; background: url(<?php echo get_option('background'); ?>) center; background-size: cover;" <?php } ?>></div>
			<?php } ?>
			
			<!-- AFTER SLIDER WEESATA -->
			<div class="boxsetickers clear">
				<div class="madticker">
			    	<div id="infotit">
			        	<span>SEKILAS </span>INFO <i class="fa fa-angle-right"></i> 
			    	</div>
				
			    	<div class="mask">
	    			<?php query_posts('post_type=sekilas-info&showposts=3'); ?>
				    	<?php if (have_posts()) { ?>
	    	                <ul id="wees-ticker" class="newstickers">
	    	                	<?php while (have_posts()): the_post(); ?>
	                                <li><span><?php the_time(); ?></span> / <?php if (function_exists('smart_excerpt')) smart_excerpt(get_the_excerpt(), 55); ?></li>
	     	                	<?php endwhile; ?>
                            </ul>
	    		        <?php } else { ?>
						    <ul id="wees-ticker" class="newstickers">
	                                <li><span>3 detik yang lalu</span> / Untuk menambahkan running text silahkan ke Dashboard > Sekilas Info</li>
                            </ul>
						<?php } ?>
					<?php wp_reset_query(); ?>
	    	     	</div>
				</div>
				
				<div class="ttsearch">
					<?php get_template_part('searchpack'); ?>
		     	</div>
			</div>

			
			<!-- AFTER SLIDER WEESATA -->
			
			
			<?php if (function_exists('dimox_breadcrumbs')) {
    			dimox_breadcrumbs();
			}			?>
				
			<!-- Container -->
			<div class="clear">
