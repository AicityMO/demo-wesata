<?php
/**
 * Arsip paket tema weesata
 * Author : yayun
 * Facebook : http://facebook.com/ciussgw
 * Whatsapp : 0838-1525-1385
 */
?>

<?php get_header(); ?>

<section class="wecontent clear">
    <div class="container">
    	<div class="pack-one clear">
			<?php get_template_part('inc/loop/paket'); ?>
		</div>
			<?php get_template_part('pagination'); ?>
	</div>
</section>
	
<?php get_footer(); ?>
