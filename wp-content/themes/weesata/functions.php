<?php
/**
 * Tema mading menambahkan beberapa fungsi hook 
 * Bertujuan untuk membantu menampilkan konten sesuai kebutuhan
 * Disesuaikan untuk sebuah website desa
 *
 * @link http://codex.wordpress.org/Theme_Development
 * @link http://codex.wordpress.org/Child_Themes
 *
 * For more information on hooks, actions, and filters,
 * @link http://codex.wordpress.org/Plugin_API
 */

/**
 * Memanggil beberapa fungsi lain, dibawah ini
 */

require_once('inc/weesata/weesata-post-type.php');
require_once('inc/weesata/weesata-time-left.php');
require_once('inc/weesata/weesata-breadcrumb.php');
require_once('inc/weesata/weesata-setup.php');
require_once('inc/weesata/weesata-script.php');
require_once('inc/weesata/weesata-option.php');
require_once('inc/weesata/weesata-sidebar.php');
require_once('inc/weesata/weesata-excerpt.php');
require_once('inc/weesata/weesata-comment.php');

 
if (!isset($content_width)) {
	$content_width = 610;
}

add_action('do_meta_boxes', 'slidebox');

function slidebox(){
    remove_meta_box( 'postimagediv', 'slider', 'side' );
    add_meta_box('postimagediv', __('Upload gambar untuk slide berukuran 1200x500 pixel'), 'post_thumbnail_meta_box', 'slider', 'normal', 'high');
}
