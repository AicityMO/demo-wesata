<?php
/**
 * 404 tema weesata
 * Author : yayun
 * Facebook : http://facebook.com/ciussgw
 * Whatsapp : 0838-1525-1385
 */
?>

<?php get_header(); ?>

<section class="wecontent clear">
    <div class="pactour container clear">
	
        <div class="nf404">	
	        <div class="head404">
	    		 <h1><?php _e("<span>Error 404</span>", 'weesata'); ?></h1>
	    	</div>
	    	<div class="content404">
		    	Mohon maaf, halaman yang coba Anda akses tidak tersedia, atau tautan menuju halaman ini telah kadaluarsa
	    	</div>
		</div>
		
	</div>
</section>

<?php get_footer(); ?>
